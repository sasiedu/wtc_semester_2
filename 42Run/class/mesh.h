/*
 * Stephen Asiedu
 * mesh.h
 */

#ifndef MESH_H
# define MESH_H

#include <string>
#include <fstream>
#include <sstream>
#include <vector>
#include <structs.h>
#include <shaderClass.h>

using namespace std;

class 	Mesh{
public:
	vector<Vertex>	vertices;
	vector<GLuint>	indices;
	vector<Texture>	textures;
	GLuint			VAO;

	Mesh(vector<Vertex> vertices, vector<GLuint> indices, vector<Texture> textures){
		this->vertices = vertices;
		this->indices = indices;
		this->textures = textures;
		//building a VBO and attaching it to a VAO
		this->setup();
	}
	void 	draw(Shader	shader){
		for (GLuint i = 0; i < this->textures.size(); i++){
			//activating a texture id for bind
			glActiveTexture(GL_TEXTURE0 + i);
			glBindTexture(GL_TEXTURE_2D, this->textures[i].id);
			//getting texture type and bind number
			stringstream	ind; //for mat index
			string			type = this->textures[i].type;

			ind << i;
			string mat = "mat[" + ind.str() + "]." + type;
			string amb = "mat[" + ind.str() + "].amb";
			string diff = "mat[" + ind.str() + "].diff";
			string spec = "mat[" + ind.str() + "].spec";
			string shine = "mat[" + ind.str() + "].shine";
			glUniform1i(glGetUniformLocation(shader.Program, mat.c_str()), i);
			glUniform3f(glGetUniformLocation(shader.Program, amb.c_str()), 
	this->textures[i].amb.x, this->textures[i].amb.y, this->textures[i].amb.z);
			glUniform3f(glGetUniformLocation(shader.Program, diff.c_str()),
	this->textures[i].diff.x, this->textures[i].diff.y, this->textures[i].diff.z);
			glUniform3f(glGetUniformLocation(shader.Program, spec.c_str()),
	this->textures[i].spec.x, this->textures[i].spec.y, this->textures[i].spec.z);
			glUniform1f(glGetUniformLocation(shader.Program, shine.c_str()),
	this->textures[i].shine);
			// And finally bind the texture
			glBindTexture(GL_TEXTURE_2D, this->textures[i].id);
		}
		glUniform1i(glGetUniformLocation(shader.Program, "matCount"),
					(GLint)this->textures.size());
		//drawing here
		glBindVertexArray(this->VAO);
		glDrawElements(GL_TRIANGLES, (GLsizei)this->indices.size(), GL_UNSIGNED_INT, 0);
		glBindVertexArray(0);
	}
	~Mesh(){
		this->vertices.clear();
		this->indices.clear();
		this->textures.clear();
	}

private:
	GLuint VBO, EBO;

	void 	setup(){
		glGenVertexArrays(1, &this->VAO);
		glGenBuffers(1, &this->VBO);
		glGenBuffers(1, &this->EBO);
		glBindVertexArray(this->VAO);
		glBindBuffer(GL_ARRAY_BUFFER, this->VBO);
		glBufferData(GL_ARRAY_BUFFER, this->vertices.size() * sizeof(Vertex),
					 &this->vertices[0], GL_STATIC_DRAW);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->EBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, this->indices.size() * sizeof(GLuint),
					 &this->indices[0], GL_STATIC_DRAW);
		//setting our vertex for location 0 for our shader
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)0);
		//setting our normal for location 1 for our shader
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex),
							  (GLvoid*)offsetof(Vertex, normal));
		//setting our texcoord for location 2 for our shader
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex),
							  (GLvoid*)offsetof(Vertex, texCoord));
		glBindVertexArray(0);
	}
};

#endif