/*
 * Stephen Asiedu
 * shaderClass.h
 */

#ifndef SHADERCLASS_H
#define SHADERCLASS_H

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <glew.h>
#include <glfw3.h>

using namespace std;

class Shader {
public:
	GLuint Program;
	// Constructor with no parameters
	Shader(){}

	int loadShader(const GLchar* vertexPath, const GLchar* fragmentPath) {
		string vertexCode, fragmentCode;
		ifstream vShaderFile, fShaderFile;
		// ensures ifstream objects can throw exceptions:
		vShaderFile.exceptions (fstream::badbit);
		fShaderFile.exceptions (ifstream::badbit);
		try {
			// Open files
			vShaderFile.open(vertexPath);
			fShaderFile.open(fragmentPath);
			stringstream vShaderStream, fShaderStream;
			// Read file's buffer contents into streams
			vShaderStream << vShaderFile.rdbuf();
			fShaderStream << fShaderFile.rdbuf();
			// close file handlers
			vShaderFile.close();
			fShaderFile.close();
			// Convert stream into string
			vertexCode = vShaderStream.str();
			fragmentCode = fShaderStream.str();
		} catch (ifstream::failure e) {
			cout << "ERROR::SHADER::FILE_NOT_SUCCESFULLY_READ" << endl;
		}
		const GLchar* vShaderCode = vertexCode.c_str();
		const GLchar * fShaderCode = fragmentCode.c_str();
		/* COMPILING SHADERS */
		GLuint vertex, fragment;
		GLint success;
		GLchar infoLog[512];
		// Vertex Shader
		vertex = glCreateShader(GL_VERTEX_SHADER);
		glShaderSource(vertex, 1, &vShaderCode, NULL);
		glCompileShader(vertex);
		// checking for vertex shader errors
		glGetShaderiv(vertex, GL_COMPILE_STATUS, &success);
		if (!success) {
			glGetShaderInfoLog(vertex, 512, NULL, infoLog);
			cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << endl;
			return (-1);
		}
		// Fragment Shader
		fragment = glCreateShader(GL_FRAGMENT_SHADER);
		glShaderSource(fragment, 1, &fShaderCode, NULL);
		glCompileShader(fragment);
		// checking for fragment shader errors
		glGetShaderiv(fragment, GL_COMPILE_STATUS, &success);
		if (!success) {
			glGetShaderInfoLog(fragment, 512, NULL, infoLog);
			cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << endl;
			return (-1);
		}
		// Shader Program
		this->Program = glCreateProgram();
		glAttachShader(this->Program, vertex);
		glAttachShader(this->Program, fragment);
		glLinkProgram(this->Program);
		// checking for program shader errors
		glGetProgramiv(this->Program, GL_LINK_STATUS, &success);
		if (!success) {
			glGetProgramInfoLog(this->Program, 512, NULL, infoLog);
			cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << endl;
			return (-1);
		}
		glDeleteShader(vertex);
		glDeleteShader(fragment);
		return (0);
	}
	void Use() { glUseProgram(this->Program); }
	GLuint getProgramShader(){ return this->Program; }
};

#endif
