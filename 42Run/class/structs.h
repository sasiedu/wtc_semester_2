/*
 * Stephen Asiedu
 * structs.h
 */

#ifndef STRUCTS_H
# define STRUCTS_H

#include <iostream>
#include <glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

using namespace std;

struct Vertex {
	glm::vec3	vertex;
	glm::vec3	normal;
	glm::vec2	texCoord;
};

struct Texture{
	glm::vec3	diff; //diffuse coeff
	glm::vec3	spec; //specular coeff
	glm::vec3	amb; //ambient coeff
	GLfloat		shine;
	GLuint 		id; //texture id
	string		type; //diffuse or specular
	aiString	path;
};

typedef struct Player{
	GLfloat 	x;
	GLfloat 	y;
	GLfloat 	z;
	int 		lane;
	bool 		jump;
	bool 		superJump;
	GLfloat		startJump;
	GLfloat		startSuperJump;
	GLfloat 	startMotion;
}		t_player;

typedef struct 	Light{
	glm::vec3	pos;
	glm::vec3	color;
}				t_lit;

#endif