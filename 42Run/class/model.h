/*
 * Stephen Asiedu
 * model.h
 */
#ifndef MODEL_H
# define MODEL_H

#include <map>
#include <mesh.h>
#include <SOIL.h>

using namespace std;

class Model{
public:
	vector<Mesh>	meshes;
	bool 			error = false;

	Model(GLchar *path){
		this->loadModel(path);
	}
private:
	string			dir;
	vector<Texture>	textures_loaded;

	void 	loadModel(string path){
		//allows us to load different formats
		Assimp::Importer importer;
		const aiScene	*scene;
		//reading file into scene
		scene = importer.ReadFile(path, aiProcess_Triangulate
			| aiProcess_GenNormals | aiProcess_GenUVCoords | aiProcess_FlipUVs);
		if (!scene || scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE ||
				!scene->mRootNode){
			cout << "ERROR::ASSIMP::" << importer.GetErrorString() << endl;
			this->error = true;
			return ;
		}
		//getting our directory to be able to process assimp node
		this->dir = path.substr(0, path.find_last_of('/'));
		this->processAssimpNode(scene->mRootNode, scene);
	}
	//
	void 	processAssimpNode(aiNode *node, const aiScene *scene){
		//process all meshes in this node
		for (GLuint i = 0; i < node->mNumMeshes; i++){
			aiMesh	*mesh = scene->mMeshes[node->mMeshes[i]];
			this->meshes.push_back(this->processMesh(mesh, scene));
		}
		//now we process the rest of the nodes
		for (GLuint i = 0; i < node->mNumChildren; i++){
			this->processAssimpNode(node->mChildren[i], scene);
		}
	}
	//
	vector<Vertex>	processVertices(aiMesh *mesh){
		vector<Vertex> vertices;

		for (GLuint i = 0; i < mesh->mNumVertices; i++){
			Vertex	v;
			//getting a vertex
			glm::vec3 vectex;
			vectex.x = mesh->mVertices[i].x;
			vectex.y = mesh->mVertices[i].y;
			vectex.z = mesh->mVertices[i].z;
			v.vertex = vectex;
			//getting a normal
			vectex.x = mesh->mNormals[i].x;
			vectex.y = mesh->mNormals[i].y;
			vectex.z = mesh->mNormals[i].z;
			v.normal = vectex;
			//getting texCoords. I don't check if texCoords exist
			//b'cos i tell assimp to generate them if the mesh doesn't have texCoords
			if(mesh->mTextureCoords[0]) {
				glm::vec2 vec;
				vec.x = mesh->mTextureCoords[0][i].x;
				vec.y = mesh->mTextureCoords[0][i].y;
				//cout << "texCoords " << vec.x << " " << vec.y << endl;
				v.texCoord = vec;
			} else
				v.texCoord = glm::vec2(0.0f, 0.0f);
			vertices.push_back(v);
		}
		return (vertices);
	}
	//
	vector<GLuint>	processMyIndices(aiMesh *mesh){
		vector<GLuint> indices;

		//looping through the faces
		for(GLuint i = 0; i < mesh->mNumFaces; i++) {
			aiFace face = mesh->mFaces[i];
			//getting the indices that make that face
			for(GLuint j = 0; j < face.mNumIndices; j++)
				indices.push_back(face.mIndices[j]);
		}
		return (indices);
	}
	//
	vector<Texture>		loadMyTextures(aiMaterial* mat, aiTextureType type, string typeName){
		vector<Texture> textures;

		for (GLuint i = 0; i < mat->GetTextureCount(type); i++) {
			aiString str;
			mat->GetTexture(type, i, &str);
			// Check if texture was loaded before and if so,
			// continue to next iteration: skip loading a new texture
			GLboolean skip = false;
			for (GLuint j = 0; j < textures_loaded.size(); j++) {
				if(textures_loaded[j].path == str)
				{
					textures.push_back(textures_loaded[j]);
					// A texture with the same filepath has already been loaded,
					// continue to next one. (optimization)
					skip = true;
					break;
				}
			}
			if (!skip) {   // If texture hasn't been loaded already, load it
				Texture 	texture;
				aiColor4D	tmp;
				GLuint 		max;

				this->dir = "../textures";
				texture.id = TextureFromFile(str.C_Str(), this->dir, false);
				texture.type = typeName;
				texture.path = str;
				aiGetMaterialColor(mat, AI_MATKEY_COLOR_DIFFUSE, &tmp);
				texture.diff = glm::vec3(tmp.r, tmp.g, tmp.b);
				aiGetMaterialColor(mat, AI_MATKEY_COLOR_SPECULAR, &tmp);
				texture.spec = glm::vec3(tmp.r, tmp.g, tmp.b);
				aiGetMaterialColor(mat, AI_MATKEY_COLOR_AMBIENT, &tmp);
				texture.amb = glm::vec3(tmp.r, tmp.g, tmp.b);
				aiGetMaterialFloatArray(mat, AI_MATKEY_SHININESS, &texture.shine, &max);
				textures.push_back(texture);
				// Store it as texture loaded for entire model,
				// to ensure we won't unnecesery load duplicate textures.
				this->textures_loaded.push_back(texture);
			}
		}
		return textures;
	}
	//
	Mesh	processMesh(aiMesh *mesh, const aiScene *scene){
		vector<Vertex> vertices;
		vector<GLuint> indices;
		vector<Texture> textures;

		//processing vertices
		vertices = this->processVertices(mesh);
		//processing indices
		indices = this->processMyIndices(mesh);
		//processing the textures
		if (mesh->mMaterialIndex > 0){
			aiMaterial	*mtl = scene->mMaterials[mesh->mMaterialIndex];
			vector<Texture> diffMaps = this->loadMyTextures(mtl, aiTextureType_DIFFUSE, "texture_diffuse");
			textures.insert(textures.end(), diffMaps.begin(), diffMaps.end());
			// 2. Specular maps
			vector<Texture> specMaps = this->loadMyTextures(mtl, aiTextureType_SPECULAR, "texture_specular");
			textures.insert(textures.end(), specMaps.begin(), specMaps.end());
		}
		return Mesh(vertices, indices, textures);
	}
	//
	GLint TextureFromFile(const char* path, string directory, bool gamma)
	{
		string filename;

		//Generate texture ID and load texture data
		char *tmp = strrchr(path, '/');
		if (tmp == NULL)
			filename = string(path);
		else
			filename = string(tmp + 1);
		filename = directory + '/' + filename;
		GLuint textureID;
		glGenTextures(1, &textureID);
		int width, height;
		unsigned char* image = SOIL_load_image(filename.c_str(), &width, &height, 0, SOIL_LOAD_RGB);
		// Assign texture to ID
		glBindTexture(GL_TEXTURE_2D, textureID);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
		glGenerateMipmap(GL_TEXTURE_2D);
		// Parameters
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glBindTexture(GL_TEXTURE_2D, 0);
		SOIL_free_image_data(image);
		//loadTexture(&textureID, filename.c_str());
		return textureID;
	}
};

#endif