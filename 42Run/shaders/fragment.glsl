#version 330 core

struct  Material{
    sampler2D   texture_diffuse;
    sampler2D   texture_specular;
    vec3        amb;
    vec3        diff;
    vec3        spec;
    float       shine;
};

struct  Light{
    vec3        pos;
    vec3        color;
};

uniform Material    mat[8];
uniform Light       lits[20];
uniform int         matCount;
uniform vec3        viewPos;

in VS_OUT {
    vec3 FragPos;
    vec3 Normal;
    vec2 TexCoords;
} fs_in;

out vec4            FragColor;

vec3    CalcLight(vec3 litPos, vec3 litColor, vec3 normal, vec3 fragPos, int i){
    float   shine = mat[i].shine;
    vec3    diffColor = texture(mat[i].texture_diffuse, fs_in.TexCoords).rgb;
    vec3    specColor = texture(mat[i].texture_specular, fs_in.TexCoords).rgb;

    vec3    fragToLight = normalize(litPos - fragPos);
    vec3    fragToCamera = normalize(viewPos - fragPos);

    //ambient
    vec3    amb = 0.5 * diffColor * litColor;
    //diffuse
    float   diffCoef = max(0.0, dot(normal, fragToLight));
    vec3    diff = diffCoef * diffColor * litColor;
    //specular
    float   specCoef = 0.0;
    if (diffCoef > 0.0)
        specCoef = pow(max(0.0, dot(fragToCamera, reflect(-fragToLight, normal))), shine);
    vec3    spec = specCoef * specColor * litColor;
    //attenuation
    float   distToLit = length(litPos - fragPos);
    float   att = 1.0 / (1.0 + 0.09 * distToLit * 0.032 * (distToLit * distToLit));

    amb *= att;
    diff *= att;
    spec *= att;
    //return amb;
    return (amb + diff + spec);
}

void    main(){
    vec3 result = vec3(0.0);
    vec3 color = vec3(0.0);
    for (int i = 0; i < matCount; i++){
        for (int j = 0; j < 6; j++){
            result += CalcLight(lits[j].pos, lits[j].color, fs_in.Normal, fs_in.FragPos, i);
        }
    }
    FragColor = vec4(result, 1.0);
    vec3 gamma = vec3(1.0 / 2.0);
    FragColor  = vec4(pow(result, gamma), 1.0);
}