#include <run.h>

extern bool 			GameOver2;
extern vector<t_obs>	GenObs;
extern vector<t_obs>	coins;
extern t_menu			menu;

bool 		checkCollision(glm::vec3 pos1, glm::vec3 size1, glm::vec3 pos2, glm::vec3 size2){
	bool collisionX = pos1.x + size1.x > pos2.x &&
					  pos2.x + size2.x > pos1.x;
	bool collisionY = pos1.y + size1.y > pos2.y &&
					  pos2.y + size2.y > pos1.y;
	bool collisionZ = pos1.z + size1.z > pos2.z &&
					  pos2.z + size2.z > pos1.z;
	return collisionX && collisionY && collisionZ;
}

void		checkGameOver(glm::vec3 marioPos, glm::vec3 marioSize){
	bool ret = false;

	for (GLuint i = 0; i < GenObs.size(); i++){
		if (GenObs[i].type == 0) {
			glm::vec3 pos = glm::vec3(GenObs[i].trans.x, GenObs[i].trans.y, GenObs[i].trans.z);
			ret = checkCollision(marioPos, marioSize, pos, glm::vec3(1.89f, 0.92f, 1.88f));
		}else if (GenObs[i].type == 1 || GenObs[i].type == 4){
			glm::vec3 pos = glm::vec3(GenObs[i].trans.x, GenObs[i].trans.y, GenObs[i].trans.z);
			ret = checkCollision(marioPos, marioSize, pos, glm::vec3(2.36f, 8.17f, 2.13f));
		}else if (GenObs[i].type == 2){
			glm::vec3 pos = glm::vec3(GenObs[i].trans.x, GenObs[i].trans.y, GenObs[i].trans.z);
			ret = checkCollision(marioPos, marioSize, pos, glm::vec3(1.47f, 2.23f, 1.34f));
		}else if (GenObs[i].type == 3){
			glm::vec3 pos = glm::vec3(GenObs[i].trans.x, GenObs[i].trans.y, GenObs[i].trans.z);
			ret = checkCollision(marioPos, marioSize, pos, glm::vec3(1.47f, 1.34f, 2.23f));
		}
		if (ret)
			GameOver2 = true;
	}
}

static bool collide2(glm::vec3 pos1, glm::vec3 size1, glm::vec3 pos2, glm::vec3 size2){
	GLfloat	dist = fabsf(pos1.x - pos2.x);
	GLfloat apart = fabsf((GLfloat)(size1.x / 2.0) + (GLfloat)(size2.x / 2.0));
	if (dist >= apart)
		return false;
	dist = fabsf(pos1.y - pos2.y);
	apart = fabsf((GLfloat)(size1.y / 2.0) + (GLfloat)(size2.y / 2.0));
	if (dist >= apart)
		return false;
	dist = fabsf(pos1.z - pos2.z);
	apart = fabsf((GLfloat)(size1.z / 2.0) + (GLfloat)(size2.z / 2.0));
	if (dist >= apart)
		return false;
	return true;
}

void		checkCoins(glm::vec3 marioPos, glm::vec3 marioSize){
	bool ret = false;

	for (GLuint i = 0; i < coins.size(); i++){
		glm::vec3 pos = glm::vec3(coins[i].trans.x,coins[i].trans.y, coins[i].trans.z);
		ret = collide2(marioPos, marioSize, pos, glm::vec3(0.25f, 0.58f, 0.55f));
		if (ret){
			menu.coins += 1;
			coins.erase(coins.begin() + i);
		}
	}
}