#include <run.h>

extern Camera		*cam;
extern GLfloat 		jumpValue;
extern GLfloat		switchTime;
extern t_menu		menu;
GLfloat				speed = 0.2f;

void 		updateCamera(void){
	static GLfloat oldTime = 0.0f;

	GLfloat newTime = (GLfloat)glfwGetTime();
	if (newTime - oldTime >= 10.0f){
		speed += 0.05f;
		jumpValue += 0.01f;
		if (jumpValue > 0.35f)
			jumpValue -= 0.005f;
		oldTime = newTime;
		switchTime -= 0.02f;
		if (switchTime < 0.09f)
			switchTime += 0.02f;
	}
	if (speed > 5.5f)
		speed -= 0.05f;
	cam->Position.x += speed;
}

void 	updateMenu(void){
	if (menu.select == 0){
		menu.size = glm::vec3(0.15f, 0.15f, 0.14f);
		menu.pos = glm::vec3();
	} else if (menu.select == 1){
		menu.size = glm::vec3(1.43f, 1.32f, 1.0f);
		menu.pos = glm::vec3();
	}
}