#include <run.h>

extern t_menu		menu;
extern t_player		player;
extern int			motion;
extern Camera		*cam;
GLfloat 			jumpValue = 0.12f;
GLfloat				switchTime = 0.2f;

static void 	updateJump(){
	static	bool increase = true;
	GLfloat frame = (GLfloat)glfwGetTime();

	if (player.jump){
		if (!increase){
			player.y -= jumpValue;
			if (player.y <= 0.0f){
				player.jump = false;
				increase = true;
			}
		}else{
			player.y += jumpValue;
			if (player.y >= 1.9f)
				increase = false;
		}
	}else
		player.y = 0.0f;
}

static void		updateMotion(){
	GLfloat frame = (GLfloat)glfwGetTime();

	if (frame - player.startMotion > switchTime && !player.jump){
		player.startMotion = frame;
		motion = (motion == 1) ? 2 : 1;
		menu.dist += 1;
	}
	player.x = cam->Position.x + 7.0f;
}

void 			updatePlayer(void){
	updateMotion();
	updateJump();
	glm::vec3 marioPos = glm::vec3(player.x, player.y, player.z);
	glm::vec3 marioSize = menu.size;
	checkCoins(marioPos, marioSize);
}