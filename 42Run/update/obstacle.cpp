#include <run.h>

extern Camera			*cam;
extern vector<t_obs>	GenObs;
extern vector<t_obs>	coins;

void 	updateObs(void){
	for (GLuint i = 0; i < GenObs.size(); i++){
		if (cam->Position.x > GenObs[i].trans.x + 1.5f)
			GenObs.erase(GenObs.begin() + i);
	}
}

void 	updateCoins(void){
	for (GLuint i = 0; i < coins.size(); i++){
		if (cam->Position.x > coins[i].trans.x + 1.5f)
			coins.erase(coins.begin() + i);
	}
}