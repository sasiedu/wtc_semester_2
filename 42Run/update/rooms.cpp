#include <run.h>

extern Camera				*cam;
extern vector<t_room>		GenRooms;
extern vector<Model *>		rooms;
GLfloat 					rrCount = 45.0f;

static void 	genRoom(GLuint noRooms){
	for (int i = 0; i < 3; i++){
		int pos = rand() % noRooms;
		t_room	room;
		room.obstacle = false;
		room.type = pos;
		GLuint size = (GLuint)GenRooms.size();
		room.move = GenRooms[size - 1].move + 27.5f;
		room.room = rooms[pos];
		GenRooms.push_back(room);
	}
}

void 	updateRoom(void){
	if (cam->Position.x > rrCount){
		GenRooms.erase(GenRooms.begin());
		genRoom((GLuint)rooms.size());
		rrCount += 45.0f;
	}
}