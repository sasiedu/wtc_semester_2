//
// Created by Stephen ASIEDU on 2016/12/09.
//

#ifndef INC_42RUN_RUN_H
#define INC_42RUN_RUN_H

#include <camera.h>
#include <model.h>
#include <shaderClass.h>
#include <stdio.h>
#include <stdlib.h>
#include <glm/gtc/type_ptr.hpp>
#include <map>
#include <ft2build.h>
#include FT_FREETYPE_H

using namespace std;

#define WIN_H 800
#define WIN_W 800
#define FAR (100.0)
#define NEAR (0.1)
#define FPS (90.0)

typedef struct s_room{
	int			type;
	GLfloat 	move;
	bool 		obstacle;
	Model		*room;
}				t_room;

typedef struct s_obs{
	int			type;
	glm::vec3	trans;
	glm::vec3	rot;
	Model		*obs;
}				t_obs;

typedef struct s_menu{
	bool 			menu;
	GLuint			select;
	GLuint			dist;
	GLuint			coins;
	glm::vec3		pos;
	glm::vec3		size;
	vector<Model *>	cartoon;
}				t_menu;

/// Holds all state information relevant to a character as loaded using FreeType
struct Character {
	GLuint 			TextureID;   // ID handle of the glyph texture
	glm::ivec2 		Size;    // Size of glyph
	glm::ivec2 		Bearing;  // Offset from baseline to left/top of glyph
	GLuint 			Advance;    // Horizontal offset to advance to next glyph
};

/*
 * init functions
 */
int			createWindow(void);
int			createPlayer(void);
int			initScene(void);
int			createLight(void);
int			createRooms(void);
int			createObs(void);
int			initText(void);

/*
 * additonal init functions
 */
int			generateRooms(int size, int noRooms);
int			generateObs(int size);
int 		generateCoins(int size);
Model		*getPlayerMotion();
void 		keyCallBack(GLFWwindow *win, int key, int scan, int action, int mode);

/*
 * engine and redner functions
 */
void 		loop(void);
void 		drawPlayer(Shader shader);
void 		addLits(Shader shader);
void		drawRooms(Shader shader);
void 		drawObs(Shader shader);
void 		drawCoins(Shader shader);
void 		drawGameOver(Shader shader, Model *g);
void 		drawCartoonMenu(Shader shader);
void 		renderText(Shader shader, string text, GLfloat x, GLfloat y, GLfloat scale, glm::vec3 color);

/*
 * update functions
 */
void 		updatePlayer(void);
void 		updateCamera(void);
void 		updateRoom(void);
void 		updateObs(void);
void		updateMenu(void);
void 		updateCoins(void);
void		reset(void);

/*
 * additional update functions
 */
void 		playerMovement(void);
void 		checkKeys(void);
void 		checkGameOver(glm::vec3 marioPos, glm::vec3 marioSize);
void		checkCoins(glm::vec3 marioPos, glm::vec3 marioSize);
bool 		checkCollision(glm::vec3 pos1, glm::vec3 size1, glm::vec3 pos2, glm::vec3 size2);

#endif //INC_42RUN_RUN_H
