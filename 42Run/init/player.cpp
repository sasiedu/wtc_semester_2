#include <run.h>

extern Model		*mario1;
extern Model		*mario2;
extern Model		*crash1;
extern Model		*crash2;
extern t_menu		menu;
extern Camera		*cam;
extern t_player		player;
int					motion;

int 	createPlayer(void){
	string tmp = "../resources/crash1.obj";
	string tmp2 = "../resources/crash2.obj";
	string tmp3 = "../resources/Mario2.obj";
	string tmp4 = "../resources/Mario3.obj";

	crash1 = new Model((GLchar *)tmp.c_str());
	cout << "loaded crash2" << endl;
	crash2 = new Model((GLchar *)tmp2.c_str());
	cout << "loaded crash3" << endl;
	mario1 = new Model((GLchar *)tmp3.c_str());
	cout << "loaded mario2" << endl;
	mario2 = new Model((GLchar *)tmp4.c_str());
	cout << "loaded mario3" << endl;
	motion = 1;
	player.jump = false;
	player.superJump = false;
	player.lane = 2;
	player.startJump = 0.0f;
	player.startSuperJump = 0.0f;
	player.x = cam->Position.x + 7.0f;
	player.y = 0.0f;
	player.z = 0.0f;
	player.startMotion = 0.0f;
	cout << "player loading done" << endl;
	return (0);
}

Model	*getPlayerMotion(){
	if (motion == 1 && menu.select == 0)
		return (mario1);
	else if (motion == 2 && menu.select == 0)
		return (mario2);
	else if (motion == 1 && menu.select == 1)
		return (crash1);
	return (crash2);
}