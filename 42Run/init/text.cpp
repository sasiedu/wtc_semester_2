#include <run.h>

extern Shader				*shader2;
GLuint						textVAO;
GLuint						textVBO;
FT_Library					text;
FT_Face 					textFace;
map<GLchar, Character>		characters;

static void 	loadAsciiCharacters(FT_Face face, FT_Library txt){ //loading only 128 ascii characters
	for (GLubyte c = 0; c < 128; c++) {
		// Load character glyph
		if (FT_Load_Char(face, c, FT_LOAD_RENDER)) {
			std::cout << "ERROR::FREETYTPE: Failed to load Glyph" << std::endl;
			continue;
		}
		// Generate texture
		GLuint texture;
		glGenTextures(1, &texture);
		glBindTexture(GL_TEXTURE_2D, texture);
		glTexImage2D(
				GL_TEXTURE_2D, 0, GL_RED, face->glyph->bitmap.width, face->glyph->bitmap.rows,
				0, GL_RED, GL_UNSIGNED_BYTE, face->glyph->bitmap.buffer
		);
		// Set texture options
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		// Now store character for later use
		Character character = {
				texture, glm::ivec2(face->glyph->bitmap.width,
				face->glyph->bitmap.rows), glm::ivec2(face->glyph->bitmap_left,
				face->glyph->bitmap_top), static_cast<GLuint>(face->glyph->advance.x)
		};
		characters.insert(std::pair<GLchar, Character>(c, character));
	}
	glBindTexture(GL_TEXTURE_2D, 0);
	// Destroy FreeType once we're finished
	FT_Done_Face(face);
	FT_Done_FreeType(txt);
}

int 	initText(void){
	// Compile and setup the shader
	shader2 = new Shader;
	if (-1 == shader2->loadShader("../shaders/textVertex.glsl",
									 "../shaders/textFragment.glsl"))
		return (-1);
	glm::mat4 projection = glm::ortho(0.0f, static_cast<GLfloat>(WIN_W), 0.0f,
									  static_cast<GLfloat>(WIN_H));
	shader2->Use();
	glUniformMatrix4fv(glGetUniformLocation(shader2->Program, "projection"),
					   1, GL_FALSE, glm::value_ptr(projection));
	// All functions return a value different than 0 whenever an error occurred
	if (FT_Init_FreeType(&text)){
		cout << "ERROR::FREETYPE: Could not init FreeType Library" << endl;
		exit(-1);
	}
	// Load font as face
	if (FT_New_Face(text, "../fonts/angryBirds.ttf", 0, &textFace)){
		cout << "ERROR::FREETYPE: Failed to load font" << endl;
		exit(-1);
	}
	// Set size to load glyphs as
	FT_Set_Pixel_Sizes(textFace, 0, 48);
	// Disable byte-alignment restriction
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	loadAsciiCharacters(textFace, text);
	// Configure VAO/VBO for texture quads
	glGenVertexArrays(1, &textVAO);
	glGenBuffers(1, &textVBO);
	glBindVertexArray(textVAO);
	glBindBuffer(GL_ARRAY_BUFFER, textVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 6 * 4, NULL, GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
	cout << "text loading done" << endl;
	return 0;
}
