#include <run.h>

extern Camera				*cam;
extern vector<Model *>		obs;
extern vector<t_obs>		GenObs;
Model						*coin;
vector<t_obs>				coins;

static Model		*createObs(string name){
	Model *obs;

	obs = new Model((GLchar *)name.c_str());
	return obs;
}

int					generateCoins(int size){
	int z = rand() % 4;
	GLfloat up = (GLfloat)(rand() % 2);
	int x = 50;
	for (int i = 0; i < size; i++){
		t_obs newCoin;
		GLfloat move = 2.23f;
		if (coins.size() < 1)
			newCoin.trans.x = cam->Position.x + 100 + (GLfloat)x;
		else
			newCoin.trans.x = coins[coins.size() - 1].trans.x + (GLfloat)x;
		if (z == 0 || z == 1)
			newCoin.trans.z = -move;
		else if (z == 3)
			newCoin.trans.z = move;
		else
			newCoin.trans.z = 0.0f;
		newCoin.trans.y = up;
		newCoin.obs = coin;
		coins.push_back(newCoin);
		x = 2;
	}
	return 0;
}

int					generateObs(int size){
	for (int i = 0; i < size; i++){
		int x = (rand() % 30) + 10;
		int z = rand() % 4;
		int pos = rand() % (int)obs.size();
		t_obs newObs;
		newObs.type = pos;
		GLfloat move = 2.23f;
		if (GenObs.size() < 1)
			newObs.trans.x = cam->Position.x + 100 + (GLfloat)x;
		else
			newObs.trans.x = GenObs[GenObs.size() - 1].trans.x + (GLfloat)x;
		if (z == 0 || z == 1)
			newObs.trans.z = -move;
		else if (z == 3)
			newObs.trans.z = move;
		else
			newObs.trans.z = 0.0f;
		newObs.trans.y = 0.0f;
		newObs.obs = obs[pos];
		GenObs.push_back(newObs);
	}
	return 0;
}

int 				createObs(void){
	obs.push_back(createObs("../resources/obs1.obj"));
	cout << "loaded obs1" << endl;
	obs.push_back(createObs("../resources/obs2.obj"));
	cout << "loaded obs2" << endl;
	obs.push_back(createObs("../resources/obs3.obj"));
	cout << "loaded obs3" << endl;
	obs.push_back(createObs("../resources/obs4.obj"));
	cout << "loaded obs4" << endl;
	obs.push_back(createObs("../resources/obs5.obj"));
	cout << "loaded obs5" << endl;
	cout << "obs loading done" << endl;
	coin = createObs("../resources/coins.obj");
	cout << "loaded coins" << endl;
	return 0;
}