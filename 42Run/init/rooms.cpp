#include <run.h>

extern vector<Model *>		rooms;
extern vector<t_room>		GenRooms;

static Model 		*createRoom(string name){
	Model *room;

	room = new Model((GLchar *)name.c_str());
	return room;
}

int 		generateRooms(int size, int noRooms){
	GLfloat move = 30.0f;
	for (int i = 0; i < size; i++){
		int pos = rand() % noRooms;
		t_room	room;
		room.obstacle = false;
		room.type = pos;
		GLuint roomCount = (GLuint)GenRooms.size();
		room.move = GenRooms[roomCount - 1].move + 27.5f;
		room.room = rooms[pos];
		GenRooms.push_back(room);
		move += 30.0f;
	}
	cout << "room generated" << endl;
	return 0;
}

int				createRooms(void){
	rooms.push_back(createRoom("../resources/world4.obj"));
	cout << "loaded world4" << endl;
	rooms.push_back(createRoom("../resources/room1.obj"));
	cout << "loaded room1" << endl;
	//rooms.push_back(createRoom("../resources/room2.obj"));
	rooms.push_back(createRoom("../resources/room3.obj"));
	cout << "loaded room3" << endl;
	rooms.push_back(createRoom("../resources/room4.obj"));
	cout << "loaded room4" << endl;
	rooms.push_back(createRoom("../resources/room5.obj"));
	cout << "loaded room5" << endl;
	rooms.push_back(createRoom("../resources/room6.obj"));
	cout << "loaded room6" << endl;
	rooms.push_back(createRoom("../resources/room7.obj"));
	cout << "loaded room7" << endl;
	t_room	room;
	room.obstacle = false;
	room.type = 0;
	room.move = 0.0f;
	room.room = rooms[0];
	GenRooms.push_back(room);
	generateRooms(10, (GLint)rooms.size());
	cout << "rooms loading done" << endl;
	return 0;
}

