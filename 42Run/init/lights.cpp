#include <run.h>

extern t_lit		lits[20];

//16.5
int 	createLight(void){
	GLfloat		pos = 7;

	for (int i = 0; i < 20; i++){
		lits[i].pos.x = pos;
		lits[i].pos.y = 0.0f;
		lits[i].pos.z = 3.0f;
		lits[i].color.x = 0.9f;
		lits[i].color.y = 0.9f;
		lits[i++].color.z = 0.9f;
		lits[i].pos.x = pos;
		lits[i].pos.y = 1.0f;
		lits[i].pos.z = 5.0f;
		lits[i].color.x = 1.0f;
		lits[i].color.y =  1.0f;
		lits[i].color.z = 1.0f;
		pos += 14.7;
	}
	cout << "loaded lights" << endl;
	return 0;
}