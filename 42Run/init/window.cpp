#include <run.h>

extern GLFWwindow	*g_Win;

int 	createWindow(void){
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
	glfwWindowHint(GLFW_DECORATED, GL_TRUE);
	g_Win = glfwCreateWindow(800, 800, "42Run", NULL, NULL);
	if (g_Win == NULL){
		cout << "Failed to create GLFW window" << endl;
		glfwTerminate(); return (-1);
	}
	glfwMakeContextCurrent(g_Win);
	glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK){
		cout << "Failed to initialize GLEW" << endl;
		glfwDestroyWindow(g_Win); glfwTerminate(); return (-1);
	}
	glViewport(0, 0, WIN_W, WIN_H);
	glfwSetKeyCallback(g_Win, keyCallBack);
	glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	cout << "Window setup done" << endl;
	return 0;
}