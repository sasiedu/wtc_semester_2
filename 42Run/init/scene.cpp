#include <run.h>

extern Shader		*shader;
extern Camera		*cam;
extern Model		*gameOver;
extern t_menu		menu;

int		initScene(void) {
	shader = new Shader;
	if (-1 == shader->loadShader("../shaders/vertex.glsl", "../shaders/fragment.glsl"))
		return (-1);
	cout << "loaded shader1" << endl;
	cam = new Camera();
	cam->Position = glm::vec3(-1.0f, 4.1f, 0.0f);
	cam->Front = glm::vec3(1.0f, 0.0f, 0.0f);

	string	tmp = "../resources/gameOver.obj";
	gameOver = new Model((GLchar *)tmp.c_str());
	cout << "loaded gameOver" << endl;

	Model	*t;
	tmp.clear();
	tmp = "../resources/mario.obj";
	t = new Model((GLchar *)tmp.c_str());
	menu.cartoon.push_back(t);
	cout << "loaded mario1" << endl;
	tmp.clear();
	tmp = "../resources/crash.obj";
	t = new Model((GLchar *)tmp.c_str());
	menu.cartoon.push_back(t);
	cout << "loaded crash1" << endl;
	menu.menu = true;
	menu.select = 0;
	menu.coins = 0;
	menu.dist = 0;
	cout << "cartoons count " << menu.cartoon.size() << endl;
	cout << "scene loading done" << endl;
	return (0);
}