#include <run.h>

extern t_player		player;
extern t_menu		menu;
extern glm::mat4	view;
extern glm::mat4	projection;
extern Camera		*cam;

void 	drawCartoonMenu(Shader shader){
	Model *image;
	GLint	viewLoc = glGetUniformLocation(shader.Program, "view");
	GLint 	projLoc = glGetUniformLocation(shader.Program, "projection");

	shader.Use();
	addLits(shader);
	image = menu.cartoon[menu.select];
	for (GLuint i = 0; i < image->meshes.size(); i++){
		glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
		glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));
		GLint modeLoc = glGetUniformLocation(shader.Program, "model");

		glm::mat4 model;
		model = glm::translate(model, glm::vec3(cam->Position.x + 1.5f, 1.5f, 0.0f));
		glUniformMatrix4fv(modeLoc, 1, GL_FALSE, glm::value_ptr(model));
		image->meshes[i].draw(shader);
	}
}