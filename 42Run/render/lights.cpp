#include <run.h>

extern Camera	*cam;
extern t_lit	lits[20];

void 	addLits(Shader shader){
	stringstream	ind;
	GLint 			loc;

	GLint 	viewPosLoc = glGetUniformLocation(shader.Program, "viewPos");
	glUniform3f(viewPosLoc, cam->Position.x, cam->Position.y, cam->Position.z);
	for (int i = 0; i < 6; i++) {
		ind << i;
		string tmp = "lits["+ ind.str() + "].pos";
		string tmp1 = "lits["+ ind.str() + "].color";
		loc = glGetUniformLocation(shader.Program, tmp.c_str());
		glUniform3f(loc, lits[i].pos.x, lits[i].pos.y, lits[i].pos.z);
		loc = glGetUniformLocation(shader.Program, tmp1.c_str());
		glUniform3f(loc, lits[i].color.x, lits[i].color.y, lits[i].color.z);
	}
}