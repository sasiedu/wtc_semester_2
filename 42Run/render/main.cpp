#include <run.h>

GLFWwindow		*g_Win;
Shader			*shader;
Shader			*shader2;
Camera			*cam;
t_player		player;
t_lit			lits[20];
Model			*mario1;
Model			*mario2;
Model			*crash1;
Model			*crash2;
Model			*gameOver;
vector<Model *>	rooms;
vector<Model *>	obs;
vector<t_obs>	GenObs;
vector<t_room>	GenRooms;
t_menu			menu;


int 			main(void){
	if (createWindow() == -1)
		return 1;
	if (initScene() != -1){
		if (initText() != -1){
			createPlayer();
			createRooms();
			createObs();
			createLight();
			loop();
		}
	}
	glfwDestroyWindow(g_Win);
	glfwTerminate();
	return 0;
}