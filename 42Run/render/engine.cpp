#include <run.h>

extern GLFWwindow	*g_Win;
extern Shader		*shader;
extern Shader		*shader2;
extern Camera		*cam;
extern Model		*gameOver;
extern t_menu		menu;
glm::mat4			view;
glm::mat4			projection;
GLfloat 			randObs;
bool 				GameOver2 = false;
extern vector<t_obs>	GenObs;
extern GLfloat			speed;
extern vector<t_room>	GenRooms;
extern vector<Model *>	rooms;
extern vector<t_obs>	coins;
extern GLfloat 			jumpValue;
extern GLfloat			switchTime;
extern GLfloat 			rrCount;

void			reset(void){
	menu.coins = 0;
	menu.dist = 0;
	menu.menu = true;
	GameOver2 = false;
	GenRooms.erase(GenRooms.begin(), GenRooms.end());
	GenObs.erase(GenObs.begin(), GenObs.end());
	coins.erase(coins.begin(), coins.end());
	jumpValue = 0.12f;
	switchTime = 0.2f;
	speed = 0.2f;
	cam->Position = glm::vec3(-1.0f, 4.1f, 0.0f);
	rrCount = 45.0f;
	t_room	room;
	room.obstacle = false;
	room.type = 0;
	room.move = 0.0f;
	room.room = rooms[0];
	GenRooms.push_back(room);
	generateRooms(10, (GLint)rooms.size());
}

static void 	update(void){
	static GLfloat oldDist = 0.0f;
	static GLfloat oldCC = 0.0f;
	static GLfloat randCC = 100.0f;
	static int obsCount = 1;
	static GLfloat obsCount1 = 0.0f;
	GLfloat newDist = cam->Position.x;
	GLfloat newCC = cam->Position.x;

	if (!menu.menu){
		if (newDist - obsCount1 > 150.0f){
			obsCount++;
			obsCount1 = newDist;
		}
		if (newDist - oldDist >= randObs){
			generateObs(rand() % 10 + obsCount);
			oldDist = newDist;
			randObs = (GLfloat)((rand() % 50) + 50);
		}
		if (newCC - oldCC >= randCC){
			generateCoins(rand() % 10);
			oldCC = newCC;
			randCC = (GLfloat)((rand() % 100) + 50);
		}
		if (!GameOver2){
			updateCamera();
			updatePlayer();
			updateRoom();
			updateObs();
			updateCoins();
		}
	}else{
		updateMenu();
	}
}

static void 	renderGame(){
	shader->Use();
	addLits(*shader);
	drawRooms(*shader);
	drawPlayer(*shader);
	drawObs(*shader);
	drawCoins(*shader);
	renderText(*shader2, to_string(menu.dist), 50.0f, 50.0f, 1.0f,
			   glm::vec3(1.0f, 0.8f, 0.0f));
	string tmp = "Coins: " + to_string(menu.coins);
	renderText(*shader2, tmp, 30.0f, 750.0f, 1.0f,
			   glm::vec3(1.0f, 0.8f, 0.0f));
	if (GameOver2){
		renderText(*shader2, "GAME OVER", 200.0f, 500.0f, 1.5f, glm::vec3(1.0f, 0.0f, 0.0f));
		renderText(*shader2, "DIST : " + to_string(menu.dist),
				   200.0f, 430.0f, 1.0f, glm::vec3(1.0f, 0.8f, 0.0f));
		renderText(*shader2, "COINS : " + to_string(menu.coins),
				   200.0f, 380.0f, 1.0f, glm::vec3(1.0f, 0.8f, 0.0f));
		renderText(*shader2, "TOTAL : " + to_string(menu.dist + menu.coins),
				   200.0f, 330.0f, 1.0f, glm::vec3(1.0f, 0.8f, 0.0f));
		renderText(*shader2, "PRESS ENTER TO CONTINUE",
				   180.0f, 270.0f, 0.7f, glm::vec3(0.0f, 0.75f, 1.0f));
	}
}

static void 	rendering(void){
	view = cam->GetViewMatrix();
	projection = glm::perspective(45.0f, (GLfloat)(WIN_W / WIN_H),
								  (GLfloat)NEAR, (GLfloat)FAR);
	if (!menu.menu)
		renderGame();
	else{
		renderText(*shader2, "42 ", 150.0f, 620.0f, 3.5f, glm::vec3(1.0f, 1.0f, 1.0f));
		renderText(*shader2, " RUN", 400.0f, 620.0f, 3.0f, glm::vec3(1.0f, 0.8f, 0.0f));
		renderText(*shader2, "SELECT PLAYER AND PRESS ENTER",
				   80.0f, 50.0f, 1.0f, glm::vec3(0.0f, 0.75f, 1.0f));
		drawCartoonMenu(*shader);
	}
}

void 	loop(void){
	cout << "engine started" << endl;
	GLfloat	fps = (GLfloat)1.0 / (GLfloat)FPS;
	cout << "fps per frame " << fps << endl;
	GLfloat timeOld = 0.0f;

	randObs = 120.0f;
	while(!glfwWindowShouldClose(g_Win)){
		GLfloat timeNew = (GLfloat)glfwGetTime();
		GLfloat timeDiff = timeNew - timeOld;
		if (timeDiff >= fps){
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
			glfwPollEvents();
			checkKeys();
			rendering();
			update();
			glfwSwapBuffers(g_Win);
			timeOld = timeNew;
		}
	}
	cout << "engine ended" << endl;
}