#include <run.h>

extern vector<t_obs>	GenObs;
extern glm::mat4		view;
extern glm::mat4		projection;
extern Camera			*cam;
extern vector<t_obs>	coins;
extern Model			*gameOver;

static void 	drawObstacle(Shader shader, t_obs o){
	Model	*r;

	GLint	viewLoc = glGetUniformLocation(shader.Program, "view");
	GLint 	projLoc = glGetUniformLocation(shader.Program, "projection");
	r = o.obs;
	for (GLuint i = 0; i < r->meshes.size(); i++){
		glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
		glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));
		GLint modeLoc = glGetUniformLocation(shader.Program, "model");

		glm::mat4 model;
		model = glm::translate(model, glm::vec3(o.trans.x, 1.5f, o.trans.z));
		glUniformMatrix4fv(modeLoc, 1, GL_FALSE, glm::value_ptr(model));
		r->meshes[i].draw(shader);
	}
}

void 			drawObs(Shader shader){
	for (GLuint i = 0; i < GenObs.size(); i++){
		drawObstacle(shader, GenObs[i]);
	}
}

void 			drawCoins(Shader shader){
	for (GLuint j = 0; j < coins.size(); j++){
		Model	*r;

		GLint	viewLoc = glGetUniformLocation(shader.Program, "view");
		GLint 	projLoc = glGetUniformLocation(shader.Program, "projection");
		r = coins[j].obs;
		for (GLuint i = 0; i < r->meshes.size(); i++){
			glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
			glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));
			GLint modeLoc = glGetUniformLocation(shader.Program, "model");

			glm::mat4 model;
			model = glm::translate(model, glm::vec3(coins[j].trans.x,
						coins[j].trans.y + 1.5f, coins[j].trans.z));
			model = glm::rotate(model, (GLfloat)glfwGetTime() * glm::radians(70.0f),
								glm::vec3(0.0f, 1.0f, 0.0f));
			glUniformMatrix4fv(modeLoc, 1, GL_FALSE, glm::value_ptr(model));
			r->meshes[i].draw(shader);
		}
	}
}

void 			drawGameOver(Shader shader, Model *g){
	GLint	viewLoc = glGetUniformLocation(shader.Program, "view");
	GLint 	projLoc = glGetUniformLocation(shader.Program, "projection");

	g = gameOver;
	for (GLuint i = 0; i < g->meshes.size(); i++){
		glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
		glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));
		GLint modeLoc = glGetUniformLocation(shader.Program, "model");

		glm::mat4 model;
		model = glm::translate(model, glm::vec3(cam->Position.x + 20.0f, 1.5f, 0.0f));
		glUniformMatrix4fv(modeLoc, 1, GL_FALSE, glm::value_ptr(model));
		g->meshes[i].draw(shader);
	}
}