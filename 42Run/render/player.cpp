#include <run.h>

extern Camera		*cam;
extern t_player		player;
extern glm::mat4	view;
extern glm::mat4	projection;
extern t_menu		menu;
extern GLfloat		speed;

void	drawPlayer(Shader shader){
	Model	*mario;
	GLint	viewLoc = glGetUniformLocation(shader.Program, "view");
	GLint 	projLoc = glGetUniformLocation(shader.Program, "projection");

	mario = getPlayerMotion();
	for (GLuint i = 0; i < mario->meshes.size(); i++){
		glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
		glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));
		GLint modeLoc = glGetUniformLocation(shader.Program, "model");

		glm::mat4 model;
		model = glm::translate(model, glm::vec3(player.x, player.y + 1.5f, player.z));
		glUniformMatrix4fv(modeLoc, 1, GL_FALSE, glm::value_ptr(model));
		mario->meshes[i].draw(shader);
	}
	GLfloat tmp = player.x - speed;
	for (GLfloat i = 0.2f; i <= speed; i += 0.05f){
		glm::vec3 marioPos = glm::vec3(tmp + i, player.y, player.z);
		glm::vec3 marioSize = menu.size;
		checkCoins(marioPos, marioSize);
		checkGameOver(marioPos, marioSize);
	}
}