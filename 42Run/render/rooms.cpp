#include <run.h>

extern vector<t_room>	GenRooms;
extern glm::mat4		view;
extern glm::mat4		projection;
extern Camera			*cam;

static void 	drawRoom(Shader shader, t_room room, GLfloat move){
	Model	*r;

	GLint	viewLoc = glGetUniformLocation(shader.Program, "view");
	GLint 	projLoc = glGetUniformLocation(shader.Program, "projection");
	r = room.room;
	for (GLuint i = 0; i < r->meshes.size(); i++){
		glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
		glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));
		GLint modeLoc = glGetUniformLocation(shader.Program, "model");

		glm::mat4 model;
		model = glm::translate(model, glm::vec3(move, 1.5f, 0.0f));
		glUniformMatrix4fv(modeLoc, 1, GL_FALSE, glm::value_ptr(model));
		r->meshes[i].draw(shader);
	}
}

void 			drawRooms(Shader shader){
	for (GLuint i = 0; i < GenRooms.size(); i++){
		drawRoom(shader, GenRooms[i], GenRooms[i].move);
	}
}