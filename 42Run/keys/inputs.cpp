#include <run.h>

extern t_player		player;
extern bool 		GameOver2;
extern t_menu		menu;
bool				keys[1024];
bool 				moveNow = false;

void 	playerMovement(void){
	if (keys[GLFW_KEY_LEFT] && !GameOver2){
		player.z = -2.23f;
		moveNow = true;
	}else if (keys[GLFW_KEY_RIGHT] && !GameOver2){
		player.z = 2.23f;
		moveNow = true;
	}else if (moveNow){
		//player.z = 0.8f;
		moveNow = false;
	}else if (!GameOver2)
		player.z = 0.0f;
	if (keys[GLFW_KEY_UP] && !player.jump && !player.superJump && !GameOver2){
		player.jump = true;
		player.startJump = (GLfloat)glfwGetTime();
	}
	if (keys[GLFW_KEY_ENTER] && GameOver2){
		keys[GLFW_KEY_ENTER] = false;
		reset();
	}
}

void 	checkKeys(void){
	if (!menu.menu)
		playerMovement();
	else{
		if (keys[GLFW_KEY_LEFT] && menu.select > 0){
			menu.select -= 1;
			keys[GLFW_KEY_LEFT] = false;
		}
		else if (keys[GLFW_KEY_RIGHT] && menu.select < 1){
			menu.select += 1;
			keys[GLFW_KEY_RIGHT] = false;
		}else if (keys[GLFW_KEY_ENTER] && menu.menu){
			menu.menu = false;
			keys[GLFW_KEY_ENTER] = false;
		}
	}
}

void 	keyCallBack(GLFWwindow *win, int key, int scan, int action, int mode){
	//ESC key to close the window
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(win, GL_TRUE);
	if (action == GLFW_PRESS)
		keys[key] = true;
	else if (action == GLFW_RELEASE)
		keys[key] = false;
}