//
// Created by Stephen Asiedu on 2016/10/28.
//

#include "main.h"

using namespace std;

Scene*  _Scene1;
float   _waterLevel1;

bool    calculatePressure(int pressurePointX, int pressurePointY, int exertPointX, int exertPointY){
    if (exertPointX < 0 || exertPointX > 499 || exertPointY < 0 || exertPointY > 499)
        return false;
    if (_Scene1->getIsWater(exertPointX, exertPointY))
        return false;
    float pressure;
    pressure = _Scene1->getPressure(pressurePointX, pressurePointY);
    float exert = WATER_DENSITY * GRAVITY * _Scene1->getHeight(exertPointX, exertPointY);
    if (pressure >= exert)
    {
        //_Scene1->setIsWater(exertPointX, exertPointY, true);
        _Scene1->setPressure(exertPointX, exertPointY, pressure);
        return true;
    }
    return false;
}

bool    pressureAtPoint(int x, int y){
    bool North;
    bool West;
    bool East;
    bool South;

    North = calculatePressure(x, y, x, y - 1);
    South = calculatePressure(x, y, x, y + 1);
    East = calculatePressure(x, y, x + 1, y);
    West = calculatePressure(x, y, x - 1, y);
    if (North || South || East || West) {
        return true;
    }
    return false;
}

void    updateWater(void){
    if (_waterLevel1 >= 120.0f)
        return ;
    bool    waterRise = true;
    for(int y = 1; y < SIZE - 1; y++) {
        for (int x = 1; x < SIZE - 1; x++) {
            if (_Scene1->getIsWater(x, y)) {
                if (pressureAtPoint(x, y))
                    waterRise = false;
            }

        }
    }
    if (waterRise){
       // cout << "water rise " << _waterLevel1 << endl;
        _waterLevel1 = _waterLevel1 + 0.5f;
    }
    for(int y = 1; y < SIZE - 1; y++){
        for(int x = 0; x < SIZE - 1; x++){
            if (_Scene1->getPressure(x, y) != 0.0f){
                _Scene1->setIsWater(x, y, true);
                _Scene1->setPressure(x, y, WATER_DENSITY * GRAVITY * _waterLevel1);
                _Scene1->setHeight(x, y, _waterLevel1);
            }
        }
    }
}

float   flood(Scene *currentScene, float waterLevel){
    _waterLevel1 = waterLevel;
    _Scene1 = currentScene;
    updateWater();
    return _waterLevel1;
}

void    floodEdges(Scene *currentScene, float waterLevel){
    _waterLevel1 = waterLevel;
    _Scene1 = currentScene;
    for(int i = 0; i < SIZE; i++){
        _Scene1->setIsWater(0, i, true);
        _Scene1->setIsWater(1, i, true);
        _Scene1->setPressure(0, i, WATER_DENSITY * GRAVITY * _waterLevel1);
        _Scene1->setPressure(1, i, WATER_DENSITY * GRAVITY * _waterLevel1);
        _Scene1->setIsWater(i, 0, true);
        _Scene1->setIsWater(i, 1, true);
        _Scene1->setPressure(i, 0, WATER_DENSITY * GRAVITY * _waterLevel1);
        _Scene1->setPressure(i, 1, WATER_DENSITY * GRAVITY * _waterLevel1);
        _Scene1->setIsWater(499, i, true);
        _Scene1->setIsWater(498, i, true);
        _Scene1->setPressure(499, i, WATER_DENSITY * GRAVITY * _waterLevel1);
        _Scene1->setPressure(498, i, WATER_DENSITY * GRAVITY * _waterLevel1);
        _Scene1->setIsWater(i, 499, true);
        _Scene1->setIsWater(i, 498, true);
        _Scene1->setPressure(i, 499, WATER_DENSITY * GRAVITY * _waterLevel1);
        _Scene1->setPressure(i, 498, WATER_DENSITY * GRAVITY * _waterLevel1);
    }
}

void    waveEdge(Scene *currentScene, float waterLevel){
    _waterLevel1 = waterLevel;
    _Scene1 = currentScene;
    for (int i = 0; i < SIZE; i++){
        _Scene1->setIsWater(0, i, true);
        _Scene1->setIsWater(1, i, true);
        _Scene1->setPressure(0, i, WATER_DENSITY * GRAVITY * _waterLevel1);
        _Scene1->setPressure(1, i, WATER_DENSITY * GRAVITY * _waterLevel1);
    }
}

