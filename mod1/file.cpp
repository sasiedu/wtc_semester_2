//
// Created by Stephen Asiedu on 2016/10/29.
//

#include "main.h"

using namespace std;

static  bool    addRandomPoints(string line, Terrain *surface){
    long double coords[3];
    int         i = 0;
    smatch      base_match;
    regex       basic_regex("[0-9]+");

    while (regex_search(line, base_match, basic_regex)){
        coords[i] = stold(base_match.str());
        if (coords[i] < 0.0f || coords[i] > 2147483647.0f){
            cout << "point exceeds 2147483647.0f OR below 0.0f : " << coords[i] << endl;
            return false;
        }
        line = base_match.suffix();
        i++;
    }
    surface->add_sparse((float)coords[0], (float)coords[1], (float)coords[2]);
    if (surface->sparseSize() > 50){
        cerr << "too many points" << endl;
        exit(1);
    }
    return true;
}

static  bool    processLine(string line, Terrain *surface){
    smatch  base_match;
    int     count = 0;
    regex   bad_regex("[^0-9\\s\\,\\(\\)]");
    regex   basic_regex("\\([0-9]+,\\s*[0-9]+,\\s*[0-9]+\\)");

    if (regex_search(line, bad_regex)){
        cout << "bad line" << endl;
        return false;
    }
    while (regex_search(line, base_match, basic_regex)){
        cout << "\t" << base_match.str() << endl;
        if (!addRandomPoints(base_match.str(), surface))
            return false;
        line = base_match.suffix();
        count++;
    }
    return true;
}

void            readFile(char *file, Terrain *surface){
    string      line;
    ifstream    buffer (file);
    cout << "File : " << file << endl;
    if (buffer.is_open()){
        while (getline(buffer, line)){
            if (!processLine(line, surface))
                exit(1);
        }
        buffer.close();
    }
    else {
        cout << "Unable to open file" << endl;
        exit(1);
    }
}

