//
// Created by Stephen Asiedu on 2016/10/28.
//
#include "main.h"

using namespace std;

Scene*  _Scene2;
float   _angle2;

Opengl::Opengl(){
    cout << "Opengl constructed" << endl;
}

Opengl::~Opengl(){
    cout << "Opengl destructed" << endl;
}

void    Opengl::setupOpengl(int argc, char **argv) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(800, 800);
    glutCreateWindow(argv[0]);
}

void    Opengl::initRendering() {
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_COLOR_MATERIAL);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_NORMALIZE);
    glShadeModel(GL_SMOOTH);
    glPointSize(20.0f);
}

void    Opengl::display(Scene *currentScene, float currentAngle) {
    _Scene2 = currentScene;
    _angle2 = currentAngle;
    glutDisplayFunc(drawScene);
    glutKeyboardFunc(handleKeypress);
    glutReshapeFunc(handleResize);
    glutIdleFunc(update);
    glutMainLoop();
}

void    handleResize(int w, int h) {
    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45.0, (double)w / (double)h, 1.0, 200.0);
}

void    drawHill(int x, int z){
    //Makes OpenGL draw a triangle at every three consecutive vertices
    glBegin(GL_TRIANGLE_STRIP);

    glEnd();
}

void    drawWater(int x, int y){
    float h = _Scene2->getHeight(x, y);
    glBegin(GL_POINTS);
    glPointSize(2);
    glColor3f(0.0f, 0.0f, 1.0f);
    glNormal3f(x, h, y);
    glVertex3f(x, h, y);
    glEnd();
}

void    drawScene() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(0.0f, 0.0f, -10.0f);
    glRotatef(30.0f, 1.0f, 0.0f, 0.0f);
    glRotatef(-_angle2, 0.0f, 1.0f, 0.0f);

    GLfloat ambientColor[] = {0.4f, 0.4f, 0.4f, 1.0f};
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambientColor);

    GLfloat lightColor0[] = {0.6f, 0.6f, 0.6f, 1.0f};
    GLfloat lightPos0[] = {-0.5f, 0.8f, 0.1f, 0.0f};
    glLightfv(GL_LIGHT0, GL_DIFFUSE, lightColor0);
    glLightfv(GL_LIGHT0, GL_POSITION, lightPos0);

    float scale = 5.0f / 499;
    glScalef(scale, scale, scale);
    glTranslatef(-(float)(_Scene2->width() - 1) / 2, 0.0f, -(float)(_Scene2->length() - 1) / 2);

    for(int z = 0; z < _Scene2->length() - 1; z++) {
        glBegin(GL_TRIANGLE_STRIP);
        for(int x = 0; x < _Scene2->width(); x++) {
            float h = _Scene2->getHeight(x, z);
            if (!_Scene2->isTempWater[z][x] && !_Scene2->getIsWater(x, z)){
                glColor3f((float)h / 100, 1.0f, (float)h / 100);
                Vec3f normal = _Scene2->getNormal(x, z);
                glNormal3f(normal[0], normal[1], normal[2]);
                glVertex3f(x, _Scene2->getHeight(x, z), z);
                normal = _Scene2->getNormal(x, z + 1);
                glNormal3f(normal[0], normal[1], normal[2]);
                glVertex3f(x, _Scene2->getHeight(x, z + 1), z + 1);
            }
            else if (_Scene2->getIsWater(x, z)){
                glColor3f(0.0f, 0.0f, 1.0f);
                Vec3f normal = _Scene2->getNormal(x, z);
                glNormal3f(normal[0], normal[1], normal[2]);
                glVertex3f(x, _Scene2->getHeight(x, z), z);
                normal = _Scene2->getNormal(x, z + 1);
                glNormal3f(normal[0], normal[1], normal[2]);
                glVertex3f(x, _Scene2->getHeight(x, z + 1), z + 1);
            }
        }
        glEnd();
    }
    for (int y = 0; y < SIZE; y++){
        for (int x = 0; x < SIZE; x++){
            if (_Scene2->isTempWater[y][x]){
                glBegin(GL_POINT);
                    float h = _Scene2->getHeight(x, y);
                    glColor3f(0.0f, 0.0f, 1.0f);
                    Vec3f normal = _Scene2->getNormal(x, y);
                    glNormal3f(normal[0], normal[1], normal[2]);
                    glVertex3f(x, _Scene2->getHeight(x, y), y);
                //glVertex2f(x, _Scene2->getHeight(x, y));
                glEnd();
            }
        }
    }
    glutSwapBuffers();
}

