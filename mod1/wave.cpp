//
// Created by Stephen ASIEDU on 2016/10/29.
//

#include "main.h"

using namespace std;

Scene*  _Scene4;

int     lowestPoint1(int x, int y, float z){
    int     path = -1;
    float   tmp = z;

    if (x > 0 && _Scene4->getHeight(x - 1, y) < z){
        tmp = _Scene4->getHeight(x - 1, y);
        path = 1;
    }else if (x < SIZE - 1 && _Scene4->getHeight(x + 1, y) < z){
        tmp = _Scene4->getHeight(x + 1, y);
        path = 2;
    }else if (y > 0 && _Scene4->getHeight(x, y - 1) < z){
        tmp = _Scene4->getHeight(x, y - 1);
        path = 3;
    }else if (y < SIZE - 1 && _Scene4->getHeight(x, y + 1) < z){
        tmp = _Scene4->getHeight(x, y + 1);
        path = 4;
    }
    return path;
}

void    plotRain1(int x, int y){
    float   z = _Scene4->getHeight(x, y);
    int     path = lowestPoint1(x, y, z);

    if (path == -1){
        _Scene4->setHeight(x, y, _Scene4->getHeight(x, y) + 0.2f);
        _Scene4->setIsWater(x, y, true);
        _Scene4->isTempWater[y][x] = false;
    }
    else if (path == 1){
        _Scene4->isTempWater[y][x] = false;
        _Scene4->isTempWater[y][x - 1] = true;
    }
    else if (path == 2){
        _Scene4->isTempWater[y][x] = false;
        _Scene4->isTempWater[y][x + 1] = true;
    }else if (path == 3){
        _Scene4->isTempWater[y][x] = false;
        _Scene4->isTempWater[y - 1][x] = true;
    }else if (path == 4){
        _Scene4->isTempWater[y][x] = false;
        _Scene4->isTempWater[y + 1][x] = true;
    }
}

void    addTempWater(){
    for (int y = 0; y < SIZE; y++){
        for (int x = 0; x < SIZE; x++){
            if (_Scene4->getIsWater(x, y))
                _Scene4->isTempWater[y][x] = true;
        }
    }
}

void    wave(Scene *currentScene){
    _Scene4 = currentScene;
    addTempWater();
    for (int y = 0; y < SIZE; y++){
        for (int x = 0; x < SIZE; x++){
            if (_Scene4->isTempWater[y][x])
                plotRain1(x, y);
        }
    }
}