//
//  main.cpp
//  
//
//  Created by Stephen Asiedu on 2016/10/24.
//
//
#include "main.h"

using namespace std;

Terrain _surface;
Scene*  _Scene;
Opengl  _render;

bool    isRaining = false;
bool    isFlood = false;
bool    isWave = false;
bool    isPause = true;
float   _waterLevel = 40.0f;
float   _angle = 60.0f;

Scene* loadScene(Terrain surface) {
    Scene* t = new Scene(SIZE, SIZE);
    for(int y = 0; y < SIZE; y++) {
        for(int x = 0; x < SIZE; x++) {
            float h = surface.getSingleHeight(x ,y);
            t->setHeight(x, y, h);
        }
    }
    t->computeNormals();
    return t;
}

void    handleKeypress(unsigned char key, int x, int y) {
    cout << "key " << key << endl;
    if (key == 27){//Escape key
        cleanup();
        exit(0);
    }else if (key == 122){ //p key
        if (isPause)
            isPause = false;
        else
            isPause = true;
    }else if (key == 102){// f key
        for(int y = 0; y < SIZE; y++) {
            for(int x = 0; x < SIZE; x++) {
                float h = _surface.getSingleHeight(x ,y);
                _Scene->setHeight(x, y, h);
                _Scene->setIsWater(x, y, false);
                _Scene->setPressure(x, y, 0.0f);
                _Scene->isTempWater[y][x] = false;
            }
        }
        _Scene->computeNormals();
        floodEdges(_Scene, _waterLevel);
        isPause = false;
        _waterLevel = 5.0f;
        isFlood = true;
        isRaining = false;
        isWave = false;
    }else if (key == 114){// r key
        for(int y = 0; y < SIZE; y++) {
            for(int x = 0; x < SIZE; x++) {
                float h = _surface.getSingleHeight(x ,y);
                _Scene->setHeight(x, y, h);
                _Scene->setIsWater(x, y, false);
                _Scene->setPressure(x, y, 0.0f);
                _Scene->isTempWater[y][x] = false;
            }
        }
        _Scene->computeNormals();
        isPause = false;
        _waterLevel = 5.0f;
        isFlood = false;
        isRaining = true;
        isWave = false;
    }else if (key == 113){// q key
        for(int y = 0; y < SIZE; y++) {
            for(int x = 0; x < SIZE; x++) {
                float h = _surface.getSingleHeight(x ,y);
                _Scene->setHeight(x, y, h);
                _Scene->setIsWater(x, y, false);
                _Scene->setPressure(x, y, 0.0f);
                _Scene->isTempWater[y][x] = false;
            }
        }
        _Scene->computeNormals();
        isRaining = false;
        isWave = false;
        isFlood = false;
        isPause = false;
    }else if (key == 119){//w key
        for(int y = 0; y < SIZE; y++) {
            for(int x = 0; x < SIZE; x++) {
                float h = _surface.getSingleHeight(x ,y);
                _Scene->setHeight(x, y, h);
                _Scene->setIsWater(x, y, false);
                _Scene->setPressure(x, y, 0.0f);
                _Scene->isTempWater[y][x] = false;
            }
        }
        _Scene->computeNormals();
        waveEdge(_Scene, _waterLevel);
        isRaining = false;
        _waterLevel = 30.0f;
        isWave = true;
        isFlood = false;
        isPause = false;
    }
}

void    update(){
    if(isPause)
        return ;
    if (isFlood)
        _waterLevel = flood(_Scene, _waterLevel);
    if (isRaining)
        rain(_Scene);
    if (isWave)
        _waterLevel = flood(_Scene, _waterLevel);
    glutPostRedisplay();

}

void    cleanup() {
    delete _Scene;
}

int		main(int argc, char **argv){
    if (argc != 2){
        cerr << "run ./mod1 [filename].mod1" << endl;
        exit(1);
    }
    readFile(argv[1], &_surface);
    _surface.printSparse();
	_surface.buildTerrain();
	_surface.printMaxs();
    _Scene = loadScene(_surface);

    _render.setupOpengl(argc, argv);
    _render.initRendering();
    //waveEdge(_Scene, _waterLevel);
    _render.display(_Scene, _angle);
	return (0);
}