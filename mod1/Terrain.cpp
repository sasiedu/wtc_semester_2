//
//  Terrain.cpp
//  
//
//  Created by Stephen Asiedu on 2016/10/24.
//
//

#include "Terrain.hpp"

using namespace std;

Terrain::Terrain(){
    for(int y = 0; y < SIZE; y++){
        for (int x = 0; x < SIZE; x++){
			this->map[y][x].scaleX = x;
			this->map[y][x].scaleY = y;
			this->map[y][x].scaleZ = 0;
			this->map[y][x].x = 0.0;
			this->map[y][x].y = 0.0;
			this->map[y][x].z = 0.0;
			this->map[y][x].color = 0x00ff00;
            this->map[y][x].isSet = true;
            if (y > 50 && y < 450 && x > 50 && x < 450)
			    this->map[y][x].isSet = false;
        }
    }
	cout << "Terrain constructed" << endl;
}

Terrain::~Terrain() {
	cout << "Terrain destructed" << endl;
}

void	Terrain::toString(){
	for(int y = 0; y < SIZE; y++){
		for(int x = 0; x < SIZE; x++) {
			cout << "scale coords " << this->map[y][x].scaleX << ", " <<
			this->map[y][x].scaleY << ", " << this->map[y][x].scaleZ << " || " <<
			this->map[y][x].x << ", " << this->map[y][x].y << ", " <<
			this->map[y][x].z << " || " << this->map[y][x].color <<
			"isSet " << this->map[y][x].isSet << endl;
		}
	}
}

void	Terrain::add_sparse(float x, float y, float z){
	t_pts	p;
	
	p.x = x;
	p.y = y;
	p.z = z;
    p.isSet = true;
	sparse.push_back(p);
}

void	Terrain::printSparse(){
	vector<t_pts>::iterator s = sparse.begin();
	while (s != sparse.end()){
		cout << s->x << ", " << s->y << ", " << s->z << endl;
		s++;
	}
}

void	Terrain::buildTerrain(){
	this->setMaxs();
    //this->roundScaleSparse();
    this->newScaleSparse();
    this->addEdges();
    //this->printScaleSparse();
    //this->roundPlot();
    //this->roundMethod();
	this->plotSparse();
	this->populateMap();
}

void	Terrain::setMaxs(){
	this->maxX = 0.0;
	this->maxY = 0.0;
	this->maxZ = 0.0;
	vector<t_pts>::iterator s = sparse.begin();
	while (s != sparse.end()){
		if (s->x > this->maxX)
			this->maxX = s->x;
		if (s->y > this->maxY)
			this->maxY = s->y;
		if (s->z > this->maxZ)
			this->maxZ = s->z;
		s++;
	}
    this->maxX += this->maxX;
    this->maxY += this->maxY;
}

void	Terrain::printMaxs(){
	cout << "Max X " << this->maxX << " Max Y " << this->maxY << " Max Z "
	<< this->maxZ << endl;
}

void	Terrain::plotSparse(){
    vector<t_pts>::iterator s = sparse.begin();
    while (s != sparse.end()){
        this->map[(int)s->scaleY][(int)s->scaleX].isSet = true;
        this->map[(int)s->scaleY][(int)s->scaleX].scaleX = s->scaleX;
        this->map[(int)s->scaleY][(int)s->scaleX].scaleY = s->scaleY;
        this->map[(int)s->scaleY][(int)s->scaleX].scaleZ = s->scaleZ;
        s++;
    }
}

float	Terrain::interpolate(int x, int y){
    float   up = 0;
    float   down = 0;
    float   dist;

    vector<t_pts>::iterator s = sparse.begin();
    while (s != sparse.end()){
        dist = sqrt((x - s->scaleX) * (x - s->scaleX) + (y - s->scaleY) * (y - s->scaleY));
        if (dist != 0){
            dist = dist * dist * dist;
            up += (float)s->scaleZ / dist;
            down += (float)1 / dist;
        }
        s++;
    }
    this->map[y][x].isSet = true;
    this->map[y][x].scaleX = x;
    this->map[y][x].scaleY = y;
    this->map[y][x].scaleZ = (float)up / down;
}

void	Terrain::populateMap(){
    for(int y = 0; y < SIZE; y++){
        for(int x = 0; x < SIZE; x++){
            if (!this->map[y][x].isSet)
                this->interpolate(x, y);
        }
    }
}

void    Terrain::addEdges() {
    t_pts   edge;

    edge.isSet = true;
    edge.scaleX = 50;
    edge.scaleY = 50;
    edge.scaleZ = 0;
    this->sparse.push_back(edge);
    edge.scaleX = SIZE - 50;
    this->sparse.push_back(edge);
    edge.scaleY = SIZE - 50;
    this->sparse.push_back(edge);
    edge.scaleX = 50;
    this->sparse.push_back(edge);
    edge.scaleY = SIZE / 2;
    this->sparse.push_back(edge);
    edge.scaleX = SIZE - 1;
    this->sparse.push_back(edge);
    edge.scaleY = 50;
    edge.scaleX = SIZE / 2;
    this->sparse.push_back(edge);
    edge.scaleY = SIZE - 50;
    this->sparse.push_back(edge);
    edge.scaleX = SIZE - 150;
    this->sparse.push_back(edge);
    edge.scaleX = 150;
    this->sparse.push_back(edge);
    edge.scaleY = 50;
    this->sparse.push_back(edge);
    edge.scaleX = SIZE - 150;
    this->sparse.push_back(edge);
    edge.scaleX = SIZE - 50;
    edge.scaleY = 150;
    this->sparse.push_back(edge);
    edge.scaleY = SIZE - 150;
    this->sparse.push_back(edge);
    edge.scaleX = 50;
    this->sparse.push_back(edge);
    edge.scaleY = 150;
    this->sparse.push_back(edge);
}

void	Terrain::printSetValues(int startX, int endX, int startY, int endY) {
    for (int y = startY; y < endY; y++) {
        for (int x = startX; x < endX; x++) {
            cout << this->map[y][x].isSet << " ";
        }
        cout << endl;
    }
    cout << endl;
}

void	Terrain::printHeightValues(int startX, int endX, int startY, int endY){
	for (int y = startY; y < endY; y++){
		for(int x = startX; x < endX; x++){
			cout  << this->map[y][x].scaleZ << " ";
		}
		cout << endl;
	}
	cout << endl;
}

void	Terrain::interpolateCircle(int posX, int posY, int z){
    int rad = 40;
    int y;
    int x;

    while(z > 0){
        x = posX - rad;
        while (x <= posX + rad){
            y = posY - rad;
            while (y <= posY + rad){
                if ((x - posX) * (x - posX) + ((y - posY) * (y - posY)) <= (rad * rad) && x < SIZE && x >= 0
                        && y < SIZE && y >= 0 && (!this->map[y][x].isSet || this->map[y][x].scaleZ <= z)){
                    this->map[y][x].isSet = true;
                    this->map[y][x].scaleZ = z;
                }
                y++;
            }
            x++;
        }
        z--;
        rad++;
    }
}

void    Terrain::printScaleSparse(){
    vector<t_pts>::iterator s = sparse.begin();
    cout << "printing scaled sparse && edges" << endl;
    while (s != sparse.end()){
        cout << "x " << s->scaleX << " y " << s->scaleY << " z " << s->scaleZ << endl;
        s++;
    }
    cout << "scaled sparse" << endl;
}

void    Terrain::roundScaleSparse() {
    int     max = 0;

    max = this->maxY > this->maxX ? this->maxY : this->maxX;
    max = max > this->maxZ ? max : this->maxZ;

    vector<t_pts>::iterator s = sparse.begin();
    while (s != sparse.end()){
        s->scaleX = s->x;
        s->scaleY = s->y;
        s->scaleZ = s->z;
        s++;
    }

    while (max > SIZE - 1){
        max /= 1.1f;
        vector<t_pts>::iterator s = sparse.begin();
        while (s != sparse.end()){
            s->scaleX /= 1.1f;
            s->scaleX = s->scaleX >= 0 ? s->scaleX : 0;
            s->scaleY /= 1.1f;
            s->scaleY = s->scaleY >= 0 ? s->scaleY : 0;
            if (s->scaleX < 200)
                s->scaleX += 200 - s->scaleX;
            if (s->scaleX > 300)
                s->scaleX -= s->scaleX - 300;
            if (s->scaleY < 200)
                s->scaleY += 100 - s->scaleY;
            if (s->scaleY > 300)
                s->scaleY -= s->scaleY - 400;
            s++;
        }
    }
    max = this->maxZ;
    while (max > 200){
        max /= 1.1f;
        vector<t_pts>::iterator s = sparse.begin();
        while (s != sparse.end()){
            s->scaleZ /= 1.115f;
            s++;
        }
    }
}

void    Terrain::roundMethod() {
    vector<t_pts>::iterator s = sparse.begin();
    while (s != sparse.end()){
        this->interpolateCircle(s->scaleX, s->scaleY, s->scaleZ);
        s++;
    }
}

void    Terrain::roundPlot() {
    vector<t_pts>::iterator s = sparse.begin();
    while (s != sparse.end()){
        this->map[(int)s->scaleY][(int)s->scaleX].scaleZ = s->scaleZ;
        this->map[(int)s->scaleY][(int)s->scaleX].isSet = true;
        s++;
    }
}

void    Terrain::newScaleSparse(){
    vector<t_pts>::iterator s = sparse.begin();
    while (s != sparse.end()){
        s->scaleX = (float)s->x / this->maxX * 500;
        //s->scaleX += 150;
        s->scaleY = (float)s->y / this->maxY * 500;
        //s->scaleY += 150;
        s->scaleZ = (float)s->z / this->maxZ * 150;
        s++;
    }
}