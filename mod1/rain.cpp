//
// Created by Stephen Asiedu on 2016/10/29.
//

#include "main.h"

using namespace std;

Scene*  _Scene3;

bool    checked[SIZE][SIZE];

void    resetChecked(){
    for (int y = 0; y < SIZE; y++){
        for (int x = 0; x < SIZE; x++){
            checked[y][x] = false;
        }
    }
}

int     lowestPoint(int x, int y, float z){
    int     path = -1;
    float   tmp = z;

    if (x > 0 && !checked[x - 1][y] && _Scene3->getHeight(x - 1, y) < z){
        tmp = _Scene3->getHeight(x - 1, y);
        path = 1;
    }else if (x < SIZE - 1 && !checked[x + 1][y] && _Scene3->getHeight(x + 1, y) < z){
        tmp = _Scene3->getHeight(x + 1, y);
        path = 2;
    }else if (y > 0 && !checked[x][y - 1] && _Scene3->getHeight(x, y - 1) < z){
        tmp = _Scene3->getHeight(x, y - 1);
        path = 3;
    }else if (y < SIZE - 1 && !checked[x][y + 1] && _Scene3->getHeight(x, y + 1) < z){
        tmp = _Scene3->getHeight(x, y + 1);
        path = 4;
    }
    return path;
}

void    plotRain(int x, int y){
    float   z = _Scene3->getHeight(x, y);
    int     path = lowestPoint(x, y, z);

    if (path == -1){
        _Scene3->setHeight(x, y, _Scene3->getHeight(x, y) + 0.2f);
        _Scene3->setIsWater(x, y, true);
        _Scene3->isTempWater[y][x] = false;
    }
    else if (path == 1){
        _Scene3->isTempWater[y][x] = false;
        _Scene3->isTempWater[y][x - 1] = true;
    }
    else if (path == 2){
        _Scene3->isTempWater[y][x] = false;
        _Scene3->isTempWater[y][x + 1] = true;
    }else if (path == 3){
        _Scene3->isTempWater[y][x] = false;
        _Scene3->isTempWater[y - 1][x] = true;
    }else if (path == 4){
        _Scene3->isTempWater[y][x] = false;
        _Scene3->isTempWater[y + 1][x] = true;
    }
}

void    generateRain(int count){
    int     x;
    int     y;

    for (int i = 0; i < count; i++){
        x = rand() % 497;
        y = rand() % 497;
        _Scene3->isTempWater[x][y] = true;
    }
}

void    rain(Scene *currentScene){
    _Scene3 = currentScene;
    generateRain(5000);
    for (int y = 0; y < SIZE; y++){
        for (int x = 0; x < SIZE; x++){
            if (_Scene3->isTempWater[y][x])
                plotRain(x, y);
        }
    }
}