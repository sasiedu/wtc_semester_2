//
// Created by Stephen Asiedu on 2016/10/28.
//

#ifndef MOD11_MAIN_H
#define MOD11_MAIN_H

#ifdef __APPLE__
#include <OpenGL/OpenGL.h>
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

# include <vector>
# include <fstream>
# include <string>
# include <iterator>
# include <string>
# include <regex>
# include "Terrain.hpp"
# include "scene.h"
# include "defines.h"

typedef struct  s_vec{
    float       x;
    float       y;
    float       z;
}               t_vec;

typedef	struct	s_color {
    int			tmp1;
    int			tmp2;
    int			tmp3;
    int			area;
    int			color;
}				t_color;

typedef struct  s_rain{
    int         x;
    int         y;
    int         height;
    int         isRemove;
}               t_rain;

typedef struct  s_rainSide{
    int         x;
    int         y;
    float       height;
}              t_rainSide;

class   Opengl{
public:
    Opengl();
    ~Opengl();
    Scene*  _Scene;
    void    setupOpengl(int argc, char **argv);
    void    display(Scene *currentScene, float currentAngle);
    void    initRendering();
    void    display(Scene *currentScene);
};

int     graphics(Terrain surface);
float   flood(Scene *currentScene, float waterLevel);
void    floodEdges(Scene *currentScene, float waterLevel);
void    handleResize(int w, int h);
void    handleKeypress(unsigned char key, int x, int y);
void    drawScene();
void    cleanup();
void    update();
void    readFile(char *file, Terrain *surface);
void    rain(Scene *currentScene);
void    waveEdge(Scene *currentScene, float waterLevel);
void    resetChecked();
int     lowestPoint(int x, int y, float z);
void    plotRain(int x, int y);
void    wave(Scene *currentScene);

#endif //MOD11_MAIN_H
