//
//  Terrain.hpp
//  
//
//  Created by Stephen Asiedu on 2016/10/24.
//
//

#ifndef Terrain_hpp
# define Terrain_hpp

#include <iostream>
# include <stdio.h>
# include <fstream>
# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <string>
# include <unistd.h>
# include <math.h>
# include <vector>
# include <openGL/gl.h>
# include <openGL/glu.h>
# include <glut/glut.h>
# include "defines.h"

typedef struct  s_pts{
    bool        isSet;
    float       scaleX;
    float       scaleY;
    float       scaleZ;
    float       x;
    float       y;
    float       z;
    int32_t    color;
}               t_pts;

class	Terrain{
private:
	t_pts				map[SIZE][SIZE];
	std::vector<t_pts>	sparse;
    float               maxX;
    float               maxY;
    float               maxZ;
    void                setMaxs();
    void                plotSparse();
    void                populateMap();
    float               interpolate(int x, int y);
    void                interpolateCircle(int posX, int posY, int z);
    void                roundScaleSparse();
    void                roundMethod();
    void                roundPlot();
    void                printScaleSparse();
    void                newScaleSparse();
    void                addEdges();
	
public:
	Terrain();
	~Terrain();
    void                 toString();
    void                 add_sparse(float x, float y, float z);
    void                 printSparse();
    void                 printMaxs();
    int                  sparseSize(){ return this->sparse.size(); }
    void                 buildTerrain();
	void				 printSetValues(int startX, int endX, int startY, int endY);
	void				 printHeightValues(int startX, int endX, int startY, int endY);
	//t_pts				 getMap(){ return this->map;}
	float				 getSingleHeight(int x, int y){ return this->map[y][x].scaleZ; }
};

#endif /* Terrain_hpp */
