//
//  defines.h
//  
//
//  Created by Stephen Asiedu on 2016/10/24.
//
//

#ifndef defines_h
# define defines_h

# define	SIZE 500
# define	START 100
# define	END 400
# define    WATER_DENSITY 1000
# define    GRAVITY 10

#endif /* defines_h */
