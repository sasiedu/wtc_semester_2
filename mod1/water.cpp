//
// Created by Stephen ASIEDU on 2016/10/27.
//
#include "main.hpp"


using namespace std;

SideWater::SideWater() {

}

void    SideWater::set(int pressureX, int pressureY, int exertX, int exertY, int height){
    this->x = exertX;
    this->y = exertY;
    this->pressure = WATER_DENSITY * GRAVITY * height;
    this->pressurePercent = 0.0;
}

bool    equalOutWater(int x, int y){
    SideWater   sides[8];
    float       totalExertPressure;
    float       pressure;
    float       maxPressureDifference;
    float       totalHeight = 0;
    int         pressurePoints = 0;
    float       volumeToRemove;

    totalExertPressure = 0.0;
    maxPressureDifference = 0.0;
    pressure = WATER_DENSITY * GRAVITY * _Scene->getPressure(x, y);
    for(int i = 0; i < 8; i++){
        if (sides[i].pressure < pressure) {
            sides[i].pressureDifference = pressure - sides[i].pressure;
            totalExertPressure += sides[i].pressure;
            pressurePoints++;
            totalHeight += _Scene->getHeight(sides[i].x, sides[i].y);
        }
    }
    if (totalExertPressure == 0.0)
        return false;
    float   newPressure = totalExertPressure + pressure;
    float   equilibriumPressure = newPressure / pressurePoints;
    totalHeight += _Scene->getHeight(x, y);
    float   equalHeight = (float)equilibriumPressure / newPressure * totalHeight;
    for(int i = 0; i < 8; i++){
        if (sides[i].pressure < pressure){
            _Scene->setHeight(sides[i].x, sides[i].y, equalHeight);
            _Scene->setIsWater(sides[i].x, sides[i].y, true);
        }
    }
    _Scene->setHeight(x, y, equalHeight);
    _Scene->setIsWater(x, y, true);
    return true;
}

void    moveWater(void){
    bool    waterRise = true;

    for(int y = 0; y < SIZE; y++) {
        for (int x = 0; x < SIZE; x++) {
            if (_Scene->getIsWater(x, y)) {
                if (equalOutWater(x, y))
                    waterRise = false;
            }

        }
    }
    glutPostRedisplay();
}