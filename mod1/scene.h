//
// Created by Stephen ASIEDU on 2016/10/25.
//

#ifndef MOD11_SCENE_H_H
# define MOD11_SCENE_H_H

# include "vec3f.h"
# include "defines.h"

class Scene {
private:
    int         w; //Width
    int         l; //Length
    float**     hs; //Heights
    bool        isWater[SIZE][SIZE];
    float       pressure[SIZE][SIZE];
    Vec3f**     normals;
    bool        computedNormals; //Whether normals is up-to-date

public:
    bool        isTempWater[SIZE][SIZE];
    Scene(int w2, int l2);
    ~Scene();
    int     width() { return w; }
    int     length() { return l; }
    void    setHeight(int x, int z, float y) { hs[z][x] = y; computedNormals = false; } //Sets the height at (x, z) to y
    float   getHeight(int x, int z) { return hs[z][x]; } //Returns the height at (x, z)
    void    computeNormals();
    Vec3f   getNormal(int x, int z);
    void    setIsWater(int x, int y, bool value){ isWater[y][x] = value; }
    bool    getIsWater(int x, int y){ return isWater[y][x]; }
    void    setPressure(int x, int y, float value){ pressure[y][x] = value; }
    float   getPressure(int x, int y){ return pressure[y][x]; }
    void    printPressure();
};

#endif //MOD11_SCENE_H_H
