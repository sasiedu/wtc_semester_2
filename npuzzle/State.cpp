//
// Created by Stephen ASIEDU on 2016/11/14.
//

#include "State.h"

State::State(int **endPuzzle, int size, int heuristicType, bool start) {
    this->puzzleSize = size;
    this->start = start;
    this->endPuzzle = endPuzzle;
    this->heuristicType = heuristicType;
    this->gridSize = size * size;
}

State::~State() {
    for (int i = 0; i < this->puzzleSize; i++){
        delete[] this->currentPuzzle[i];
    }
    delete[] this->currentPuzzle;
    cout << "state destructed" << endl;
}

int     State::manthattanDistance(int posX, int posY, int value) {
    int     cost = 0;
    bool    found = false;

    for (int y = 0; y < this->puzzleSize; y++){
       for (int x = 0; x < this->puzzleSize; x++){
           if (this->currentPuzzle[y][x] == value){
               cost = abs(posX - x) + abs(posY - y);
               found = true;
               break ;
           }
       }
       if (found)
           break ;
    }
    return cost;
}

void    State::manthattanHeuristic() {
    for (int y = 0; y < this->puzzleSize; y++){
        for (int x = 0; x < this->puzzleSize; x++){
            if (this->endPuzzle[y][x] > 0) {
                this->heuristicCost += this->manthattanDistance(x, y, this->endPuzzle[y][x]);
            }
        }
    }
}

void    State::misplacedTilesHeuristic() {
    int     count = 0;

    for (int i = 0; i < this->puzzleSize; i++){
        for(int j = 0; j < this->puzzleSize; j++){
            if (this->currentPuzzle[i][j] != 0 && this->currentPuzzle[i][j] != this->endPuzzle[i][j])
                count++;
        }
    }
    this->heuristicCost += count;
}

int     State::isOutOfRowCol(int x, int y, int val) {
    int     count = 0;

    for (int i = 0; i < this->puzzleSize; i++){
        for (int j = 0; j < this->puzzleSize; j++){
            if (this->endPuzzle[i][j] == val){
                if (i != y)
                    count++;
                if (x != j)
                    count++;
                return count;
            }
        }
    }
    return 0;
}

void    State::tilesOutofRowAndCol() {
    for (int y = 0; y < this->puzzleSize; y++){
        for (int x = 0; x < this->puzzleSize; x++){
            this->heuristicCost += isOutOfRowCol(x, y, this->currentPuzzle[y][x]);
        }
    }
}

void    State::linearConflicts() {
    for (int i = 0; i < this->puzzleSize; i++){
        for (int j = 1; j < this->puzzleSize; j++){
            for (int y = 0; y < this->puzzleSize; y++){
                for (int x = 1;x < this->puzzleSize; x++){
                    if (this->endPuzzle[i][j] == this->currentPuzzle[y][x - 1]
                        && this->endPuzzle[i][j - 1] == this->currentPuzzle[y][x])
                        this->heuristicCost += 2;
                }
            }
        }
    }
}

void    State::euclideanHeuristic() {
    int     found = false;
    for (int i = 0; i < this->puzzleSize; i++){
        for (int j = 0; j < this->puzzleSize; j++){
            int val = this->currentPuzzle[i][j];
            for (int y = 0; y < this->puzzleSize; y++){
                for (int x = 0; x < this->puzzleSize; x++){
                    if (this->endPuzzle[y][x] == val){
                        int dx = abs(x - j);
                        int dy = abs(y - i);
                        this->heuristicCost += sqrt(dx * dx + dy * dy);
                        found = true;
                        break ;
                    }
                }
                if (found)
                    break;
            }
            found = false;
        }
    }
}

void    State::addCurrentPuzzle(int **puzzle, int stepCost, int move) {
    this->lastMove = move;
    this->currentPuzzle = puzzle;
    this->stepsCost = stepCost;
    this->heuristicCost = 0;
    if (this->heuristicType == 1){
        this->manthattanHeuristic();
        this->euclideanHeuristic();
    }else if (this->heuristicType == 2){
        this->misplacedTilesHeuristic();
        this->manthattanHeuristic();
    }else if (this->heuristicType == 3) {
        this->tilesOutofRowAndCol();
        this->linearConflicts();
    }else if (this->heuristicType == 4){
        this->euclideanHeuristic();
        this->linearConflicts();
    }
    this->totalCost = this->heuristicCost + this->stepsCost;
}

void    State::printPuzzle(int **puzzle) {
    for (int i = 0; i < this->puzzleSize; i++){
        for (int j = 0; j < this->puzzleSize; j++){
            if (this->gridSize < 10){
                if (puzzle[i][j] == 0)
                    printf(GREEN "%01d " RESET, puzzle[i][j]);
                else
                    printf(RED "%01d " RESET, puzzle[i][j]);
            }
            else if (this->gridSize > 9 && this->gridSize < 100) {
                if (puzzle[i][j] == 0)
                    printf(GREEN "%02d " RESET, puzzle[i][j]);
                else
                    printf(RED "%02d " RESET, puzzle[i][j]);
            }
            else{
                if (puzzle[i][j] == 0)
                    printf(GREEN "%03d " RESET, puzzle[i][j]);
                else
                    printf(RED "%03d " RESET, puzzle[i][j]);
            }
        }
        cout << endl;
    }
}

bool    State::solvedState() {
    for (int i = 0; i <  this->puzzleSize; i++){
        for (int j = 0; j < this->puzzleSize; j++){
            if (this->endPuzzle[i][j] != this->currentPuzzle[i][j])
                return false;
        }
    }
    return true;
}

void    State::getEmptySlot(int *x, int *y) {
    for (int i = 0; i < this->puzzleSize; i++){
        for (int j = 0; j < this->puzzleSize; j++){
            if (this->currentPuzzle[i][j] == 0){
                *x = j;
                *y = i;
                return;
            }
        }
    }
}