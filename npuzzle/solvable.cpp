//
// Created by Stephen Asiedu on 2016/11/15.
//
#include "npuzzle.h"

void printTopRight(int **puzz, int x1, int y1, int x2, int y2, int *arr, int ind){
    int i = 0, j = 0;
    //print values in the row
    for(i = x1; i <= x2; i++){
        if (puzz[y1][i] != 0)
            arr[++ind] = puzz[y1][i];
    }
    //print values in column
    for(j = y1 + 1; j <= y2; j++){
        if (puzz[j][x2] != 0)
            arr[++ind] = puzz[j][x2];
    }
    if(x2 - x1 > 0)
        printBottomLeft(puzz, x1, y1 + 1, x2 - 1, y2, arr, ind);
}

void printBottomLeft(int **puzz, int x1, int y1, int x2, int y2, int *arr, int ind){
    int i = 0, j = 0;
    //print the values in the row in reverse order
    for(i = x2; i >= x1; i--){
        if (puzz[y2][i] != 0)
            arr[++ind] = puzz[y2][i];
    }
    //print values in column in reverse order
    for(j = y2 -1; j >= y1; j--){
        if (puzz[j][x1] != 0)
            arr[++ind] = puzz[j][x1];
    }
    if (x2 - x1 > 0)
        printTopRight(puzz, x1 + 1, y1, x2, y2 - 1, arr, ind);
}

// A utility function to count inversions in given
// array 'arr[]'. Note that this function can be
// optimized to work in O(n Log n) time. The idea
// here is to keep code small and simple.
int     getInvCount(int *puzzle, int size)
{
    int     invCount = 0;
    cout << GREEN << "grid in line : ";
    for (int a = 0; a < size * size - 1; a++)
        cout << puzzle[a] << " ";
    cout << RESET << endl;
    for (int i = 0; i < size * size - 2; i++){
        for (int j = i + 1; j < size * size - 1; j++){
            // count pairs(i, j) such that i appears
            // before j, but i > j.
            if (puzzle[i] > puzzle[j]) {
                invCount++;
            }
        }
    }
    cout << MAGENTA << "inversions : " << YELLOW << invCount << RESET << endl;
    return invCount;
}

// find Position of blank from bottom
int     findBlankPosition(int **puzzle, int size){
    // start from bottom-right corner of matrix
    for (int i = size - 1; i >= 0; i--){
        for (int j = size - 1; j >= 0; j--){
            if (puzzle[i][j] == 0)
                return size - i;
        }
    }
    return -1;
}

// This function returns true if given
// instance of N*N - 1 puzzle is solvable
bool    isPuzzleSolvable(int **puzzle, int size){
    // Count inversions in given puzzle
    int     temp[size * size];

    //generate conversion string
    printTopRight(puzzle, 0, 0, size - 1, size - 1, temp, -1);

    int count = 0;
    for (int i = 1; i < size * size - 1; i++){
        for (int j = 0; j < size * size - 1; j++){
            if (temp[j] == i)
                count++;
        }
        if (count != i){
            cout << RED << "invalid map." << RESET << endl;
            exit(1);
        }
    }

    int invCount = getInvCount(temp, size);

    // If grid is odd, return true if inversion
    // count is even.
    if (size % 2 != 0){
        if (invCount % 2 == 0)
            return true;
    }
    else //puzzle is even
    {
        int blankPos = findBlankPosition(puzzle, size);
        if (blankPos % 2 == 0 && invCount % 2 != 0)
            return true;
        if (blankPos % 2 != 0 && invCount % 2 == 0)
            return true;
    }
    return false;
}

