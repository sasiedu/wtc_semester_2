cmake_minimum_required(VERSION 3.6)
project(npuzzle)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(SOURCE_FILES main.cpp npuzzle.h npuzzleTools.cpp State.cpp State.h aStar.cpp solvable.cpp)
add_executable(npuzzle ${SOURCE_FILES} npuzzle.h npuzzleTools.cpp State.cpp State.h solvable.cpp)