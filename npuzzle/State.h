//
// Created by Stephen ASIEDU on 2016/11/14.
//

#ifndef NPUZZLE_STATECLASS_H
#define NPUZZLE_STATECLASS_H

#include <iostream>
#include <fstream>
#include <cmath>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define LEFT 1
#define RIGHT 2
#define UP 3
#define DOWN 4
#define NONE 5
#define FOUND -1;

#define RESET   "\033[0m"
#define BLACK   "\033[30m"      /* Black */
#define RED     "\033[31m"      /* Red */
#define GREEN   "\033[32m"      /* Green */
#define YELLOW  "\033[33m"      /* Yellow */
#define BLUE    "\033[34m"      /* Blue */
#define MAGENTA "\033[35m"      /* Magenta */
#define CYAN    "\033[36m"      /* Cyan */
#define WHITE   "\033[37m"      /* White */

using namespace std;

class State {
private:
    State   *cameFrom;
    int     **currentPuzzle;
    int     **endPuzzle;
    int     heuristicType;
    int     heuristicCost;
    int     stepsCost;
    int     totalCost;
    int     puzzleSize;
    int     gridSize;
    void    manthattanHeuristic();
    void    printPuzzle(int **puzzle);
    int     manthattanDistance(int posX, int posY, int value);
    void    misplacedTilesHeuristic();
    void    tilesOutofRowAndCol();
    int     isOutOfRowCol(int x, int y, int val);
    void    euclideanHeuristic();
    void    linearConflicts();

public:
    int     lastMove;
    bool    start;
    State(int **endPuzzle, int size, int heuristicType, bool start);
    ~State();
    void    addCurrentPuzzle(int **puzzle, int stepCost, int move);
    void    printCurrentPuzzle(){this->printPuzzle(this->currentPuzzle);}
    void    printEndPuzzle(){this->printPuzzle(this->endPuzzle);}
    void    printCameFromPuzzle(){this->printPuzzle(this->cameFrom->currentPuzzle);}
    int     getTotalCost(){return this->totalCost;}
    int     getHeuristicCost(){return this->heuristicCost;}
    int     getHeuristicType(){ return this->heuristicType;}
    int     getStepsCost(){ return this->stepsCost;}
    int     getSize(){ return this->puzzleSize;}
    void    addCameFrom(State *cameFrom){this->cameFrom = cameFrom;}
    State   *getCameFrom(){ return this->cameFrom;}
    bool    solvedState();
    void    getEmptySlot(int *x, int *y);
    int     **getCurrentPuzzle(){ return this->currentPuzzle;}
    int     **getEndPuzzle(){ return this->endPuzzle;}
};


#endif //NPUZZLE_STATECLASS_H
