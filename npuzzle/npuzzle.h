//
// Created by Stephen ASIEDU on 2016/11/14.
//

#ifndef NPUZZLE_NPUZZLE_H
#define NPUZZLE_NPUZZLE_H

#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <cmath>
#include "State.h"
#include <vector>
#include <regex>
#include <regex.h>
#include <string.h>
#include <string>
#include <fcntl.h>
#include <unistd.h>

using namespace std;

int     **generateEndPuzzle(int size);
void    aStarAlgorithm(State *initialState, int greedy);
void    idaStarAlgorithm(State *initialState);
bool    isNumber(const char *str);
bool    isPuzzleSolvable(int **puzzle, int size);
void    printTopRight(int **puzz, int x1, int y1, int x2, int y2, int *arr, int ind);
void    printBottomLeft(int **puzz, int x1, int y1, int x2, int y2, int *arr, int ind);
void    printTopRight(int **puzz, int x1, int y1, int x2, int y2, int *arr, int ind);

#endif //NPUZZLE_NPUZZLE_H
