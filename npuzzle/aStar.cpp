//
// Created by Stephen Asiedu on 2016/11/14.
//
#include "npuzzle.h"
#include <time.h>

vector<State *>     openState;
vector<State *>     closedState;
time_t startTime;

int     idaOpenState;
int     idaClosedState;
int     greedySearch;

bool    sortByTotalCost(State *obj1, State *obj2){
    return obj1->getTotalCost() < obj2->getTotalCost();
}

bool    isPuzzleSame(int **puzzle1, int **puzzle2, int size){
    for (int i = 0; i < size; i++){
        for (int j = 0; j < size; j++){
            if (puzzle1[i][j] != puzzle2[i][j])
                return false;
        }
    }
    return true;
}

State   *movePieceIda(int emptyX, int emptyY, int x, int y, State *state, int move){
    int **puzzle = state->getCurrentPuzzle();
    int **newPuzzle;

    newPuzzle = (int **)malloc(sizeof(int *) * state->getSize());
    for (int i = 0; i < state->getSize(); i++){
        newPuzzle[i] = (int *)malloc(sizeof(int) * state->getSize());
        for (int j = 0; j < state->getSize(); j++){
            newPuzzle[i][j] = puzzle[i][j];
        }
    }
    newPuzzle[emptyY][emptyX] = newPuzzle[y][x];
    newPuzzle[y][x] = 0;
    State *temp = new State(state->getEndPuzzle(), state->getSize(), state->getHeuristicType(), false);
    temp->addCurrentPuzzle(newPuzzle, state->getStepsCost() + 1, move);
    temp->addCameFrom(state);
    return temp;
}

void    movePiece(int emptyX, int emptyY, int x, int y, State *state, int move){
    int **puzzle = state->getCurrentPuzzle();
    int **newPuzzle;

    newPuzzle = (int **)malloc(sizeof(int *) * state->getSize());
    for (int i = 0; i < state->getSize(); i++){
        newPuzzle[i] = (int *)malloc(sizeof(int) * state->getSize());
        for (int j = 0; j < state->getSize(); j++){
            newPuzzle[i][j] = puzzle[i][j];
        }
    }
    newPuzzle[emptyY][emptyX] = newPuzzle[y][x];
    newPuzzle[y][x] = 0;
    State *temp = new State(state->getEndPuzzle(), state->getSize(), state->getHeuristicType(), false);
    if (greedySearch == 0)
        temp->addCurrentPuzzle(newPuzzle, state->getStepsCost() + 1, move);
    else if (greedySearch == 1)
        temp->addCurrentPuzzle(newPuzzle, 0, move);
    temp->addCameFrom(state);
    for (int ind = 0; ind < openState.size(); ind++){
        if (isPuzzleSame(openState[ind]->getCurrentPuzzle(), newPuzzle, state->getSize())){
            if (openState[ind]->getTotalCost() > temp->getTotalCost()){
                openState[ind] = temp;
                return;
            }
        }
    }
    for (int ind = 0; ind < closedState.size(); ind++){
        if (isPuzzleSame(closedState[ind]->getCurrentPuzzle(), newPuzzle, state->getSize())){
            if (closedState[ind]->getTotalCost() > temp->getTotalCost()){
                closedState[ind] = temp;
                openState.push_back(closedState[ind]);
                closedState.erase(closedState.begin() + ind);
                return;
            }
        }
    }
    openState.push_back(temp);
}

State   *getLowestCost(){
    State *current;

    sort(openState.begin(), openState.end(), sortByTotalCost);
    int lowestCost = openState[0]->getTotalCost();
    for (int i = 0; i < openState.size(); i++){
        if (openState[i]->getTotalCost() == lowestCost){
            current = openState[i];
            openState.erase(openState.begin()+i);
            return current;
        }
    }
    return NULL;
}

void    aStarAlgorithm(State *initialState, int greedy){
    State   *current;
    int     y;
    int     x;

    greedySearch = greedy;
    current = initialState;
    openState.push_back(initialState);
    startTime = time(NULL);
    if (greedy == 0)
        printf(MAGENTA "starting A* Algorithm\n" RESET);
    else
        printf(MAGENTA "starting Greedy Search Algorithm\n" RESET);
    while (openState.size() > 0){
                current = getLowestCost();
                closedState.push_back(current);
                if (current->solvedState()){
                    time_t endTime = time(NULL);
                    double diff = (double)(endTime - startTime);
                    cout << MAGENTA << "Program finished in " << YELLOW << diff << " seconds" << RESET << endl;
                    break ;
                }
                current->getEmptySlot(&x, &y);
                if (x - 1 >= 0 && current->lastMove != RIGHT)
                    movePiece(x, y, x - 1, y, current, LEFT);
                if (x + 1 < current->getSize() && current->lastMove != LEFT)
                    movePiece(x, y, x + 1, y, current, RIGHT);
                if (y - 1 >= 0 && current->lastMove != DOWN)
                    movePiece(x, y, x, y - 1, current, UP);
                if (y + 1 < current->getSize() && current->lastMove != UP)
                    movePiece(x, y, x, y + 1, current, DOWN);
    }
    cout << MAGENTA << "OpenState size : " << YELLOW << openState.size() << RESET << endl;
    cout << MAGENTA << "ClosedState size : " << YELLOW << closedState.size() << RESET << endl;
    cout << MAGENTA << "Total States : " << YELLOW << openState.size() + closedState.size() << RESET << endl;
    cout << MAGENTA << "final total cost : " << YELLOW << current->getTotalCost() << RESET << endl;
    cout << MAGENTA << "number of steps : " << YELLOW << current->getStepsCost() << RESET << endl;
    int     i = 0;
    while (!current->start){
        current->printCurrentPuzzle();
        cout << endl;
        current = current->getCameFrom();
        i++;
    }
    current->printCurrentPuzzle();
    cout << GREEN << "number of steps : " << YELLOW << i << RESET << endl;
    openState.clear();
    closedState.clear();
}

int    idaSearch(State *node, int stepCost, int heuristicBound){
    int  totalCost = node->getTotalCost();
    if (totalCost > heuristicBound){idaOpenState++; return totalCost;}
    idaClosedState++;
    if(node->solvedState()){
        time_t endTime = time(NULL);
        double diff = (double)(endTime - startTime);
        cout << MAGENTA << "Program finished in " << YELLOW << diff << " seconds" << RESET << endl;
        node->addCameFrom(NULL);
        return FOUND;
    }
    int min = numeric_limits<int>::max();
    int x, y;
    node->getEmptySlot(&x, &y);
    State *temp;
    if (x - 1 >= 0 && node->lastMove != RIGHT){
        temp = movePieceIda(x, y, x - 1, y, node, LEFT);
        int totalCost2 = idaSearch(temp, stepCost + 1, heuristicBound);
        if (totalCost2 == -1){ node->addCameFrom(temp);return FOUND; }
        if (totalCost2 < min)min = totalCost2;
    }
    if (x + 1 < node->getSize() && node->lastMove != LEFT){
        temp = movePieceIda(x, y, x + 1, y, node, RIGHT);
        int totalCost2 = idaSearch(temp, stepCost + 1, heuristicBound);
        if (totalCost2 == -1){ node->addCameFrom(temp);return FOUND; }
        if (totalCost2 < min)min = totalCost2;
    }
    if (y - 1 >= 0 && node->lastMove != DOWN){
        temp = movePieceIda(x, y, x, y - 1, node, UP);
        int totalCost2 = idaSearch(temp, stepCost + 1, heuristicBound);
        if (totalCost2 == -1){ node->addCameFrom(temp);return FOUND; }
        if (totalCost2 < min)min = totalCost2;
    }
    if (y + 1 < node->getSize() && node->lastMove != UP){
        temp = movePieceIda(x, y, x, y + 1, node, DOWN);
        int totalCost2 = idaSearch(temp, stepCost + 1, heuristicBound);
        if (totalCost2 == -1){ node->addCameFrom(temp);return FOUND; }
        if (totalCost2 < min)min = totalCost2;
    }
    return min;
}

void    idaStarAlgorithm(State *initialState){
    int     idaBound = initialState->getHeuristicCost();

    printf(MAGENTA "starting IDA* Algorithm\n" RESET);
    cout << MAGENTA << "ida bound -> " << BLUE << idaBound << RESET << endl;
    startTime = time(NULL);
    while (1){
        idaClosedState = 0;
        idaOpenState = 0;
        int searchResult = idaSearch(initialState, 0, idaBound);
        if (searchResult == -1){ break; }
        if (searchResult == -2){ cout << "could not solve puzzle" << endl; break;}
        idaBound = searchResult;
    }
    cout << MAGENTA << "OpenStates : " << YELLOW << idaOpenState << RESET << endl;
    cout << MAGENTA << "ClosedStates : " << YELLOW << idaClosedState << RESET << endl;
    cout << MAGENTA << "Total States : " << YELLOW << idaClosedState + idaOpenState << RESET << endl;
    int i = 0;
    State *final = initialState;
    while (final != NULL){
        cout << GREEN << "step : " << YELLOW << i << RESET << endl;
        final->printCurrentPuzzle();
        cout << endl;
        final = final->getCameFrom();
        i++;
    }
}
