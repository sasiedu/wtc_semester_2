# npuzzle
The goal of this project is to solve the N-puzzle ("taquin" in French) game using the A* search algorithm or one of its variants. &lt;br /> I implemented bot the A* search, an A* variant IDA* search as well as the greedy search &lt;br /> The heuristics used are : manhattan distance,  euclidean distance, tiles out of row&amp;&amp; column, misplaced tiles and linear conflicts
