//
// Created by Stephen ASIEDU on 2016/11/14.
//

#include "npuzzle.h"

int getSpiralIndex(int i, int j, int size) {
    int x = min(i, size - 1 - i);
    int y = min(j, size - 1 - j);
    return min(x,y);
}

int getNumber(int i, int j, int size) {
    int k = getSpiralIndex(i, j, size);
    int start = 4 * k * (size - k) + 1;
    int offset = 0;
    if (i == k) {
        offset += (j - k);
    } else {
        offset += (size - 1 - k - k);
        if (j == size - 1 - k) {
            offset += (i - k);
        } else {
            offset += (size - 1 - k - k);
            if (i == size - 1 - k) {
                offset += (size - 1 - k - j);
            } else {
                offset += (size - 1 - k - k);
                offset += (size - 1 - k - i);
            }
        }
    }
    return start + offset;
}

int     **generateEndPuzzle(int size){
    int     **puzzle;

    puzzle = (int **)malloc(sizeof(int *) * size);
    for (int i = 0; i < size; i++){
        puzzle[i] = (int *)malloc(sizeof(int) * size);
        for (int j = 0; j < size; j++){
            puzzle[i][j] = getNumber(i, j, size);
            if (puzzle[i][j] == size * size)
                puzzle[i][j] = 0;
        }
    }
    return puzzle;
}


bool    isNumber(const char *str){
    for(int i = 0; i < strlen(str); i++){
        if (str[i] != ' ' && isdigit(str[i]) == 0)
            return false;
    }
    return true;
}