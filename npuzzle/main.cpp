#include <iostream>
#include "npuzzle.h"

using namespace std;

int     size = -1;
int     **startState;

void    addPuzzleLine(const char *str, int y){
    int j = 0;
    char *t1 = strdup(str);

    char *temp = strtok(t1, " ");
    while (temp != 0){
        j++;
        temp = strtok(NULL, " ");
    }
    delete [] t1;
    if (j > size){cout << "invalid puzzle. line exceeds size : " << str << endl; exit(2);}
    if (j < size){cout << "invalid puzzle. line is less than size : " << str << endl; exit(2);}
    if(y == 0)
        startState = (int **)malloc(sizeof(int *) * size);
    startState[y] = (int *)malloc(sizeof(int) * size);
    char *value = strtok((char *)str, " ");
    for(int x = 0; x < size; x++){
        startState[y][x] = atoi(value);
        value = strtok(NULL, " ");
    }
    for(int x = 0; x < size; x++){
        cout << startState[y][x] << " ";
    }
    cout << endl;
}

void    readFile(int ac, char **av){
    string  line;
    int     y = 0;

    if (ac < 2){
        cout << "Too few arguments. Run ./npuzzle [puzzle].txt" << endl;
        exit(2);
    }
    if (ac > 2){
        cout << "Too many arguments. Run ./npuzzle [puzzle].txt" << endl;
        exit(2);
    }
    ifstream myfile(av[1]);
    if (myfile.is_open()) {
        while (getline(myfile, line)){
            line = line.substr(0, line.find("#"));
            if (line.size() < 1)
                continue ;
            line = std::regex_replace(line, std::regex("^ +"), "");
            if (line.find(' ') == string::npos){
                if (size != -1){cout << "invalid puzzle. dupilcate puzzle size." << endl; exit(2);}
                if (!isNumber(line.c_str())){cout << "invalid puzzle size." << endl; exit(2);}
                size = atoi(line.c_str());
                cout << "puzzle size : " << size << endl;
            }else{
                if (size == -1){cout << "invalid puzzle. no puzzle size." << endl; exit(2);}
                if (!isNumber(line.c_str())){cout << "invalid puzzle line : " << line << endl; exit(2);}
                addPuzzleLine(line.c_str(), y);
                y++;
            }
        }
        myfile.close();
    }else
        cout << "Unable to open file : " << av[1] << endl;
    if (y != size){
        cout << "invalid puzzle. number of puzzle lines is not the same as size" << endl;
        exit(2);
    }
}

void    aStarOriginalType(){
    int type = 1;
    char c;

    printf(BLUE "select one of the heuristics below :\n" RESET);
    printf(GREEN "Press 1 for Manhattan Distance\n" RESET);
    printf(GREEN "Press 2 for Misplaced Tiles\n" RESET);
    printf(GREEN "Press 3 for Tiles out of Row and Column\n" RESET);
    printf(GREEN "Press 4 for Euclidean Distance\n" RESET);
    read(0, &c, 1);
    while (read(0, &c, 1)){
        if (c == '1' || c == '2' || c == '3' || c == '4'){
            type = atoi(&c);
            break ;
        }else {
            printf(RED "invalid heuristic number\n" RESET);
        }
    }
    State *first = new State(generateEndPuzzle(size), size, type, true);
    printf(BLUE "End Puzzle\n" RESET);
    first->printEndPuzzle();
    cout << endl;
    first->addCurrentPuzzle(startState, 0, NONE);
    aStarAlgorithm(first, 0);
}

void    aStarGreedyType(){
    int type = 1;
    char c;

    printf(BLUE "select one of the heuristics below :\n" RESET);
    printf(GREEN "Press 1 for Manhattan Distance\n" RESET);
    printf(GREEN "Press 2 for Misplaced Tiles\n" RESET);
    printf(GREEN "Press 3 for Tiles out of Row and Column\n" RESET);
    printf(GREEN "Press 4 for Euclidean Distance\n" RESET);
    read(0, &c, 1);
    while (read(0, &c, 1)){
        if (c == '1' || c == '2' || c == '3' || c == '4'){
            type = atoi(&c);
            break ;
        }else {
            printf(RED "invalid heuristic number\n" RESET);
        }
    }
    State *first = new State(generateEndPuzzle(size), size, type, true);
    printf(BLUE "End Puzzle\n" RESET);
    first->printEndPuzzle();
    cout << endl;
    first->addCurrentPuzzle(startState, 0, NONE);
    aStarAlgorithm(first, 1);
}

void    idaStarType(){
    int type = 1;
    char c;

    printf(BLUE "select one of the heuristics below :\n" RESET);
    printf(GREEN "Press 1 for Manhattan Distance\n" RESET);
    printf(GREEN "Press 2 for Misplaced Tiles\n" RESET);
    printf(GREEN "Press 3 for Tiles out of Row and Column\n" RESET);
    printf(GREEN "Press 4 for Euclidean Distance\n" RESET);
    read(0, &c, 1);
    while (read(0, &c, 1)){
        if (c == '1' || c == '2' || c == '3' || c == '4'){
            type = atoi(&c);
            break ;
        }else {
            printf(RED "invalid heuristic number\n" RESET);
        }
    }
    State *first = new State(generateEndPuzzle(size), size, type, true);
    printf(BLUE "End Puzzle\n" RESET);
    first->printEndPuzzle();
    cout << endl;
    first->addCurrentPuzzle(startState, 0, NONE);
    idaStarAlgorithm(first);
}

int     main(int ac, char **av) {
    int     type = 1;
    char    c;

    readFile(ac, av);
    if (!isPuzzleSolvable(startState, size)){
        printf(RED "===Not Solvable===\n" RESET);
        exit(1);
    }
    printf(GREEN "===Solvable===\n" RESET);
    printf(BLUE "select one of the A* algorithm below :\n" RESET);
    printf(GREEN "Press 1 for A star Original\n" RESET);
    printf(GREEN "Press 2 for IDA star\n" RESET);
    printf(GREEN "Press 3 for Greedy search\n" RESET);
    while (read(0, &c, 1)){
        if (c == '1' || c == '2' || c == '3'){
            type = atoi(&c);
            break ;
        }else {
            printf(RED "invalid Algorithm number\n" RESET);
        }
    }
    if (type == 1)
        aStarOriginalType();
    else if (type == 2)
        idaStarType();
    else if (type == 3)
        aStarGreedyType();
    for (int i = 0; i < size; i++){
        delete[] startState[i];
    }
    delete[] startState;
    return 0;
}