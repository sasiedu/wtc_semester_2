package gameGUI;

import BoardChecks.*;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.sql.Time;

/*
 * In this program the 2d array is in the format[x][y]
 */
public class Grid extends JPanel {

    public Game ParentGame;

    public Box[][] Gameboard;
    /*
     * this is the logic board, it contains :
     * 0 to represent empty box
     * 1 to represent occupied player1 piece
     * 2 to represent occupied player2 piece
     */
    public int[][] logicBoard;
    //holds the player whose turn is to play
    public Player currentPlayer;

    public Grid(Game game) {
        ParentGame = game;
        createGrid();
        currentPlayer = ParentGame.PlayerOne;
    }

    private void createGrid() {
        setLayout(null);
        setBackground(Color.GRAY);
        setBounds(120, 70, 760, 760);
        //this is the board for create the box buttons
        Gameboard = new Box[19][19];
        logicBoard = new int[19][19];
        for (int y = 0; y < 19; y++) {
            for (int x = 0; x < 19; x++) {
                logicBoard[x][y] = 0;
                Gameboard[x][y] = new Box(x, y, 40, 40, this);
            }
        }
        ParentGame.boardPanel.add(this);
    }

    public void changePlayer(int x, int y) {
        currentPlayer = (currentPlayer == ParentGame.PlayerOne)
                ? ParentGame.PlayerTwo : ParentGame.PlayerOne;
        //printBoard();
        ParentGame.sideBar.updateSide();
        prePareBoardForPlayer();
        currentPlayer.startTime = System.nanoTime();
        if (currentPlayer.getIsPlayerAI())
            aiIsPlaying();
    }

    private void printBoard() {
        for (int y = 0; y < 19; y++) {
            for (int x = 0; x < 19; x++) {
                System.out.print(logicBoard[x][y] + " ");
            }
            System.out.println();
        }
    }

    public void prePareBoardForPlayer() {
        //Checker checker = new Checker();
        BoardChecker checker = new BoardChecker();

        for (int y = 0; y < 19; y++) {
            for (int x = 0; x < 19; x++) {
                Gameboard[x][y].box.setEnabled(checker.isValidMove(logicBoard,
                        currentPlayer.getPlayerNumber(), x, y));
                Gameboard[x][y].box.setOpaque(false);
                Gameboard[x][y].box.setBackground(Color.WHITE);
            }
        }
    }

    public void     aiIsPlaying(){
        String ans = currentPlayer.theAI.play(logicBoard);
        currentPlayer.stopTime = System.nanoTime();
        String[] divde = ans.split(",");
        int x = Integer.parseInt(divde[0]);
        int y = Integer.parseInt(divde[1]);
        Gameboard[x][y].actionByAi();
    }
}
