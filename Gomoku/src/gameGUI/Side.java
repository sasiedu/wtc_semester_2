package gameGUI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;

public class Side implements ActionListener {

    private Game    game;
    private JPanel  sidePanel;

    public JLabel   pOneHeader;
    public JLabel   pOneCatch;
    public JLabel   pOneTime;
    public JLabel   pTwoHeader;
    public JLabel   pTwoCatch;
    public JLabel   pTwoTime;
    public JLabel   whoPlay;
    private JButton HINT;

    public Side(Game g, JPanel panel){
        game = g;
        sidePanel = panel;
        createPanel();
    }

    public void actionPerformed(ActionEvent event){
        String ans = game.theGrid.currentPlayer.theAI.play(game.theGrid.logicBoard);
        String[] divde = ans.split(",");
        int x = Integer.parseInt(divde[0]);
        int y = Integer.parseInt(divde[1]);
        game.theGrid.Gameboard[x][y].box.setBackground(Color.GREEN);
        game.theGrid.Gameboard[x][y].box.setOpaque(true);
    }

    private void    createPanel(){
        pOneHeader = new JLabel("Player 1");
        pOneHeader.setFont(new Font("Verdana", Font.BOLD, 40));
        pOneHeader.setForeground(Color.RED);
        pOneHeader.setBounds(50, 50, 250, 50);
        sidePanel.add(pOneHeader);
        //
        pOneCatch = new JLabel("Pieces Captured: 0");
        pOneCatch.setFont(new Font("Verdana", Font.BOLD, 20));
        pOneCatch.setBounds(50, 100, 250, 50);
        sidePanel.add(pOneCatch);
        //
        pOneTime = new JLabel("Time Taken: 0secs");
        pOneTime.setFont(new Font("Verdana", Font.BOLD, 20));
        pOneTime.setBounds(50, 150, 300, 50);
        sidePanel.add(pOneTime);
        //
        JLabel   pOneAi;
        pOneAi = new JLabel("Is Player Ai: ");
        pOneAi.setFont(new Font("Verdana", Font.BOLD, 20));
        pOneAi.setBounds(50, 200, 150, 50);
        sidePanel.add(pOneAi);
        JLabel tmp;
        if (game.PlayerOne.getIsPlayerAI())
            tmp = new JLabel(" True");
        else
            tmp = new JLabel(" False");
        tmp.setFont(new Font("Verdana", Font.BOLD, 20));
        tmp.setForeground(Color.RED);
        tmp.setBounds(190, 200, 100, 50);
        sidePanel.add(tmp);

        pTwoHeader = new JLabel("Player 2");
        pTwoHeader.setFont(new Font("Verdana", Font.BOLD, 40));
        pTwoHeader.setForeground(Color.BLUE);
        pTwoHeader.setBounds(50, 400, 250, 50);
        sidePanel.add(pTwoHeader);
        //
        pTwoCatch = new JLabel("Pieces Captured: 0");
        pTwoCatch.setFont(new Font("Verdana", Font.BOLD, 20));
        pTwoCatch.setBounds(50, 450, 250, 50);
        sidePanel.add(pTwoCatch);
        //
        pTwoTime = new JLabel("Time Taken: 0secs");
        pTwoTime.setFont(new Font("Verdana", Font.BOLD, 20));
        pTwoTime.setBounds(50, 500, 300, 50);
        sidePanel.add(pTwoTime);
        //
        JLabel   pTwoAi;
        pTwoAi = new JLabel("Is Player Ai: ");
        pTwoAi.setFont(new Font("Verdana", Font.BOLD, 20));
        pTwoAi.setBounds(50, 550, 150, 50);
        sidePanel.add(pTwoAi);
        if (game.PlayerTwo.getIsPlayerAI())
            tmp = new JLabel(" True");
        else
            tmp = new JLabel(" False");
        tmp.setFont(new Font("Verdana", Font.BOLD, 20));
        tmp.setForeground(Color.BLUE);
        tmp.setBounds(190, 550, 100, 50);
        sidePanel.add(tmp);

        whoPlay = new JLabel("Player One's Turn");
        whoPlay.setFont(new Font("Verdana", Font.BOLD, 30));
        whoPlay.setForeground(Color.RED);
        whoPlay.setBounds(10, 700, 330, 50);
        sidePanel.add(whoPlay);

        HINT = new JButton("HINT");
        HINT.setFont(new Font("Verdana", Font.BOLD, 30));
        HINT.setForeground(Color.BLACK);
        HINT.setBackground(Color.GREEN);
        HINT.setOpaque(true);
        HINT.addActionListener(this);
        HINT.setBounds(60, 780, 150, 50);
        sidePanel.add(HINT);
    }

    public void updateSide(){
        DecimalFormat df = new DecimalFormat("#.000");

        if (game.theGrid.currentPlayer.getPlayerNumber() == 1){
            whoPlay.setText("Player One's Turn");
            whoPlay.setForeground(Color.RED);
        }else{
            whoPlay.setText("Player Two's Turn");
            whoPlay.setForeground(Color.BLUE);
        }
        pOneCatch.setText("Pieces Captured: "+game.PlayerOne.getCatches());
        pTwoCatch.setText("Pieces Captured: "+game.PlayerTwo.getCatches());
        float p1Time = (float)((game.PlayerOne.stopTime - game.PlayerOne.startTime) / 1000000000);
        float p2Time = (float)((game.PlayerTwo.stopTime - game.PlayerTwo.startTime) / 1000000000);
        pOneTime.setText("Time Taken: "+df.format(p1Time)+"secs");
        pTwoTime.setText("Time Taken: "+df.format(p2Time)+"secs");
    }
}