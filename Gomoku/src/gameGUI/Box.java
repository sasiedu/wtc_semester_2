package gameGUI;

import BoardChecks.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Box implements ActionListener {
    public int      PosX;
    public int      PosY;
    public boolean  isEmpty = true; //is to prevent overriding box with pieces
    private int     Boxwidth;
    private int     Boxheight;
    private Grid    ParentGrid;
    public JButton   box;

    public Box(int x, int y, int width, int height, Grid grid){
        PosX = x;//the x index assigned to the box
        PosY = y; //the y index assigned to the box
        Boxwidth = width;
        Boxheight = height;
        ParentGrid = grid;//the whole grid
        createBox();
    }

    public void     paintComponent(Graphics g){
        g.drawRect(0, 0, Boxwidth, Boxheight);
    }

    private void    createBox(){
        box = new JButton("");
        box.setBounds(PosY * Boxwidth, PosX * Boxheight, Boxwidth, Boxheight);
        box.setLayout(null);
        box.addActionListener(this);
        ParentGrid.add(box);
    }

    public void actionPerformed(ActionEvent event){
        if (!ParentGrid.currentPlayer.getIsPlayerAI())
            ParentGrid.currentPlayer.stopTime = System.nanoTime();
        box.add(new Piece(ParentGrid.currentPlayer.getPlayerColor(),
                Boxwidth, Boxheight));
        box.setEnabled(false);
        isEmpty = false;
        ParentGrid.logicBoard[PosX][PosY] =
                ParentGrid.currentPlayer.getPlayerNumber();
        checkForCapture();
        checkForLine();
        ParentGrid.changePlayer(PosX, PosY);
    }

    public void actionByAi(){
        System.out.println("AI played");
        box.add(new Piece(ParentGrid.currentPlayer.getPlayerColor(),
                Boxwidth, Boxheight));
        box.setEnabled(false);
        isEmpty = false;
        ParentGrid.logicBoard[PosX][PosY] =
                ParentGrid.currentPlayer.getPlayerNumber();
        checkForCapture();
        checkForLine();
        ParentGrid.changePlayer(PosX, PosY);
    }

    private void checkForLine(){
        BoardChecker checker = new BoardChecker();

        if (checker.isLineVictory(ParentGrid.logicBoard,
                ParentGrid.currentPlayer.getPlayerNumber())){
            if (furtherLineCheck())
                checkWinByLine();
        }
    }

    private boolean    furtherLineCheck(){
        BoardChecker checker = new BoardChecker();
        int opp = (ParentGrid.currentPlayer.getPlayerNumber() == 1) ? 2 : 1;

        if (opp == 1){
            if (ParentGrid.ParentGame.PlayerOne.getCatches() < 8)
                return true;
        }else{
            if (ParentGrid.ParentGame.PlayerTwo.getCatches() < 8)
                return true;
        }
        for (int y = 0; y < 19; y++){
            for (int x = 0; x < 19; x++){
                if (ParentGrid.logicBoard[x][y] == 0){
                   String tmp =  checker.isCapture(ParentGrid.logicBoard, opp, x, y);
                   if (tmp.length() > 0)
                       return false;
                }
            }
        }
        return true;
    }

    private void    checkWinByLine(){
        JOptionPane tmp;

        tmp = new JOptionPane();
        tmp.showMessageDialog(null,"Player "+
                 ParentGrid.currentPlayer.getPlayerNumber()
                    + " wins by Line");
        tmp.setFont(new Font("Verdana", Font.BOLD, 30));
        tmp.setForeground(ParentGrid.currentPlayer.getPlayerColor());
        System.exit(0);
    }

    private void checkForCapture(){
        BoardChecker checker = new BoardChecker();

        String captures = checker.isCapture(ParentGrid.logicBoard,
                ParentGrid.currentPlayer.getPlayerNumber(), PosX, PosY);
        System.out.println("In capture : "+captures);
        if (captures.length() > 0){
            for(int i = 0; i < captures.length(); i+=2){
                if (captures.charAt(i) == '2'){
                    captureChange(PosX+1, PosY);
                    captureChange(PosX+2, PosY);
                    ParentGrid.currentPlayer.addToCatch(2);
                }else if (captures.charAt(i) == '6'){
                    captureChange(PosX-1, PosY);
                    captureChange(PosX-2, PosY);
                    ParentGrid.currentPlayer.addToCatch(2);
                }else if (captures.charAt(i) == '8'){
                    captureChange(PosX, PosY-1);
                    captureChange(PosX, PosY-2);
                    ParentGrid.currentPlayer.addToCatch(2);
                }else if (captures.charAt(i) == '4'){
                    captureChange(PosX, PosY+1);
                    captureChange(PosX, PosY+2);
                    ParentGrid.currentPlayer.addToCatch(2);
                }else if (captures.charAt(i) == '1'){
                    captureChange(PosX+1, PosY-1);
                    captureChange(PosX+2, PosY-2);
                    ParentGrid.currentPlayer.addToCatch(2);
                }else if (captures.charAt(i) == '3'){
                    captureChange(PosX+1, PosY+1);
                    captureChange(PosX+2, PosY+2);
                    ParentGrid.currentPlayer.addToCatch(2);
                }else if (captures.charAt(i) == '5'){
                    captureChange(PosX-1, PosY+1);
                    captureChange(PosX-2, PosY+2);
                    ParentGrid.currentPlayer.addToCatch(2);
                }else if (captures.charAt(i) == '7'){
                    captureChange(PosX-1, PosY-1);
                    captureChange(PosX-2, PosY-2);
                    ParentGrid.currentPlayer.addToCatch(2);
                }
            }
            checkWinByCapture();
        }
    }

    private void    captureChange(int x, int y){
        ParentGrid.Gameboard[x][y].box.setEnabled(true);
        ParentGrid.Gameboard[x][y].box.removeAll();
        ParentGrid.logicBoard[x][y] = 0;
    }

    private void    checkWinByCapture(){
        if (ParentGrid.currentPlayer.getCatches() >= 10){
            JOptionPane tmp;

            tmp = new JOptionPane();
            tmp.showMessageDialog(null,"Player "+
                    ParentGrid.currentPlayer.getPlayerNumber()
                    + " wins by Capture");
            tmp.setFont(new Font("Verdana", Font.BOLD, 30));
            tmp.setForeground(ParentGrid.currentPlayer.getPlayerColor());
            System.exit(0);
        }
    }
}
