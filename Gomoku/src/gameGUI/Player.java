package gameGUI;

import AI.GomokuAI;
import AI.GomokuAI.*;

import java.awt.*;
import java.sql.Time;

public class Player {
    private int      playerNo;
    private Color    playerColor;
    private boolean  isPlayerAI;
    private int      catches;
    public GomokuAI  theAI;
    public float      startTime = 0.0f;
    public float      stopTime = 0.0f;

    public Player(int No, Color color, boolean isAI, String level){
        playerNo = No;
        playerColor = color;
        isPlayerAI = isAI;
        catches = 0;
        if (!isAI)
            No = 2;
        if (level == "easy")
            theAI = new GomokuAI(No, 1, 2);
        else if (level == "medium")
            theAI = new GomokuAI(No, 2, 3);
        else
            theAI = new GomokuAI(No, 3, 3);
    }

    public int getPlayerNumber(){
        return playerNo;
    }

    public Color getPlayerColor(){
        return playerColor;
    }

    public boolean getIsPlayerAI(){
        return isPlayerAI;
    }

    public void addToCatch(int number){
        catches += number;
    }

    public int getCatches(){
        return catches;
    }
}
