<?php

include ("user_tools.php");

if (isset($_REQUEST['user']))
{
    include ($_SERVER['DOCUMENT_ROOT'] . "/camagru/config/database.php");

    $token = token_generate(45);
    $username = $_REQUEST['user'];
    try {
        $conn = new PDO($DB_DSN, $DB_USER, $DB_PASSWORD);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $str = "SELECT * FROM `users` WHERE `username` = :user";
        $stmt = $conn->prepare($str);
        $stmt->bindParam(':user', $username);
        $stmt->execute();

        foreach ($stmt as $user)
        {
            if ($user['username'] == $username)
            {
                reset_pass_mail($username, $user['fullname'], $user['email'], $token);
                $str = "UPDATE `users` SET `verification_token` = :token WHERE `username` = :username";
                $stmt = $conn->prepare($str);
                $stmt->bindParam(':token', $token);
                $stmt->bindParam(':username', $username);
                $stmt->execute();
                echo "found";
                return 1;
            }
        }
        echo "not found";
    }
    catch (PDOException $e)
    {
        echo "conn failed";
    }
    $conn = null;
}

?>