<?php

session_start();

?>

<head xmlns="http://www.w3.org/1999/html">
    <link rel="stylesheet" type="text/css" href="/camagru/css/glob.css" />
    <link rel="stylesheet" type="text/css" href="/camagru/css/front.css" />
    <link rel="stylesheet" type="text/css" href="/camagru/css/dash.css" />
    <link rel="stylesheet" type="text/css" href="/camagru/css/dash_page.css" />
    <link rel="stylesheet" type="text/css" href="/camagru/css/camera.css" />
    <link rel="stylesheet" type="text/css" href="/camagru/css/phone.css" />
    <link rel="stylesheet" type="text/css" href="/camagru/css/tab1.css" />
    <link rel="stylesheet" type="text/css" href="/camagru/css/tab2.css" />
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <script src="/camagru/script/java.js"></script>
    <script src="/camagru/script/main.js"></script>
    <script src="/camagru/script/camera.js"></script>
    <script src="/camagru/script/cam.js"></script>
</head>
<body onload="startup();" >
    <div id="camera_zone">
        <div id="cam1">
            <video id="video" style="width: 100%; height: 100%;">Video stream not available.</video>
        </div>
        <div id="cam2">
            <img id="cam2img" name="temp" src="/camagru/images/demo2.jpeg" />
        </div>
        <div id="cam3">
            <img id="photo" src="/camagru/images/demo2.jpeg" />
        </div>
        <div id="cam11">
            <div style="text-align: center;">
                <button id="snap" class="cam-button" onclick="snap();">Capture</button>
                <span>
                    <button class="cam-button" style="margin-left: 70px; overflow: hidden;" >Upload
                        <input class="upload_but" type="file" name="fileupload" accept="image/*" onchange="readimage(this);" />
                    </button>
                </span>
            </div>
        </div>
        <div id="cam22" style="display: none;">
            <div style="text-align: center;">
                <button class="cam-button" onclick="display_camera()">Camera</button>
                <span>
                    <button class="cam-button" style="margin-left: 70px;" >Edit</button>
                </span>
                <span>
                    <button class="cam-button" style="margin-left: 70px;" onclick="delete_image();">Delete</button>
                </span>
            </div>
        </div>
        <div id="cam33" style="display: none;">
            <div style="text-align: center;">
                <button class="cam-button" onclick="discard(this);">Discard</button>
                <span>
                    <button class="cam-button" style="margin-left: 70px;" onclick="save_img();">Save</button>
                </span>
            </div>
        </div>
        <canvas id="canvas" style="display: none;">
        </canvas>
    </div>
</body>