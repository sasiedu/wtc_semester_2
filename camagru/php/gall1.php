<?php

include ("gallery_tools.php");

session_start();

include ($_SERVER['DOCUMENT_ROOT'] . "/camagru/config/database.php");

try {
    $conn = new PDO($DB_DSN, $DB_USER, $DB_PASSWORD);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $str = "SELECT * FROM `images`";
    $stmt = $conn->prepare($str);
    $stmt->execute();
    $_SESSION['image_total'] = $stmt->rowCount();
    $i = 0;
    foreach ($stmt as $tmp){
        ?>
        <article>
            <?php build_gallery($tmp, $conn); ?>
        </article>
<?php
        $i++;
        if ($i > 5)
            break ;
    }
    $_SESSION['image_index'] = $i;
}
catch (PDOException $e)
{
    echo "conn failed" . $e;
}
$conn = null;

?>