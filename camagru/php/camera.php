<?php

?>

<script>
    document.getElementById("profile").className = "dash-button";
    document.getElementById("camera").className = "dash-button dash-button_selected";
    document.getElementById("gallery").className = "dash-button";
</script>

<article id="camera1" class="user_profile clearfix">
    <div class="user_profile_header" style="text-align: center;">
        <iframe id="frame1" width="90%" src="/camagru/php/lay_over.php" class="lay_over" scrolling="auto"></iframe>
        <div style="margin-top: 5px;">
            <iframe id="frame2" width="60%" height="80%" src="/camagru/php/cam.php" scrolling="no" frameborder="0"></iframe>
            <iframe id="frame3" width="270px" height="80%" style="margin-left: 20px;" src="/camagru/php/cam_pic.php"></iframe>
        </div>
        <div id="mobile_frame" class="mobile_frame">
            <button class="cam-button new_cam_butt" onclick="iframe1_button();">frames</button>
            <span style="background-color: #efefef;">
                <button class="cam-button new_cam_butt" onclick="iframe2_button();">photo</button>
            </span>
            <span style="background-color: #efefef;">
                <button class="cam-button new_cam_butt" onclick="iframe3_button();">images</button>
            </span>
        </div>
    </div>
</article>