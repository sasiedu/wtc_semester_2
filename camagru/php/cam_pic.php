<?php

session_start();

?>

<head xmlns="http://www.w3.org/1999/html">
    <link rel="stylesheet" type="text/css" href="/camagru/css/glob.css" />
    <link rel="stylesheet" type="text/css" href="/camagru/css/front.css" />
    <link rel="stylesheet" type="text/css" href="/camagru/css/dash.css" />
    <link rel="stylesheet" type="text/css" href="/camagru/css/dash_page.css" />
    <link rel="stylesheet" type="text/css" href="/camagru/css/camera.css" />
    <link rel="stylesheet" type="text/css" href="/camagru/css/phone.css" />
    <link rel="stylesheet" type="text/css" href="/camagru/css/tab1.css" />
    <link rel="stylesheet" type="text/css" href="/camagru/css/tab2.css" />
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <script src="/camagru/script/java.js"></script>
    <script src="/camagru/script/main.js"></script>
    <script src="/camagru/script/camera.js"></script>

</head>
<body style="text-align: center; background-color: #fff;">
<div id="side_side" style="text-align: center">
    <div class="campics">
        <?php
        include ($_SERVER['DOCUMENT_ROOT'] . "/camagru/config/database.php");

        /*$username = $_SESSION['username'];

        try {
            $conn = new PDO($DB_DSN, $DB_USER, $DB_PASSWORD);
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $str = "SELECT * FROM `images` WHERE `username` LIKE :user";
            $stmt = $conn->prepare($str);
            $stmt->bindParam(':user', $username);
            $stmt->execute();
            foreach ($stmt as $img)
            {*/
        if (isset($_SESSION['sidepics']))
        {
            $images = $_SESSION['sidepics'];
            $dates = $_SESSION['dates'];
            $i = 0;

            foreach ($images as $img)
            {
                ?>
                <div>
                    <div>
                        <button class="profile_img" title="cam picture" style="outline: none;">
                            <img alt="temp" name="<?php echo $dates[$i]; ?>" src="<?php echo $img; ?>" onclick="display_img('<?php echo $img; ?>', '<?php echo $dates[$i]; ?>')" />
                        </button>
                    </div>
                </div>
        <?php
                $i++;
            }
        }
        ?>
    </div>
</div>
</body>
