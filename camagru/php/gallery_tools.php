<?php

session_start();

function build_gallery($gallery, $conn)
{
    ?>
    <header class="gala_header">
        <div>
            <div><?php echo $gallery['username']; ?></div>
        </div>
    </header>
    <div class="gala_image">
        <div>
            <div>
                <img alt="temp" class="gala_img" name="<?php echo $gallery['image_id']; ?>" src="<?php echo $gallery['image_base64']; ?>" />
            </div>
        </div>
    </div>
    <div class="gala_comment">
        <section>
            <div class="gala_comment_text">
                <span><span id="<?php echo "like" . $gallery['image_id']; ?>"><?php echo$gallery['image_like_count']; ?> </span> likes</span>
            </div>
        </section>
        <ul id="<?php echo "comment" . $gallery['image_id']; ?>" style="background-color: #ffffff !important;">
            <?php add_image_comment($gallery['image_id'], $conn); ?>
        </ul>
        <hr />
        <div class="gal_comment_box" style="background-color: #ffffff !important;">
            <div style="margin-top: 50px; display: inline;">
                <?php get_like_button($gallery['image_id'], $conn); ?>
            </div>
            <div style="display: inline; margin-left: 10px;">
                <form style="display: inline;" onsubmit="add_comment('<?php echo $gallery['image_id']; ?>'); return false;">
                    <input id="<?php echo "enter". $gallery['image_id']; ?>" class="comment_text" type="text" placeholder="Add comment..." value="" required />
                </form>
            </div>
        </div>
    </div>
    <?php
}

function get_like_button($id, $conn){
    $str = "SELECT * FROM `likes` WHERE (`image_id` = :image_id AND `like_owner` = :user)";
    $stmt = $conn->prepare($str);
    $stmt->bindParam(':image_id', $id);
    $stmt->bindParam(':user', $_SESSION['username']);
    $stmt->execute();
    $is_exist = null;
    foreach ($stmt as $tmp)
    {
        if ($tmp['like_owner'] == $_SESSION['username'])
            $is_exist = $_SESSION['username'];
    }
    if ($is_exist == null)
    {
        ?>
        <img class="like" src="/camagru/images/unlike.jpeg" name="0" onclick="like_button(this, <?php echo $id; ?>);" />
        <?php
    }
    else
    {
        ?>
        <img class="like" src="/camagru/images/like.jpeg" name="1" onclick="like_button(this, <?php echo $id; ?>);" />
        <?php
    }
}

function add_image_comment($id, $conn)
{
    $str = "SELECT * FROM `comments` WHERE `image_id` = :image_id";
    $stmt = $conn->prepare($str);
    $stmt->bindParam(':image_id', $id);
    $stmt->execute();
    foreach ($stmt as $tmp)
    {
        ?>
        <li style="background-color: #ffffff !important;">
            <span class="gala_comment_user"><?php echo $tmp['comment_owner']; ?></span>
            <span style="background-color: #ffffff !important;"> <?php echo $tmp['message']; ?></span>
        </li>
        <?php
    }
}

?>