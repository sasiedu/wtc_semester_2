<?php

?>

<div id="error1" class="hidden">
    <p class="sign_error">The username or password you entered doesn't belong to an account. Please check your username or password and try again.</p>
</div>


<div id="error2" class="hidden">
    <p class="sign_error">The email you entered is invalid. Please change your email and try again</p>
</div>

<div id="error3" class="hidden">
    <p class="sign_error">Your password must contain at least one number [0-9].</p>
</div>

<div id="error4" class="hidden">
    <p class="sign_error">The username you entered belongs to an account. Please change your username and try again.</p>
</div>

<div id="error5" class="hidden">
    <p class="sign_error">The email you entered belongs to an account. Please change your email and try again.</p>
</div>

<div id="error6" class="hidden">
    <p class="sign_error" style="color: #006600">Your account has been successfully created, please verify it by clicking the activation link that has been sent to your email.</p>
</div>

<div id="error7" class="hidden">
    <p class="sign_error" style="color: #006600">Please verify your account to log in.</p>
</div>

<div id="error8" class="hidden">
    <p class="sign_error" style="color: #006600">Your account has been successfully verified, please log in to continue.</p>
</div>

<div id="error9" class="hidden">
    <p class="sign_error">Your account verification was unsuccessfully, please send an email to service@camagru.com for assistance.</p>
</div>

<div id="error10" class="hidden">
    <p class="sign_error">The verification token is invalid, please send an email to service@camagru.com for assistance.</p>
</div>

<div id="error11" class="hidden">
    <p class="sign_error" style="color: #006600">Your password has been successfully changed, please log in to continue.</p>
</div>

<div id="error12" class="hidden">
    <p class="sign_error" style="color: #006600">A link has been sent to your email for reseting your password.</p>
</div>