<?php

session_start();

function imagecopymerge_alpha($dst_im, $src_im, $dst_x, $dst_y, $src_x, $src_y, $src_w, $src_h, $pct)
{

    $cut = imagecreatetruecolor($src_w, $src_h);
    imagecopy($cut, $dst_im, 0, 0, $dst_x, $dst_y, $src_w, $src_h);
    imagecopy($cut, $src_im, 0, 0, $src_x, $src_y, $src_w, $src_h);

    imagecopymerge($dst_im, $cut, $dst_x, $dst_y, 0, 0, $src_w, $src_h, $pct);

}


if (isset($_POST['overlay']) && isset($_POST['img']))
{
    $image = $_POST['img'];
    $image = preg_replace('/data:([a-zA-Z]+\/[a-zA-Z]+);base64,/', '', $image);
    $image = str_replace(' ', '+', $image);
    $image = base64_decode($image);
    $image = imagecreatefromstring($image);
    $lay = imagecreatefrompng($_POST['overlay']);
    $user_h = 0;
    $user_w = 0;
    list($user_w, $user_h) = getimagesize($image);
    $lay = imagescale($lay, 800, 600, IMG_BILINEAR_FIXED);
    $image = imagescale($image, 800, 600, IMG_BILINEAR_FIXED);
    imagecopymerge_alpha($image, $lay, 0, 0, 0, 0, 800, 600, 100);
    imagejpeg($image, "temp.jpeg");
    $image = file_get_contents("temp.jpeg");
    $image = base64_encode($image);
    $image = "data:image/png;base64," . $image;
    echo $image;
    exit(1);
}

if (isset($_POST["frame"]))
{
    $_SESSION['select_frame'] = null;
    $_SESSION['select_frame'] = $_POST["frame"];
    echo "frame set";
    return 1;
}

if (isset($_POST["image"]))
{
    date_default_timezone_set('Africa/Johannesburg');
    $current_date = date('j-n-Y G:i:s');
    $image =  $_POST["image"];
    $image = str_replace(' ', '+', $image);

    if (isset($_SESSION['sidepics']))
    {
        $sidepics = $_SESSION['sidepics'];
        $dates = $_SESSION['dates'];
    }
    else
    {
        $sidepics = array();
        $dates = array();
    }
    $sidepics[] = $image;
    $dates[] = $current_date;
    $_SESSION['sidepics'] = $sidepics;
    $_SESSION['dates'] = $dates;

    include ($_SERVER['DOCUMENT_ROOT'] . "/camagru/config/database.php");

    $username = $_SESSION['username'];
    try {
        $conn = new PDO($DB_DSN, $DB_USER, $DB_PASSWORD);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $str = "INSERT INTO `images`(`image_base64`, `username`, `image_creation_date`) VALUES (:image_base64, :username, :creation_date)";
        $stmt = $conn->prepare($str);
        $stmt->bindParam(':username', $username);
        $stmt->bindParam(':image_base64', $image);
        $stmt->bindParam(':creation_date', $current_date);
        $stmt->execute();
        $str = "UPDATE `users` SET `users`.`user_post_count` = `users`.`user_post_count` + 1 WHERE `users`.`username` = :username";
        $stmt = $conn->prepare($str);
        $stmt->bindParam(':username', $username);
        $stmt->execute();
    }
    catch (PDOException $e)
    {
        echo "conn failed" . $e;
    }
    $conn = null;
    echo $current_date;
    return 1;
}

if (isset($_REQUEST['frame']))
{
    if ($_REQUEST['frame'] == "set") {
        if (isset($_SESSION['select_frame']))
            echo 1;
        else
            echo 2;
        exit (1);
    }
    if ($_REQUEST['frame'] == "get")
    {
        echo $_SESSION['select_frame'];
        exit (1);
    }
}

if (isset($_REQUEST['current']))
{
    $_SESSION['current'] = $_REQUEST['current'];
}

if (isset($_POST['delete']))
{
    $name = $_POST['delete'];
    $dates = $_SESSION['dates'];
    $image = $_SESSION['sidepics'];
    $i = 0;

    foreach ($dates as $tmp)
    {
        if ($tmp == $name)
        {
            array_splice($dates, $i, 1);
            array_splice($image, $i, 1);
            echo "found";
        }
        $i++;
    }
    $_SESSION['dates'] = $dates;
    $_SESSION['sidepics'] = $image;

    include ($_SERVER['DOCUMENT_ROOT'] . "/camagru/config/database.php");

    $username = $_SESSION['username'];
    try {

        $conn = new PDO($DB_DSN, $DB_USER, $DB_PASSWORD);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $str = "DELETE FROM `images` WHERE (`images`.`username` = :username AND `images`.`image_creation_date` = :creation_date)";
        $stmt = $conn->prepare($str);
        echo $username . "\n";
        echo $name . "\n";
        $stmt->bindParam(':username', $username);
        $stmt->bindParam(':creation_date', $name);
        $stmt->execute();
        $str = "UPDATE `users` SET `users`.`user_post_count` = `users`.`user_post_count` - 1 WHERE `users`.`username` = :username";
        $stmt = $conn->prepare($str);
        $stmt->bindParam(':username', $username);
        $stmt->execute();
    }
    catch (PDOException $e)
    {
        echo "conn failed" . $e;
    }
    $conn = null;
}

if (isset($_REQUEST['userdelete']))
{
    $dates = $_SESSION['dates'];
    $image = $_SESSION['sidepics'];
    $username = $_SESSION['username'];
    $id = $_REQUEST['userdelete'];
    $remove = null;
    include ($_SERVER['DOCUMENT_ROOT'] . "/camagru/config/database.php");

    $username = $_SESSION['username'];
    try {

        $conn = new PDO($DB_DSN, $DB_USER, $DB_PASSWORD);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $str = "SELECT * FROM `images` WHERE (`images`.`username` = :username AND `images`.`image_id` = :id)";
        $stmt = $conn->prepare($str);
        $stmt->bindParam(':username', $username);
        $stmt->bindParam(':id', $id);
        $stmt->execute();

        foreach ($stmt as $tmp)
        {
            if ($tmp['image_id'] == $id)
                $remove = $tmp['image_creation_date'];
        }
        $i = 0;

        foreach ($dates as $tmp)
        {
            if ($tmp == $remove)
            {
                array_splice($dates, $i, 1);
                array_splice($image, $i, 1);
                echo "found";
            }
            $i++;
        }
        $_SESSION['dates'] = $dates;
        $_SESSION['sidepics'] = $image;


        $str = "DELETE FROM `images` WHERE (`images`.`username` = :username AND `images`.`image_id` = :id)";
        $stmt = $conn->prepare($str);
        $stmt->bindParam(':username', $username);
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        $str = "UPDATE `users` SET `users`.`user_post_count` = `users`.`user_post_count` - 1 WHERE `users`.`username` = :username";
        $stmt = $conn->prepare($str);
        $stmt->bindParam(':username', $username);
        $stmt->execute();
    }
    catch (PDOException $e)
    {
        echo "conn failed" . $e;
    }
    $conn = null;
}

?>
