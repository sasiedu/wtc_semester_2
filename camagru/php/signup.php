<?php

include ("user_tools.php");

$user = $_REQUEST["user"];
$pass = $_REQUEST["pass"];
$name = $_REQUEST["name"];
$email = $_REQUEST["email"];


if ($user !== ""){
    include ($_SERVER['DOCUMENT_ROOT'] . "/camagru/config/database.php");

    if (preg_match('/[0-9]+/', $pass) != 1){ echo 3; exit(1); }

    if (filter_var($email, FILTER_VALIDATE_EMAIL) == FALSE){ echo 2; exit(1); }

    try {
        $conn = new PDO($DB_DSN, $DB_USER, $DB_PASSWORD);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $str = "SELECT * FROM `users`";
        $stmt = $conn->prepare($str);
        $stmt->execute();

        foreach ($stmt as $data)
        {
            if ($data['username'] == $user){ echo 4; exit(1); }

            if ($data['email'] == $email){ echo 5; exit(1); }
        }
        $pass = secure_pass($pass);
        $token = token_generate(45);
        $stmt = $conn->prepare("INSERT INTO users (username, fullname, email, pass_key, pass_hash, verification_token)
        VALUES (:username, :fullname, :email, :pass_key, :pass_hash, :verification_token)");
        $stmt->bindParam(':username', $user);
        $stmt->bindParam(':fullname', $name);
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':pass_key', $pass['salt']);
        $stmt->bindParam(':pass_hash', $pass['pass']);
        $stmt->bindParam(':verification_token', $token);
        $stmt->execute();
        verification_mail($user, $name, $email, $token);
        echo 6;
    }
    catch (PDOException $e)
    {
        echo "conn failed";
    }
    $conn = null;
}

?>