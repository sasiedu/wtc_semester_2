<?php

session_start();

include ($_SERVER['DOCUMENT_ROOT'] . "/camagru/config/database.php");

function get_post_numbers($DB_DSN, $DB_USER, $DB_PASSWORD)
{


    try {
        $conn = new PDO($DB_DSN, $DB_USER, $DB_PASSWORD);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $str = "SELECT `user_post_count` FROM `users` WHERE `username` = :user";
        $stmt = $conn->prepare($str);
        $stmt->bindParam(':user', $_SESSION['username']);
        $stmt->execute();
        foreach ($stmt as $tmp)
        {
            if (isset($tmp['user_post_count']))
                return $tmp['user_post_count'];
        }

    }
    catch (PDOException $e)
    {
        echo "conn failed" . $e;
    }
    $conn = null;
}

?>


<article id="profile1" class="user_profile clearfix">
    <header class="user_profile_header clearfix">
        <div class="profile_img_box1 clearfix">
            <div class="profile_img_box2">
                <button class="profile_img" title="Change profile picture">
                    <img alt="Change profile picture" class="profile_img1" src="/camagru/images/pp.jpg" />
                </button>
            </div>
        </div>
        <div class="profile_side clearfix" >
            <div class="profile_side_top clearfix">
                <h1> <?php echo $_SESSION['username']; ?>  </h1>
                <a href="#"><span><button>Edit Profile</button></span></a>
            </div>
            <ul>
                <li>
                    <span><span> <?php echo get_post_numbers($DB_DSN, $DB_USER, $DB_PASSWORD); ?> </span> post </span>
                </li>
            </ul>
            <div class="profile_side_down"><h2> <?php echo $_SESSION['name']; ?> </h2></div>
        </div>
    </header>
    <div>
        <div>
            <div class="dash_img">
                <?php

                include ($_SERVER['DOCUMENT_ROOT'] . "/camagru/config/database.php");

                try {
                    $conn = new PDO($DB_DSN, $DB_USER, $DB_PASSWORD);
                    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                    $str = "SELECT * FROM `images` WHERE `username` = :user";
                    $stmt = $conn->prepare($str);
                    $stmt->bindParam(':user', $_SESSION['username']);
                    $stmt->execute();
                    foreach ($stmt as $img) {
                        ?>
                        <div class="dash_img1">
                            <div class="dash_img2">
                                <button class="user_imgs">
                                    <img alt="<?php echo $img['image_id']; ?>" class="dash_img3" src="<?php echo $img['image_base64']; ?>" onclick="dash_images_setup(this);" />
                                </button>
                            </div>
                        </div>
                        <?php
                    }
                }
                catch (PDOException $e)
                {
                    echo "conn failed";
                }
                $conn = null;
                ?>
            </div>
        </div>
    </div>
</article>
<div id="dash_display" style="width: 100%; height: 100%; position: fixed; background-color: transparent; top: 0; left: 0; display: none;">
    <table style="position: fixed; width: 80%; height: 600px; margin: auto; background-color: #efefef; top: 20%; left: 10%; padding: 20px">
        <tr>
            <th style="width: 65%; height: 100%; background-color: #efefef; margin-bottom: 30px; padding: 10px;">
                <img id="dash_images" alt="temp" src="/camagru/images/demo1.jpeg" style="width: 90%; height: 80%;"/>
                <div style="background-color: #efefef;">
                    <button class="user_button" style="margin: 10px;" onclick="user_delete();">Delete</button><span><button class="user_button" style="margin-top: 20px; margin-left: 30%;">Share</button></span>
                </div>
            </th>
            <th style="width: 30%; height: 100%; background-color: #efefef; margin-bottom: 30px;">
                <div style="height: 6%; width: 100%; background-color: #efefef; margin: 10px; padding: 5px; text-align: left;">
                    <span id="dash_likes" style="background-color: #efefef !important;">0 </span><span style="background-color: #efefef !important; font-weight: 300;"> likes</span>
                </div>
                <div style="max-height: 90%; width: 100%; background-color: #efefef; margin: 10px; padding: 10px; text-align: left;">
                    <ul id="dash_cc" style="background-color: #ffffff !important;">
                        dgjsidjglsijglsjgldsjglisdjg
                    </ul>
                </div>
            </th>
        </tr>
    </table>
</div>