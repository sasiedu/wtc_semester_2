<?php

if (isset($_GET['email']) && !empty($_GET['email']) AND isset($_GET['token']) && !empty($_GET['token']))
{
    include($_SERVER['DOCUMENT_ROOT'] . "/camagru/config/database.php");

    echo '<script>all_user_errors_off();
    document.getElementById("signup").style.display = "none";
    document.getElementById("login").style.display = "block";
    document.getElementById("forgot").style.display = "none";
    document.getElementById("down1").style.display = "none";
    document.getElementById("down2").style.display = "block";
    document.getElementById("down3").style.display = "none";
       </script>';
    $email = $_GET['email'];
    $token = $_GET['token'];
    try {
        $conn = new PDO($DB_DSN, $DB_USER, $DB_PASSWORD);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $str = "SELECT `verification_token` FROM `users` WHERE `email` = :email";
        $stmt = $conn->prepare($str);
        $stmt->bindParam(':email', $email);
        $stmt->execute();

        foreach ($stmt as $row)
        {
            if ($row['verification_token'] == $token)
            {
                $str = "UPDATE `users` SET `user_verified`='1' WHERE `email` = :email";
                $stmt = $conn->prepare($str);
                $stmt->bindParam(':email', $email);
                $stmt->execute();
                echo '<script>document.getElementById("error8").className = "input-block";</script>';
                $str = "UPDATE `users` SET `verification_token`= NULL WHERE `email` LIKE :email";
                $stmt = $conn->prepare($str);
                $stmt->bindParam(':email', $email);
                $stmt->execute();
                exit(1);
            }
        }
        echo '<script>document.getElementById("error9").className = "input-block";</script>';
    }
    catch (PDOException $e)
    {
        echo "conn failed";
    }
    $conn = null;
    return 1;
}

if (isset($_GET['user']) && !empty($_GET['user']) AND isset($_GET['token']) && !empty($_GET['token']))
{
    include($_SERVER['DOCUMENT_ROOT'] . "/camagru/config/database.php");

    $user = $_GET['user'];
    $token = $_GET['token'];
    try {
        $conn = new PDO($DB_DSN, $DB_USER, $DB_PASSWORD);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $str = "SELECT `verification_token` FROM `users` WHERE `username` = :username";
        $stmt = $conn->prepare($str);
        $stmt->bindParam(':username', $user);
        $stmt->execute();

        foreach ($stmt as $row)
        {
            if ($row['verification_token'] == $token)
            {
                echo '<script>all_user_errors_off();
    document.getElementById("signup").style.display = "none";
    document.getElementById("login").style.display = "none";
    document.getElementById("reset").style.display = "block";
    document.getElementById("reset").target = "' . $user . '";
    document.getElementById("forgot").style.display = "none";
    document.getElementById("down1").style.display = "block";
    document.getElementById("down2").style.display = "none";
    document.getElementById("down3").style.display = "none";
       </script>';
                $token = null;
                $str = "UPDATE `users` SET `verification_token` = :token WHERE `username` = :username";
                $stmt = $conn->prepare($str);
                $stmt->bindParam(':token', $token);
                $stmt->bindParam(':username', $user);
                $stmt->execute();
                exit(1);
            }
        }
        echo '<script>document.getElementById("error10").className = "input-block";</script>';
    }
    catch (PDOException $e)
    {
        echo "conn failed";
    }
    $conn = null;
}

?>