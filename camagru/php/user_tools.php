<?php

function secure_pass($pass){
    $options = ['cost' => 10];

    $salt = token_generate(15);
    $hash = password_hash($pass, PASSWORD_BCRYPT, $options);
    $pass = $salt . $hash;

    return array("salt" => $salt, "pass" => $pass);
}

function token_generate($length){
    $str = "";
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    $size = strlen($chars);
    for( $i = 0; $i < $length; $i++) {
        $str .= $chars[rand( 0, $size - 1 )];
    }
    return $str;
}

function verification_mail($user, $name, $email, $verification_token)
{
    $to      = $email; // Send email to our user
    $subject = 'Camagru Sign up verification'; // Give the email a subject
    $message = '
 
Thanks '.$name.'  for signing up to Camagru!
Your account has been created, you can login with the following credentials after you have successfully activated your account.
 
------------------------
Username: '.$user.'
------------------------
 
Please click this link to activate your account:
http://localhost:8080/camagru/index.php?email='.$email.'&token='.$verification_token.'
 
'; // Our message above including the link

    $headers = 'From:noreply@camagru.com' . "\r\n"; // Set from headers
    mail($to, $subject, $message, $headers); // Send our email
}

function reset_pass_mail($user, $name, $email, $verification_token)
{
    $to      = $email; // Send email to our user
    $subject = 'Camagru Password reset'; // Give the email a subject
    $message = '
 
Hi '.$name.' , 

Below is a link to reset your password. Ignore this mail if your did not request
a change of your Camagru account password.

Please click this link to reset your Camagru account password:
http://localhost:8080/camagru/index.php?user='.$user.'&token='.$verification_token.'
 
'; // Our message above including the link

    $headers = 'From:noreply@camagru.com' . "\r\n"; // Set from headers
    mail($to, $subject, $message, $headers); // Send our email
}

if (isset($_REQUEST['user']) AND isset($_REQUEST['new_pass']))
{
    $user = $_REQUEST['user'];
    $pass = $_REQUEST['new_pass'];
    $pass = secure_pass($pass);

    include ($_SERVER['DOCUMENT_ROOT'] . "/camagru/config/database.php");

    try {
        $conn = new PDO($DB_DSN, $DB_USER, $DB_PASSWORD);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $str = "UPDATE `users` SET  `pass_key` = :pass_key, `pass_hash` = :pass_hash WHERE `username` = :username";
        $stmt = $conn->prepare($str);
        $stmt->bindParam(':pass_key', $pass['salt']);
        $stmt->bindParam(':pass_hash', $pass['pass']);
        $stmt->bindParam(':username', $user);
        $stmt->execute();
    }
    catch (PDOException $e)
    {
        echo "conn failed" . $e;
    }
    $conn = null;
}

?>