<?PHP

?>

<html>
<head>
    <title>Camagru</title>
    <link rel="stylesheet" type="text/css" href="/camagru/css/glob.css" />
    <link rel="stylesheet" type="text/css" href="/camagru/css/front.css" />
    <link rel="stylesheet" type="text/css" href="/camagru/css/dash.css" />
    <link rel="stylesheet" type="text/css" href="/camagru/css/dash_page.css" />
    <link rel="stylesheet" type="text/css" href="/camagru/css/camera.css" />
    <link rel="stylesheet" type="text/css" href="/camagru/css/gallery.css" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/camagru/css/phone.css" />
    <link rel="stylesheet" type="text/css" href="/camagru/css/tab1.css" />
    <link rel="stylesheet" type="text/css" href="/camagru/css/tab2.css" />
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <script src="/camagru/script/java.js"></script>
    <script src="/camagru/script/main.js"></script>
    <script src="/camagru/script/camera.js"></script>
    <script src="/camagru/script/cam.js"></script>
    <script src="/camagru/script/gallery.js"></script>
</head>
<body>
