<?php

session_start();

$user = $_REQUEST["user"];
$pass = $_REQUEST["pass"];

if ($user !== ""){
    include ($_SERVER['DOCUMENT_ROOT'] . "/camagru/config/database.php");

    try {
        $conn = new PDO($DB_DSN, $DB_USER, $DB_PASSWORD);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $str = "SELECT * FROM `users` WHERE `username` = :user";
        $stmt = $conn->prepare($str);
        $stmt->bindParam(':user', $user);
        $stmt->execute();
        foreach ($stmt as $row)
        {
            if ($row['username'] == $user)
            {
                $true_pass = substr($row['pass_hash'], 15);
                if (password_verify($pass, $true_pass) == TRUE)
                {
                    if ($row['user_verified'] == '0'){ echo 7; exit(1); }

                    echo 10;
                    $_SESSION['username'] = $user;
                    $_SESSION['name'] = $row['fullname'];
                    $str = "SELECT * FROM `images` WHERE `username` = :user";
                    $stmt = $conn->prepare($str);
                    $stmt->bindParam(':user', $user);
                    $stmt->execute();
                    $_SESSION['current'] = "gallery1";
                    $_SESSION['frames'] = array("/camagru/images/lay1.png", "/camagru/images/lay2.png", "/camagru/images/lay3.png",
                        "/camagru/images/lay4.png", "/camagru/images/lay5.png", "/camagru/images/lay6.png", "/camagru/images/lay7.png");
                    return 10;
                }
            }
        }
        echo 1;
    }
    catch (PDOException $e)
    {
        echo "conn failed";
    }
    $conn = null;
}

?>