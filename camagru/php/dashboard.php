<?php

?>

<div id="main1">
    <div id="header">
        <div class="logo"><a href="/camagru/"><span class="camag1">Camagru</span></a><div class="sign-out" onclick="sign_out()">sign out</div></div>
    </div>
    <div id="header2">
        <div class="logo"><a href="/camagru/"><span class="camag1">Camagru</span></a></div>
        <div style="text-align: center; justify-content: center; width: 100%; background-color: #efefef">
            <button class="dash-button1" style="margin-right: 10%;" onclick="gallery_main('gallery1', 'gallery')"><i class="fa fa-home fa-3x" aria-hidden="true"></i></button>
            <button class="dash-button1" style="margin-right: 10%;" onclick="camera_main('camera1', 'camera')"><i class="fa fa-camera fa-3x" aria-hidden="true"></i></button>
            <button class="dash-button1" style="margin-right: 10%;" onclick="profile_main('profile1', 'profile')"><i class="fa fa-user fa-3x" aria-hidden="true"></i></button>
            <button class="dash-button1" style="margin-right: 10%;" onclick="sign_out()"><i class="fa fa-sign-out fa-3x" aria-hidden="true"></i></button>
        </div>
    </div>
    <section id="active_page" class="clearfix">
        <div class="dash-side">
            <div id="gallery" class="dash-button" onclick="gallery_main('gallery1', 'gallery')">Gallery</div>
            <div id="camera" class="dash-button" onclick="camera_main('camera1', 'camera')">Camera</div>
            <div id="profile" class="dash-button dash-button_selected" onclick="profile_main('profile1', 'profile')">Profile</div>
        </div>
        <div class="volume">
            <?php

                session_start();
                if ($_SESSION['current'] == "profile1")
                    include ("dash.php");
                else if ($_SESSION['current'] == "gallery1")
                    include ("gallery.php");
                else if ($_SESSION['current'] == "camera1")
                {
                    include("camera.php");
                }
            ?>
        </div>
    </section>
    <?php include ("foot.php"); ?>
</div>
