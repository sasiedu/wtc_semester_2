<?php

include ("gallery_tools.php");

session_start();

if (isset($_REQUEST['like']) && isset($_REQUEST['id']))
{
    $like = $_REQUEST['like'];
    $id = $_REQUEST['id'];
    include ($_SERVER['DOCUMENT_ROOT'] . "/camagru/config/database.php");

    try {
        $conn = new PDO($DB_DSN, $DB_USER, $DB_PASSWORD);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        if ($like == '0')
            $str = "INSERT INTO `likes` (`image_id`, `like_owner`) VALUES (:image_id, :user)";
        else
            $str = "DELETE FROM `likes` WHERE (`likes`.`image_id` = :image_id AND `likes`.`like_owner` = :user)";
        $stmt = $conn->prepare($str);
        $stmt->bindParam(':image_id', $id);
        $stmt->bindParam(':user', $_SESSION['username']);
        $stmt->execute();
        if ($like == '0')
            $str = "UPDATE `images` SET `images`.`image_like_count` = `images`.`image_like_count` + 1 WHERE `images`.`image_id` = :image_id";
        else
            $str = "UPDATE `images` SET `images`.`image_like_count` = `images`.`image_like_count` - 1 WHERE `images`.`image_id` = :image_id";
        $stmt = $conn->prepare($str);
        $stmt->bindParam(':image_id', $id);
        $stmt->execute();
    }
    catch (PDOException $e)
    {
        echo "conn failed" . $e;
    }
    $conn = null;
    return ;
}

if (isset($_REQUEST['comment']) && isset($_REQUEST['msg']))
{
    $id = $_REQUEST['comment'];
    $msg = $_REQUEST['msg'];
    include ($_SERVER['DOCUMENT_ROOT'] . "/camagru/config/database.php");

    try {
        $conn = new PDO($DB_DSN, $DB_USER, $DB_PASSWORD);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $str = "INSERT INTO `comments` (`image_id`, `message`, `comment_owner`) VALUES (:image_id, :message, :username)";
        $stmt = $conn->prepare($str);
        $stmt->bindParam(':image_id', $id);
        $stmt->bindParam(':username', $_SESSION['username']);
        $stmt->bindParam(':message', $msg);
        $stmt->execute();
        echo $_SESSION['username'];
    }
    catch (PDOException $e)
    {
        echo "conn failed" . $e;
    }
    $conn = null;
    return 1;
}

if (isset($_REQUEST['dash_comment']))
{
    $id = $_REQUEST['dash_comment'];
    $result = array();

    include ($_SERVER['DOCUMENT_ROOT'] . "/camagru/config/database.php");

    try {
        $conn = new PDO($DB_DSN, $DB_USER, $DB_PASSWORD);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $str = "SELECT * FROM `comments` INNER JOIN `images` ON `comments`.`image_id` = `images`.`image_id` WHERE `comments`.`image_id` = :image_id";
        $stmt = $conn->prepare($str);
        $stmt->bindParam(':image_id', $id);
        $stmt->execute();
        foreach ($stmt as $tmp)
        {
            $result[] = $tmp;
        }
        echo json_encode($result);
    }
    catch (PDOException $e)
    {
        echo "conn failed" . $e;
    }
    $conn = null;
    return 1;
}

if (isset($_REQUEST['scroll']))
{
    if ($_SESSION['current'] != "gallery1")
        return 1;
    $diff = intval($_SESSION['image_total']) - intval($_SESSION['image_index']);
    if (intval($diff) < 1)
        return 1;
    if (intval($diff) > 5)
        $diff = 5;

    include ($_SERVER['DOCUMENT_ROOT'] . "/camagru/config/database.php");

    try {
        $conn = new PDO($DB_DSN, $DB_USER, $DB_PASSWORD);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $str = "SELECT * FROM `images` LIMIT :off, :lim";
        $stmt = $conn->prepare($str);
        $stmt->bindParam(':lim', $diff, PDO::PARAM_INT );
        $stmt->bindParam(':off', $_SESSION['image_index'], PDO::PARAM_INT );
        $stmt->execute();

        foreach ($stmt as $tmp){
            ?>
            <article>
                <?php build_gallery($tmp, $conn); ?>
            </article>
            <?php
        }
        $_SESSION['image_index'] = $_SESSION['image_index'] + $diff;
    }
    catch (PDOException $e)
    {
        echo "conn failed" . $e;
    }
    $conn = null;
    return 1;
}

?>