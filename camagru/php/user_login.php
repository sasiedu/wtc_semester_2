<?php
?>

<div class="side2">
    <div class="subform">
        <h1 class="camag">Camagru</h1>
        <form id="signup" name="signup" action="signup.php" method="post" class="signup-form" onsubmit="my_signup(); return false;">
            <h2 class="sign_text">Sign up to see, capture, edit and upload photos and moments</h2>
            <br />
            <div class="input-block">
                <input type="text" class="input-box" name="email" placeholder="Email" value="" required>
            </div>
            <div class="input-block">
                <input type="text" class="input-box" name="fullname" placeholder="Full Name" value="" required>
            </div>
            <div class="input-block">
                <input type="text" class="input-box" name="username" placeholder="Username" value="" required>
            </div>
            <div class="input-block">
                <input type="password" class="input-box" name="password" placeholder="Password" value="" required>
            </div>
            <div class="input-block">
                <span class="sign-button">
                    <button type="submit" form="signup" class="sign-butt">Sign up</button>
                </span>
            </div>
            <div style="text-align: center; margin: 10px 60px; font-size: 14px; line-height: 18px; background-color: #fff;">
                <span style="color: #999; background-color: #fff;">By signing up, you agree to our <b style="background-color: #fff;">Terms & Privacy Policy</b>.</span>
            </div>
        </form>
        <form id="login" name="login" action="login.php" method="post" class="login-form" onsubmit="my_login(); return false;">
            <h2 class="sign_text">Sign in to see, capture, edit and upload photos and moments</h2>
            <br />
            <div class="input-block">
                <input type="text" class="input-box" name="username" placeholder="Username" value="" required>
            </div>
            <div class="input-block">
                <input type="password" class="input-box" name="password" placeholder="Password" value="" required>
            </div>
            <div class="input-block">
                <span class="sign-button">
                   <button type="submit" form="login" class="sign-butt">Log in</button>
                </span>
            </div>
        </form>
        <form id="forgot" name="forgot" action="login.php" method="post" class="login-form" onsubmit="my_reset(); return false;">
            <h2 class="sign_text">Sign in to see, capture, edit and upload photos and moments</h2>
            <br />
            <div class="input-block">
                <input type="text" class="input-box" name="pass_user" placeholder="username" value="" required>
            </div>
            <div class="input-block">
                <span class="sign-button">
                   <button type="submit" form="forgot" class="sign-butt">Reset password</button>
                </span>
            </div>
        </form>
        <form id="reset" name="reset"  action="login.php" target="tmp" method="post" class="login-form" onsubmit="new_password(); return false;">
            <h2 class="sign_text">Sign in to see, capture, edit and upload photos and moments</h2>
            <br />
            <div class="input-block">
                <input type="password" class="input-box" name="new_pass" placeholder="new password" value="" required>
            </div>
            <div class="input-block">
                <span class="sign-button">
                   <button type="submit" form="reset" class="sign-butt">Reset</button>
                </span>
            </div>
        </form>
        <?php include ("error.php"); ?>
    </div>
    <div id="down1" class="sign-next">
        <p style="background-color: #fff; font-family: sans-serif;">
            Have an account? <a style="color: #3897f0; text-decoration: none;" href="#" onclick="user_login();" >Log in</a>
        </p>
    </div>
    <div id="down2" class="sign-next" style="display: none">
        <p style="background-color: #fff; font-family: sans-serif;">
            Don't have an account? <a style="color: #3897f0; text-decoration: none;" href="#" onclick="user_signup();" >Sign up</a>
        </p>
    </div>
    <div id="down3" class="sign-next" style="display: none">
        <p style="background-color: #fff; font-family: sans-serif;">
            Forgot your password? <a style="color: #3897f0; text-decoration: none;" href="#" onclick="user_forgot();" >Reset Passowrd</a>
        </p>
    </div>
</div>
