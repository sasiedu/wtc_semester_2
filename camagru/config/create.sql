IF EXISTS `camagru` DROP;
USE `camagru`;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";
CREATE TABLE `comments` (
  `comment_id` bigint(20) NOT NULL,
  `image_id` bigint(20) NOT NULL,
  `message` longtext NOT NULL,
  `comment_owner` varchar(255) NOT NULL,
  `comment_creation_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `images` (
  `image_id` bigint(20) NOT NULL,
  `image_base64` longblob NOT NULL,
  `username` varchar(255) NOT NULL,
  `image_creation_date` varchar(255) NOT NULL,
  `image_like_count` bigint(20) NOT NULL DEFAULT '0',
  `image_comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `likes` (
  `like_id` bigint(20) NOT NULL,
  `image_id` bigint(20) NOT NULL,
  `like_owner` varchar(255) NOT NULL,
  `like_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `users` (
  `username` varchar(255) NOT NULL,
  `fullname` text NOT NULL,
  `email` text NOT NULL,
  `pass_key` text NOT NULL,
  `pass_hash` text NOT NULL,
  `verification_token` text NOT NULL,
  `user_verified` int(11) NOT NULL DEFAULT '0',
  `user_creation_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_post_count` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
ALTER TABLE `comments`
  ADD PRIMARY KEY (`comment_id`),
  ADD KEY `image_id` (`image_id`);
ALTER TABLE `images`
  ADD PRIMARY KEY (`image_id`),
  ADD KEY `username` (`username`);
ALTER TABLE `likes`
  ADD PRIMARY KEY (`like_id`),
  ADD KEY `image_id` (`image_id`);
ALTER TABLE `users`
  ADD PRIMARY KEY (`username`);
ALTER TABLE `comments`
  MODIFY `comment_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
ALTER TABLE `images`
  MODIFY `image_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
ALTER TABLE `likes`
  MODIFY `like_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`image_id`) REFERENCES `images` (`image_id`) ON DELETE CASCADE;
ALTER TABLE `images`
  ADD CONSTRAINT `images_ibfk_1` FOREIGN KEY (`username`) REFERENCES `users` (`username`) ON DELETE CASCADE;
ALTER TABLE `likes`
  ADD CONSTRAINT `likes_ibfk_1` FOREIGN KEY (`image_id`) REFERENCES `images` (`image_id`) ON DELETE CASCADE;