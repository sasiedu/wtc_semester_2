<?php

session_start();

include ("php/header.php");

if (!isset($_SESSION['username']) || empty($_SESSION['username']))
    include ("php/front_page.php");
else
    include ("php/dashboard.php");
include ("php/footer.php");
include ("php/verify.php");

?>