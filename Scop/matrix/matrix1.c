/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   matrix1.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/26 23:23:24 by sasiedu           #+#    #+#             */
/*   Updated: 2016/11/26 23:23:26 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/scop.h"

void	mat4init(t_mat4 *mat)
{
	int		i;
	int		j;

	i = -1;
	while (++i < 4)
	{
		j = -1;
		while (++j < 4)
		{
			mat->mat[i][j] = 0.0f;
		}
	}
}

void	mat4identity(t_mat4 *mat)
{
	mat4init(mat);
	mat->mat[0][0] = 1.0f;
	mat->mat[1][1] = 1.0f;
	mat->mat[2][2] = 1.0f;
	mat->mat[3][3] = 1.0f;
}

void	mat4print(t_mat4 mat)
{
	for (int i = 0; i < 4; i++){
		for (int j = 0; j < 4; j++){
			printf("%f ", mat.mat[i][j]);
		}
		printf("\n");
	}
	printf("\n");
}

t_mat4	mat4dot(t_mat4 a, t_mat4 b)
{
	t_mat4	mat;
	int 	i;
	int 	j;
	int 	k;
	float	sum;

	mat4init(&mat);
	i = -1;
	sum = 0.0f;
	while (++i < 4)
	{
		j = -1;
		while (++j < 4)
		{
			k = -1;
			while (++k < 4)
				sum = sum + a.mat[i][k] * b.mat[k][j];
			mat.mat[i][j] = sum;
			sum = 0.0f;
		}
	}

	return (mat);
}

void	mat4copy(t_mat4 *dst, t_mat4 *old)
{
	int 	i;
	int 	j;

	i = -1;
	while (++i < 4)
	{
		j = -1;
		while (++j < 4)
		{
			dst->mat[i][j] = old->mat[i][j];
		}
	}
}