# Scop
This mini project is a first step towards the use of OpenGL. 

Your goal is to create a small program that will show a 3D object conceived with a mod- elisation program like Blender. 
The 3D object is stored in a .obj file. You will be at least in charge of parsing to obtain the requested rendering. 
In a window, your 3D object will be displayed in perspective (which means that what is far must be smaller), rotate on itself around its main symmetrical axis (middle of the object basically...). 

By using various colors, it must be possible to distinguish the various sides. The object can be moved on three axis, in both directions. 
Finally a texture must be applicable simply on the object when we press a dedicated key, and the same key allows to go back to the different colors. 
A soft transition between the two is requested. 

You can use external libraries (other than OpenGL, libm and lib C) ONLY to manage the windows and the events. 
You are allowed to use your libft. 
If unsure, use the MinilibX with its extension OpenGL. 
No libraries allowed to load the 3D object, nor to make your matrixes or to load the shaders.
