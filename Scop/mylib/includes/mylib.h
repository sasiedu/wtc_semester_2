/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mylib.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/21 09:58:07 by sasiedu           #+#    #+#             */
/*   Updated: 2016/11/21 10:32:16 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MYLIB_H
# define MYLIB_H

# include <stdio.h>
# include <unistd.h>
# include <stdlib.h>
# include <fcntl.h>
# include <string.h>

# define BUFF_SIZE	1
# define ABS(x) 	(x < 0) ? x * -1 : x

typedef	struct	s_split_string
{
	size_t		words;
	char		**strings;
}				t_split_string;

int				ft_strcmp(const char *s1, const char *s2);
int				ft_strncmp(const char *s1, const char *s2, size_t n);
char			*ft_strsub(char const *s, unsigned int start, size_t len);
int				ft_atoi(const char *nptr);
void			ft_putchar(char c);
void			ft_putendl_fd(char const *s, int fd);
void			ft_putnbr_fd(int n, int fd);
char			*ft_strrchr(const char *s, int c);
t_split_string	ft_nstrsplit(char const *s, char c);
void			ft_free_split(t_split_string *split);
int				ft_gnl(const int fd, char **line);
size_t			ft_strlen(const char *s);
char			*ft_strnew(size_t size);
char			*ft_strjoin(char const *s1, char const *s2);
void			ft_strdel(char **as);
char            *ft_itoa(int n);
void            ft_putnbr(int n);

#endif
