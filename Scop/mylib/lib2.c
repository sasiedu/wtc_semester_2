/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib2.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/21 10:16:20 by sasiedu           #+#    #+#             */
/*   Updated: 2016/11/21 10:16:22 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/mylib.h"

void    ft_putchar(char c)
{
	write(1, &c, 1);
}

void    ft_putendl_fd(char const *s, int fd)
{
	write(fd, s, ft_strlen(s));
	write(fd, "\n", 1);
}

void	ft_putnbr_fd(int n, int fd)
{
	char	*str;

	str = ft_itoa(n);
	write(fd, str, ft_strlen(str));
	ft_strdel(&str);
}

char	*ft_strnew(size_t size)
{
	char	*str;
	char	*str2;

	str = malloc(size);
	if (str == NULL)
		return (NULL);
	str2 = str;
	while (size--)
		*str2++ = '\0';
	return (str);
}

void	ft_strdel(char **as)
{
	if (*as)
		free(*as);
	*as = NULL;
}