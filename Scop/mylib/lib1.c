/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib1.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/21 10:00:58 by sasiedu           #+#    #+#             */
/*   Updated: 2016/11/21 10:01:02 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/mylib.h"

int     ft_strcmp(const char *s1, const char *s2)
{
	while ((*s1 == *s2) && (*s1 != '\0') && (*s2 != '\0'))
	{
		s1++;
		s2++;
	}
	return (*(char *)s1 - *(char *)s2);
}

int     ft_strncmp(const char *s1, const char *s2, size_t n)
{
	while ((*s1 == *s2) && (*s1 != '\0') && (*s2 != '\0') && --n)
	{
		s1++;
		s2++;
	}
	return (*(char *)s1 - *(char *)s2);
}

char	*ft_strsub(char const *s, unsigned int start, size_t len)
{
	char	*str;
	size_t	i;

	i = 0;
	if (s == NULL)
		return (NULL);
	str = (char *)malloc(len + 1);
	if (str == NULL)
		return (NULL);
	while (i < len && s[i])
	{
		str[i] = s[start + i];
		i++;
	}
	while (i < len)
	{
		str[i] = '\0';
		i++;
	}
	str[i] = '\0';
	return (str);
}

int     ft_atoi(const char *nptr)
{
	long int	res;
	size_t		i;
	int			neg;

	res = 0;
	i = 0;
	neg = 0;
	if (nptr[0] == '-')
	{
		neg = 1;
		++i;
	}
	if (nptr[0] == '+')
		++i;
	while ((nptr[i] >= '0') && (nptr[i] <= '9'))
	{
		res *= 10;
		res += (nptr[i++] - '0');
	}
	if (neg)
		res = -res;
	return ((int)res);
}

char	*ft_strrchr(const char *s, int c)
{
	unsigned int i;

	i = ft_strlen(s);
	while (s[i] != c)
		i--;
	return ((char *)s + i);
}