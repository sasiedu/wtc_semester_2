#ifndef STRUCT2_H
# define STRUCT2_H

# include "structs.h"

typedef	struct 			s_mesh
{
	GLfloat 			vs[20000000];
	GLfloat 			uvs[20000000];
	GLfloat 			vns[20000000];
	GLfloat 			v_color[20000000];
	GLfloat 			v_gray[20000000];
	size_t				size;
	size_t				n_size;
	size_t				t_size;
}						t_mesh;

typedef struct 			s_mvp
{
	t_mat4				v;
	t_mat4				m;
	t_mat4				p;
	t_mat4				mvp;
	t_mat4				rot;
	t_vec3f				trans;
}						t_mvp;

typedef struct 			s_ids
{
	GLuint				program;
	GLuint 				mvp;
	GLuint 				view;
	GLuint 				model;
	GLuint 				texture;
	GLuint 				v_array;
}						t_ids;

typedef struct 			s_loc
{
	GLuint				v;
	GLuint				uv;
	GLuint				vn;
	GLuint				vc;
}						t_loc;

typedef struct 			s_buffers
{
	GLuint				v;
	GLuint				uv;
	GLuint				vc;
	GLuint				vn;
	GLuint				t_tga;
	GLuint				t_bmp;
	GLuint				t_dds;
}						t_buffers;

typedef struct 			s_obj
{
	t_ids				ids;
	t_ids				tids;
	t_loc				locs;
	t_mesh				meshs;
	t_mvp				mvps;
	t_buffers			bufs;
	t_opt				opts;
	t_mtl				*mtl;
}						t_obj;

typedef	struct			s_scop
{
	GLFWwindow			*win;
	size_t				ob_size;
	t_obj				*ob;
	t_camera			cam;
	t_vec3f				angle;
	t_vec3f				move;
	int 				is_color;
	int 				gray;
	int 				x_axis;
	int 				y_axis;
	int 				z_axis;
}						t_scop;

#endif