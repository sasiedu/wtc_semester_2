/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   loadshader.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/27 01:22:28 by sasiedu           #+#    #+#             */
/*   Updated: 2016/11/27 18:23:44 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/scop.h"

static size_t	shader_code_size(char *path)
{
	size_t	code_size;
	int		fd;
	char	dir[500];
	char	*line;

	code_size = 0;
	bzero(dir, sizeof(char) * 500);
	strcat(&dir[0], "shaders/");
	strcat(&dir[0], path);
	if ((fd = open(dir, O_RDONLY)) == -1)
	{
		ft_putendl_fd("Error: failed to open shader file", 2);
		return (-1);
	}
	while (ft_gnl(fd, &line))
	{
		code_size += ft_strlen(line);
		code_size += 1;
		ft_strdel(&line);
	}
	close(fd);
	return (code_size);
}

char			*read_shader(char *path, char *line, int code_size)
{
	int		fd;
	char	dir[500];
	char	*shader_code;

	code_size = shader_code_size(path);
	shader_code = (char *)malloc(sizeof(char) * code_size + 1);
	bzero(shader_code, sizeof(char) * code_size + 1);
	bzero(dir, sizeof(char) * 500);
	strcat(&dir[0], "shaders/");
	strcat(&dir[0], path);
	if ((fd = open(dir, O_RDONLY)) == -1)
	{
		ft_putendl_fd("Error: failed to open shader file", 2);
		return (NULL);
	}
	while (ft_gnl(fd, &line))
	{
		strcat(shader_code, line);
		strcat(shader_code, "\n");
		ft_strdel(&line);
	}
	close(fd);
	return (shader_code);
}

GLuint			compile_shader_code(char *code, GLuint shader_id)
{
	GLint	result;
	char	error_msg[5000];
	int		info_log_length;

	result = GL_FALSE;
	if (code == NULL)
		return (9);
	glShaderSource(shader_id, 1, (const GLchar *const *)&code, NULL);
	glCompileShader(shader_id);
	glGetShaderiv(shader_id, GL_COMPILE_STATUS, &result);
	glGetShaderiv(shader_id, GL_INFO_LOG_LENGTH, &info_log_length);
	if (info_log_length > 0)
	{
		glGetShaderInfoLog(shader_id, info_log_length, NULL, &error_msg[0]);
		ft_putendl_fd(error_msg, 1);
		return (9);
	}
	ft_strdel(&code);
	return (shader_id);
}

GLuint			link_program(GLuint vertex_shader_id, GLuint frag_shader_id)
{
	GLuint	program_id;
	GLint	result;
	char	error_msg[5000];
	int		info_log_length;

	program_id = glCreateProgram();
	glAttachShader(program_id, vertex_shader_id);
	glAttachShader(program_id, frag_shader_id);
	glLinkProgram(program_id);
	glGetProgramiv(program_id, GL_LINK_STATUS, &result);
	glGetProgramiv(program_id, GL_INFO_LOG_LENGTH, &info_log_length);
	if (info_log_length > 0)
	{
		glGetProgramInfoLog(program_id, info_log_length, NULL, &error_msg[0]);
		ft_putendl_fd(error_msg, 1);
		return (9);
	}
	glDetachShader(program_id, vertex_shader_id);
	glDetachShader(program_id, frag_shader_id);
	return (program_id);
}

GLuint			load_shaders(const char *vertex_path, char *fragment_path)
{
	GLuint	vertex_shader_id;
	GLuint	fragment_shader_id;
	GLuint	program_id;

	vertex_shader_id = glCreateShader(GL_VERTEX_SHADER);
	fragment_shader_id = glCreateShader(GL_FRAGMENT_SHADER);
	if ((vertex_shader_id = compile_shader_code(read_shader(\
						(char *)vertex_path, NULL, 0), vertex_shader_id)) == 9)
		return (9);
	if ((fragment_shader_id = compile_shader_code(read_shader(fragment_path, \
						NULL, 0), fragment_shader_id)) == 9)
		return (9);
	program_id = link_program(vertex_shader_id, fragment_shader_id);
	glDeleteShader(vertex_shader_id);
	glDeleteShader(fragment_shader_id);
	return (program_id);
}
