/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   loadtga.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/27 00:26:17 by sasiedu           #+#    #+#             */
/*   Updated: 2016/11/27 18:25:06 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/scop.h"

static GLuint			create_opengl_texture(unsigned int width, \
							unsigned int height, unsigned char *data)
{
	GLuint	texture_id;

	glGenTextures(1, &texture_id);
	glBindTexture(GL_TEXTURE_2D, texture_id);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_BGRA, \
			GL_UNSIGNED_BYTE, data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, \
										GL_LINEAR_MIPMAP_LINEAR);
	glGenerateMipmap(GL_TEXTURE_2D);
	return (texture_id);
}

static GLuint			read_tga(FILE *fp, t_tgaheader *header, int w, int h)
{
	int				bpp;
	long int		imagesize;
	unsigned char	*data;

	w = (int)header->width;
	h = (int)header->height;
	bpp = (int)header->bpp;
	if (bpp != 32)
	{
		ft_putendl_fd("tga file is not 32 bit", 2);
		return (0);
	}
	imagesize = w * h * 4;
	data = (unsigned char *)malloc(sizeof(unsigned char) * imagesize);
	fread(data, 1, imagesize, fp);
	return (create_opengl_texture(w, h, data));
}

GLuint					load_tga(const char *imagepath)
{
	GLuint			texture_id;
	FILE			*fp;
	t_tgaheader		header;

	fp = fopen(imagepath, "rb");
	if (!fp)
	{
		ft_putendl_fd("Failed to open tga file", 2);
		return (0);
	}
	fread(&header, sizeof(t_tgaheader), 1, fp);
	texture_id = read_tga(fp, &header, 0, 0);
	fclose(fp);
	return (texture_id);
}
