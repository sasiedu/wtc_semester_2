/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vertex1.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/26 23:59:04 by sasiedu           #+#    #+#             */
/*   Updated: 2016/11/27 18:15:38 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/scop.h"

t_vec3f		vec3add(t_vec3f a, t_vec3f b)
{
	return ((t_vec3f){a.x + b.x, a.y + b.y, a.z + b.z});
}

t_vec3f		vec3sub(t_vec3f a, t_vec3f b)
{
	return ((t_vec3f){a.x - b.x, a.y - b.y, a.z - b.z});
}

t_vec3f		vec3cross(t_vec3f a, t_vec3f b)
{
	return ((t_vec3f){CROSS_OP(y, z), CROSS_OP(z, x), CROSS_OP(x, y)});
}

float		vec3dot(t_vec3f a, t_vec3f b)
{
	return (DOT_OP(x) + DOT_OP(y) + DOT_OP(z));
}

void		vec3print(t_vec3f v)
{
	printf("%f %f %f\n", v.x, v.y, v.z);
}
