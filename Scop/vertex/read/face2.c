/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   face2.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/27 02:44:43 by sasiedu           #+#    #+#             */
/*   Updated: 2016/11/27 18:03:09 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/scop.h"

static t_obj		*bind_vertices(t_obj *ob, t_tmp *t, t_face *p, char *line)
{
	t_split_string	v;

	v = ft_nstrsplit(line, '/');
	ob = add_gl_vertices(atoi(v.strings[0]) - 1, ob, t, p);
	ob = add_gl_normals(&v, ob, t);
	ob = add_gl_uvs(&v, ob, t);
	return (ob);
}

static void			normal(t_face *p, size_t i, t_split_string *v)
{
	p->first = v->strings[i];
	p->second = v->strings[i + 1];
	p->third = v->strings[i + 2];
}

static void			abnormal(t_face *p, size_t i, t_split_string *v)
{
	p->first = v->strings[i];
	p->second = v->strings[i + 1];
	p->third = v->strings[0];
}

static	t_obj		*build_indices(t_obj *ob, t_split_string *v, \
							size_t i, t_tmp *t)
{
	t_face		p;

	p.c1 = ((float)rand() / RAND_MAX);
	p.c2 = ((float)rand() / RAND_MAX);
	p.gray = (t_vec3f){p.c1, p.c1, p.c1};
	p.color = (t_vec3f){p.c2, p.c1, p.c2};
	while (i < v->words)
	{
		if (i == v->words - 1)
			break ;
		if (i == v->words - 2)
			abnormal(&p, i, v);
		else
			normal(&p, i, v);
		ob = bind_vertices(ob, t, &p, p.first);
		ob = bind_vertices(ob, t, &p, p.second);
		ob = bind_vertices(ob, t, &p, p.third);
		i += 2;
	}
	return (ob);
}

void				expand_faces(t_obj *ob, t_tmp *tmp, size_t i, \
								size_t face_ind)
{
	t_split_string	values;
	char			*line;

	ob = get_min_max(ob, tmp, -1);
	while (++face_ind < tmp->f_count)
	{
		i = 0;
		line = tmp->faces[face_ind].line;
		while (line[i] == ' ' || line[i] == '\t' || line[i] == 'f')
			i++;
		if (line[i] != '\0')
		{
			values = ft_nstrsplit(&line[i], ' ');
			ob = build_indices(ob, &values, 0, tmp);
			ft_free_split(&values);
		}
	}
}
