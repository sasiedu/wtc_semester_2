/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   loadobj.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/27 01:54:49 by sasiedu           #+#    #+#             */
/*   Updated: 2016/11/27 17:59:15 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/scop.h"

int					g_count = 0;

static ssize_t		read_line(FILE *file, char **line)
{
	char		*tmp;
	ssize_t		len;
	size_t		linecap;

	linecap = 0;
	tmp = NULL;
	len = getline(&tmp, &linecap, file);
	*line = tmp;
	return (len);
}

static t_tmp		*init_obj(void)
{
	t_tmp		*obj;

	if ((obj = (t_tmp *)malloc(sizeof(t_tmp))) == NULL)
		return (NULL);
	obj->f_count = 0;
	obj->v_count = 0;
	obj->vc_count = 0;
	obj->vn_count = 0;
	obj->vt_count = 0;
	obj->faces = NULL;
	obj->v = NULL;
	obj->vc = NULL;
	obj->vt = NULL;
	obj->vn = NULL;
	return (obj);
}

static t_scop		*preset(t_scop *e)
{
	e->ob_size += 1;
	e->ob = (t_obj *)realloc(e->ob, sizeof(t_obj) * e->ob_size);
	e->ob[e->ob_size - 1].opts.face_check = 0;
	e->ob[e->ob_size - 1].opts.has_texture = 0;
	e->ob[e->ob_size - 1].opts.has_normal = 0;
	return (e);
}

static t_scop		*process_line(t_scop *e, t_tmp **obj, char *line)
{
	if (line[0] == 'o')
	{
		g_count++;
		if (g_count > 1)
			e = process_object(e, &(*obj));
	}
	if (line[0] == 'v' && line[1] == ' ')
		add_vertices(&(*obj), line);
	if (line[0] == 'v' && line[1] == 'n')
		(*obj)->vn = add_vnormals((*obj)->vn, &(*obj)->vn_count, line);
	if (line[0] == 'v' && line[1] == 't')
		(*obj)->vt = add_vtextures((*obj)->vt, &(*obj)->vt_count, line);
	if (line[0] == 'f')
		(*obj)->faces = add_face((*obj)->faces, &(*obj)->f_count, \
				line, &e->ob[e->ob_size - 1]);
	return (e);
}

t_scop				*load_obj(t_scop *e, char *file)
{
	FILE		*f;
	t_tmp		*obj;
	char		*line;
	ssize_t		len;

	f = fopen(file, "r");
	e->ob_size = 0;
	e->ob = NULL;
	if (f == NULL || (obj = init_obj()) == NULL)
	{
		ft_putendl_fd("failed to open obj file", 2);
		return (NULL);
	}
	len = 1;
	e = preset(e);
	while (len != -1)
	{
		len = read_line(f, &line);
		if (len != -1)
			e = process_line(e, &obj, line);
	}
	e = process_object(e, &obj);
	if (obj != NULL)
		free(obj);
	return (e);
}
