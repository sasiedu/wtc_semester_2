/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vertex.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/27 10:27:03 by sasiedu           #+#    #+#             */
/*   Updated: 2016/11/27 18:00:12 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/scop.h"

static t_obj	*for_x(t_obj *ob, t_vec3f c, t_vec3f v, t_face *p)
{
	ob->meshs.vs[ob->meshs.size - 1] = v.x;
	ob->meshs.v_gray[ob->meshs.size - 1] = p->gray.x;
	ob->meshs.v_color[ob->meshs.size - 1] = c.x;
	return (ob);
}

static t_obj	*for_y(t_obj *ob, t_vec3f c, t_vec3f v, t_face *p)
{
	ob->meshs.vs[ob->meshs.size - 1] = v.y;
	ob->meshs.v_gray[ob->meshs.size - 1] = p->gray.y;
	ob->meshs.v_color[ob->meshs.size - 1] = c.y;
	return (ob);
}

static t_obj	*for_z(t_obj *ob, t_vec3f c, t_vec3f v, t_face *p)
{
	ob->meshs.vs[ob->meshs.size - 1] = v.z;
	ob->meshs.v_gray[ob->meshs.size - 1] = p->gray.z;
	ob->meshs.v_color[ob->meshs.size - 1] = c.z;
	return (ob);
}

t_obj			*add_gl_vertices(int ind, t_obj *ob, t_tmp *t, t_face *p)
{
	t_vec3f		color;
	t_vec3f		v;
	int			i;

	if (t->vc_count > 0)
		color = t->vc[ind];
	else
		color = p->color;
	v = t->v[ind];
	i = -1;
	while (++i < 3)
	{
		ob->meshs.size += 1;
		if (i == 0)
			ob = for_x(ob, color, v, p);
		else if (i == 1)
			ob = for_y(ob, color, v, p);
		else
			ob = for_z(ob, color, v, p);
	}
	return (ob);
}
