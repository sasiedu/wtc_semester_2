/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   uvs.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/27 11:02:07 by sasiedu           #+#    #+#             */
/*   Updated: 2016/11/27 17:59:35 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/scop.h"

static t_obj		*add_values(t_obj *ob, t_vec2f uv)
{
	ob->meshs.t_size += 1;
	ob->meshs.uvs[ob->meshs.t_size - 1] = uv.x;
	ob->meshs.t_size += 1;
	ob->meshs.uvs[ob->meshs.t_size - 1] = uv.y;
	return (ob);
}

t_obj				*add_gl_uvs(t_split_string *v, t_obj *ob, t_tmp *t)
{
	t_vec2f		uv;
	t_vec3f		vec;

	if (ob->opts.has_texture == 1)
		uv = t->vt[atoi(v->strings[1]) - 1];
	else
	{
		vec = t->v[atoi(v->strings[0]) - 1];
		uv.x = ((float)rand() / RAND_MAX);
		uv.y = ((float)rand() / RAND_MAX);
	}
	ob = add_values(ob, uv);
	return (ob);
}
