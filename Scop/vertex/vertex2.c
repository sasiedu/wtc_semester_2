/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vertex2.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/26 23:59:08 by sasiedu           #+#    #+#             */
/*   Updated: 2016/11/27 18:17:18 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/scop.h"

t_vec3f		vec3normalise(t_vec3f v)
{
	float	coef;

	coef = sqrt(v.x * v.x + v.y * v.y + v.z * v.z);
	if (coef != 0.0f)
	{
		v.x /= coef;
		v.y /= coef;
		v.z /= coef;
	}
	return (v);
}

t_vec3f		vec3inverse(t_vec3f v)
{
	return ((t_vec3f){-v.x, -v.y, -v.z});
}

void		vec3copy(t_vec3f *dst, t_vec3f *old)
{
	dst->x = old->x;
	dst->y = old->y;
	dst->z = old->z;
}
