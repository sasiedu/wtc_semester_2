/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   matrix2.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/26 23:23:28 by sasiedu           #+#    #+#             */
/*   Updated: 2016/11/26 23:23:30 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/scop.h"

static	void	euler_angle(t_vec3f *c, t_vec3f *s, t_vec3f angle)
{
	c->x = cos(-angle.x * PI / 180);
	c->y = cos(-angle.y * PI / 180);
	c->z = cos(-angle.z * PI / 180);
	s->x = sin(-angle.x * PI / 180);
	s->y = sin(-angle.y * PI / 180);
	s->z = sin(-angle.z * PI / 180);
}

t_mat4			mat4rotation(t_vec3f angle)
{
	t_mat4		mat;
	t_vec3f		s;
	t_vec3f		c;

	euler_angle(&c, &s, angle);
	mat4init(&mat);
	mat.mat[0][0] = c.y * c.z;
	mat.mat[0][1] = -c.x * s.z + s.x * s.y * c.z;
	mat.mat[0][2] = s.x * s.z + c.x * s.y * c.z;
	mat.mat[1][0] = c.y * s.z;
	mat.mat[1][1] = c.x * c.z + s.x * s.y * s.z;
	mat.mat[1][2] = -s.x * c.z + c.x * s.y * s.z;
	mat.mat[2][0] = -s.y;
	mat.mat[2][1] = s.x * c.y;
	mat.mat[2][2] = c.x * c.y;
	mat.mat[3][3] = 1.0f;
	return (mat);
}

t_mat4			mat4translate(t_vec3f translate)
{
	t_mat4	mat;

	mat4identity(&mat);
	mat.mat[3][0] = translate.x;
	mat.mat[3][1] = translate.y;
	mat.mat[3][2] = translate.z;
	return (mat);
}