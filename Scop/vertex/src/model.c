/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   model.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/27 14:31:09 by sasiedu           #+#    #+#             */
/*   Updated: 2016/11/27 18:07:00 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/scop.h"

void		perform_rotation(t_obj *ob, t_scop *e)
{
	t_mat4	rot;
	t_mat4	trans;
	t_mat4	temp;
	t_vec3f	cent;

	cent = (t_vec3f){-ob->opts.center.x, -ob->opts.center.x, \
		-ob->opts.center.x};
	mat4identity(&temp);
	trans = mat4translate(cent);
	temp = mat4dot(temp, trans);
	trans = mat4translate(e->move);
	temp = mat4dot(temp, trans);
	rot = mat4rotation(e->angle);
	temp = mat4dot(temp, rot);
	trans = mat4translate(ob->opts.center);
	temp = mat4dot(temp, trans);
	ob->mvps.m = temp;
}
