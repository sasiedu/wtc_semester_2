/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   render.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/27 14:07:50 by sasiedu           #+#    #+#             */
/*   Updated: 2016/11/27 18:12:17 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/scop.h"

static void		using_color(t_obj *ob)
{
	glUseProgram(ob->ids.program);
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, ob->bufs.v);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, ob->bufs.vc);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	glEnableVertexAttribArray(2);
	glBindBuffer(GL_ARRAY_BUFFER, ob->bufs.vn);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	glDrawArrays(GL_TRIANGLES, 0, ob->meshs.size);
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);
}

static void		using_texture(t_obj *ob)
{
	glUseProgram(ob->tids.program);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, ob->bufs.t_tga);
	glUniform1i(ob->tids.texture, 0);
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, ob->bufs.v);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, ob->bufs.uv);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);
	glEnableVertexAttribArray(2);
	glBindBuffer(GL_ARRAY_BUFFER, ob->bufs.vn);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	glDrawArrays(GL_TRIANGLES, 0, ob->meshs.size);
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);
}

void			render_object(t_obj *ob, int is_color, int is_gray)
{
	if (is_gray == OFF)
	{
		glGenBuffers(1, &ob->bufs.vc);
		glBindBuffer(GL_ARRAY_BUFFER, ob->bufs.vc);
		glBufferData(GL_ARRAY_BUFFER, 4 * ob->meshs.size, ob->meshs.v_color, \
				GL_STATIC_DRAW);
	}
	else
	{
		glGenBuffers(1, &ob->bufs.vc);
		glBindBuffer(GL_ARRAY_BUFFER, ob->bufs.vc);
		glBufferData(GL_ARRAY_BUFFER, 4 * ob->meshs.size, ob->meshs.v_gray, \
				GL_STATIC_DRAW);
	}
	glUniformMatrix4fv(ob->ids.mvp, 1, GL_FALSE, &ob->mvps.mvp.mat[0][0]);
	glUniformMatrix4fv(ob->ids.view, 1, GL_FALSE, &ob->mvps.v.mat[0][0]);
	glUniformMatrix4fv(ob->ids.model, 1, GL_FALSE, &ob->mvps.m.mat[0][0]);
	if (is_color == ON)
		using_color(ob);
	else
		using_texture(ob);
}
