/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   setup.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/27 11:58:35 by sasiedu           #+#    #+#             */
/*   Updated: 2016/11/27 18:14:58 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/scop.h"

static t_scop		*setup_camera(t_scop *e)
{
	e->cam.pos = (t_vec3f){0, 0, -20};
	e->cam.dir = (t_vec3f){0, 0, 0};
	e->cam.up = (t_vec3f){0, 1, 0};
	e->cam.far = 10000.0f;
	e->cam.near = 0.1f;
	e->cam.fov = 90.0f;
	e->cam.w = 800;
	e->cam.h = 800;
	e->cam.ratio = (float)e->cam.w / e->cam.h;
	return (e);
}

static int			glfw_setup(t_scop *e)
{
	if (!glfwInit())
	{
		ft_putendl_fd("Failed to initialize GLFW", 2);
		return (-1);
	}
	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	e = setup_camera(e);
	return (0);
}

static GLFWwindow	*setup_window(t_scop *e, char *file)
{
	GLFWwindow		*window;

	window = glfwCreateWindow(e->cam.w, e->cam.h, file, NULL, NULL);
	if (window == NULL)
	{
		ft_putendl_fd("Failed to open GLFW window", 2);
		glfwTerminate();
		return (NULL);
	}
	glfwMakeContextCurrent(window);
	glewExperimental = 1;
	if (glewInit() != GLEW_OK)
	{
		ft_putendl_fd("Failed to initialize GLEW", 2);
		glfwTerminate();
		return (NULL);
	}
	return (window);
}

static int			final_setup(t_obj *ob, size_t i)
{
	i = 0;
	glGenVertexArrays(1, &ob->ids.v_array);
	glBindVertexArray(ob->ids.v_array);
	if ((ob->ids.program = load_shaders("color.vert", "color.frag")) == 9)
		return (-1);
	if ((ob->tids.program = load_shaders("tex.vert", "tex.frag")) == 9)
		return (-1);
	ob->ids.mvp = glGetUniformLocation(ob->ids.program, "MVP");
	ob->ids.view = glGetUniformLocation(ob->ids.program, "M");
	ob->ids.model = glGetUniformLocation(ob->ids.program, "V");
	ob->tids.mvp = glGetUniformLocation(ob->tids.program, "MVP");
	ob->tids.view = glGetUniformLocation(ob->tids.program, "M");
	ob->tids.model = glGetUniformLocation(ob->tids.program, "V");
	ob->locs.v = glGetAttribLocation(ob->ids.program, "vertexPosition");
	ob->locs.uv = glGetAttribLocation(ob->ids.program, "vertexUV");
	ob->locs.vc = glGetAttribLocation(ob->ids.program, "vertexColor");
	ob->locs.vn = glGetAttribLocation(ob->ids.program, "vertexNormal");
	ob->opts.angle = (t_vec3f){0.0f, 0.0f, 0.0f};
	ob->opts.move = (t_vec3f){0.0f, 0.0f, 0.0f};
	ob->bufs.t_tga = load_tga("../textures/Porcelain.tga");
	return (0);
}

t_scop				*setup_env(t_scop *e, char **av, int ret, size_t i)
{
	if (glfw_setup(e) == 0)
	{
		if ((e->win = setup_window(e, av[1])) != NULL)
		{
			if ((e = load_obj(e, av[1])) != NULL)
			{
				while (++i < e->ob_size)
				{
					ret = final_setup(&e->ob[i], i);
					if (ret == -1)
					{
						free(e);
						return (NULL);
					}
				}
				return (e);
			}
		}
	}
	free(e);
	return (NULL);
}
