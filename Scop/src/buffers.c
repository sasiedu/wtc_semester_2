/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   buffers.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/27 13:47:38 by sasiedu           #+#    #+#             */
/*   Updated: 2016/11/27 18:05:24 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/scop.h"

static void					load_object_buffers(t_obj *ob, size_t i)
{
	i = 0;
	glGenBuffers(1, &ob->bufs.v);
	glBindBuffer(GL_ARRAY_BUFFER, ob->bufs.v);
	glBufferData(GL_ARRAY_BUFFER, 4 * ob->meshs.size, ob->meshs.vs, \
			GL_STATIC_DRAW);
	glGenBuffers(1, &ob->bufs.vc);
	glBindBuffer(GL_ARRAY_BUFFER, ob->bufs.vc);
	glBufferData(GL_ARRAY_BUFFER, 4 * ob->meshs.size, ob->meshs.v_gray, \
			GL_STATIC_DRAW);
	glGenBuffers(1, &ob->bufs.vn);
	glBindBuffer(GL_ARRAY_BUFFER, ob->bufs.vn);
	glBufferData(GL_ARRAY_BUFFER, 4 * ob->meshs.n_size, ob->meshs.vns, \
			GL_STATIC_DRAW);
	glGenBuffers(1, &ob->bufs.uv);
	glBindBuffer(GL_ARRAY_BUFFER, ob->bufs.uv);
	glBufferData(GL_ARRAY_BUFFER, 4 * ob->meshs.t_size, ob->meshs.uvs, \
			GL_STATIC_DRAW);
}

t_scop						*load_buffers(t_scop *e, size_t i)
{
	while (++i < e->ob_size)
	{
		load_object_buffers(&e->ob[i], i);
	}
	return (e);
}
