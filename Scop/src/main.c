/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/27 03:10:00 by sasiedu           #+#    #+#             */
/*   Updated: 2016/11/27 18:13:40 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/scop.h"

static t_scop	*render_objects(t_scop *e, size_t i)
{
	while (++i < e->ob_size)
	{
		e->ob[i].mvps.v = glm_lookat(e->cam.pos, e->cam.dir, e->cam.up);
		e->ob[i].mvps.p = glm_perspective(e->cam.fov, e->cam.ratio, \
				e->cam.near, e->cam.far);
		perform_rotation(&e->ob[i], e);
		e->ob[i].mvps.mvp = mat4dot(e->ob[i].mvps.m, e->ob[i].mvps.v);
		e->ob[i].mvps.mvp = mat4dot(e->ob[i].mvps.mvp, e->ob[i].mvps.p);
		render_object(&e->ob[i], e->is_color, e->gray);
	}
	return (e);
}

static void		begin_loop(t_scop *e)
{
	while (glfwGetKey(e->win, GLFW_KEY_ESCAPE) != GLFW_PRESS \
					&& glfwWindowShouldClose(e->win) == 0)
	{
		e = check_keys(e);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		e = render_objects(e, -1);
		glfwSwapBuffers(e->win);
		glfwPollEvents();
	}
}

int				main(int ac, char **av)
{
	t_scop		*env;

	env = (t_scop *)malloc(sizeof(t_scop));
	if (ac != 2)
		return (-1);
	env = setup_env(env, av, 0, -1);
	if (env != NULL)
	{
		glfwSetInputMode(env->win, GLFW_STICKY_KEYS, GL_TRUE);
		glClearColor(0.0f, 0.0f, 0.4f, 0.0f);
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LESS);
		env = load_buffers(env, -1);
		printf("object count : %zu\n", env->ob_size);
		begin_loop(env);
	}
	return (0);
}
