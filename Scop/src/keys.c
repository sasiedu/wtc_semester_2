/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   keys.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/27 15:49:32 by sasiedu           #+#    #+#             */
/*   Updated: 2016/11/27 18:11:29 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/scop.h"

static t_scop		*check_colors2(t_scop *e)
{
	if (glfwGetKey(e->win, GLFW_KEY_X) == GLFW_PRESS)
	{
		if (glfwGetKey(e->win, GLFW_KEY_X) == GLFW_RELEASE)
			e->x_axis = (e->x_axis == ON) ? OFF : ON;
	}
	if (glfwGetKey(e->win, GLFW_KEY_Y) == GLFW_PRESS)
	{
		if (glfwGetKey(e->win, GLFW_KEY_Y) == GLFW_RELEASE)
			e->y_axis = (e->y_axis == ON) ? OFF : ON;
	}
	return (e);
}

static t_scop		*check_colors(t_scop *e)
{
	e = check_colors2(e);
	if (glfwGetKey(e->win, GLFW_KEY_Z) == GLFW_PRESS)
	{
		if (glfwGetKey(e->win, GLFW_KEY_Z) == GLFW_RELEASE)
			e->z_axis = (e->z_axis == ON) ? OFF : ON;
	}
	if (glfwGetKey(e->win, GLFW_KEY_W) == GLFW_PRESS)
	{
		if (glfwGetKey(e->win, GLFW_KEY_W) == GLFW_RELEASE)
			e->gray = (e->gray == ON) ? OFF : ON;
	}
	if (glfwGetKey(e->win, GLFW_KEY_O) == GLFW_PRESS)
	{
		if (glfwGetKey(e->win, GLFW_KEY_O) == GLFW_RELEASE)
			e->is_color = (e->is_color == OFF) ? ON : OFF;
	}
	return (e);
}

t_scop				*check_keys(t_scop *e)
{
	if (glfwGetKey(e->win, GLFW_KEY_LEFT) == GLFW_PRESS)
		e->move.x += 0.05f;
	if (glfwGetKey(e->win, GLFW_KEY_RIGHT) == GLFW_PRESS)
		e->move.x -= 0.05f;
	if (glfwGetKey(e->win, GLFW_KEY_UP) == GLFW_PRESS)
		e->move.y += 0.05f;
	if (glfwGetKey(e->win, GLFW_KEY_DOWN) == GLFW_PRESS)
		e->move.y -= 0.05f;
	if (glfwGetKey(e->win, GLFW_KEY_PAGE_UP) == GLFW_PRESS)
		e->move.z += 0.05f;
	if (glfwGetKey(e->win, GLFW_KEY_PAGE_DOWN) == GLFW_PRESS)
		e->move.z -= 0.05f;
	e = check_colors(e);
	if (e->x_axis == ON)
		e->angle.x = (e->angle.x >= 359.0f) ? 0.0f : e->angle.x + 1.0f;
	if (e->y_axis == ON)
		e->angle.y = (e->angle.y >= 359.0f) ? 0.0f : e->angle.y + 1.0f;
	if (e->z_axis == ON)
		e->angle.z = (e->angle.z >= 359.0f) ? 0.0f : e->angle.z + 1.0f;
	return (e);
}
