/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   perspective.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/27 00:14:57 by sasiedu           #+#    #+#             */
/*   Updated: 2016/11/27 18:26:12 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/scop.h"

t_mat4		glm_perspective(float fov, float aspect, float near, float far)
{
	t_mat4	mat;
	float	half_fov;
	float	radians;

	mat4init(&mat);
	radians = fov * (PI / 180);
	half_fov = tan(radians / 2);
	mat.mat[0][0] = 1 / (aspect * half_fov);
	mat.mat[1][1] = 1 / (half_fov);
	mat.mat[2][2] = -(far + near) / (far - near);
	mat.mat[2][3] = -1.0f;
	mat.mat[3][2] = -(2 * far * near) / (far - near);
	return (mat);
}
