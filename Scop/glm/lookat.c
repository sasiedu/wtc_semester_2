/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lookat.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/27 00:16:56 by sasiedu           #+#    #+#             */
/*   Updated: 2016/11/27 18:24:15 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/scop.h"

static void		fill_matrix(t_mat4 *mat, t_vec3f f, t_vec3f s, t_vec3f u)
{
	mat->mat[0][0] = s.x;
	mat->mat[1][0] = s.y;
	mat->mat[2][0] = s.z;
	mat->mat[0][1] = u.x;
	mat->mat[1][1] = u.y;
	mat->mat[2][1] = u.z;
	mat->mat[0][2] = -f.x;
	mat->mat[1][2] = -f.y;
	mat->mat[2][2] = -f.z;
}

t_mat4			glm_lookat(t_vec3f eye, t_vec3f center, t_vec3f up)
{
	t_vec3f		f;
	t_vec3f		s;
	t_vec3f		u;
	t_mat4		mat;

	f = vec3normalise(vec3sub(center, eye));
	s = vec3normalise(vec3cross(f, up));
	u = vec3cross(s, f);
	mat4init(&mat);
	fill_matrix(&mat, f, s, u);
	mat.mat[3][0] = -vec3dot(s, eye);
	mat.mat[3][1] = -vec3dot(u, eye);
	mat.mat[3][2] = vec3dot(f, eye);
	mat.mat[3][3] = 1.0f;
	return (mat);
}
