/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   scop.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/26 23:23:00 by sasiedu           #+#    #+#             */
/*   Updated: 2016/11/26 23:23:03 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SCOP_H
# define SCOP_H

# include "glm.h"

/*
 * read and process obj file functions
 */
t_scop			*load_obj(t_scop *e, char *file);
t_scop			*process_object(t_scop *e, t_tmp **obj);
t_face			*add_face(t_face *f, size_t *f_count, char *line, t_obj *ob);
t_vec3f			*add_vnormals(t_vec3f *vn, size_t *vn_count, char *line);
t_vec2f			*add_vtextures(t_vec2f *vt, size_t *vt_count, char *line);
t_obj			*add_gl_vertices(int ind, t_obj *ob, t_tmp *t, t_face *p);
t_obj			*add_gl_normals(t_split_string *v, t_obj *ob, t_tmp *t);
t_obj			*add_gl_uvs(t_split_string *v, t_obj *ob, t_tmp *t);
t_obj			*get_min_max(t_obj *ob, t_tmp *t, size_t i);
t_obj			*center_and_uv(t_obj *ob);
void			add_vertices(t_tmp **o, char *line);
void			expand_faces(t_obj *ob, t_tmp *tmp, size_t i, size_t face_ind);

/*
 * linker functions
 */
t_scop			*setup_env(t_scop *e, char **av, int ret, size_t i);
t_obj			*ids_for_objects(t_obj *ob);

/*
 * rendering, movement and rotation functions
 */
t_scop			*load_buffers(t_scop *e, size_t i);
t_scop			*check_keys(t_scop *e);
void			perform_rotation(t_obj *ob, t_scop *e);
void			render_object(t_obj *ob, int is_color, int is_gray);

#endif
