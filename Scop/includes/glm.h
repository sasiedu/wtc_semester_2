/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   glm.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/26 23:27:41 by sasiedu           #+#    #+#             */
/*   Updated: 2016/11/26 23:27:44 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GLM_H
# define GLM_H

# include "tools.h"

t_mat4		glm_perspective(float fov, float aspect, float near, float far);
t_mat4		glm_lookat(t_vec3f eye, t_vec3f center, t_vec3f up);

GLuint 		load_tga(const char *imagepath);
GLuint 		load_shaders(const char *vertex_path, char *fragment_path);

#endif