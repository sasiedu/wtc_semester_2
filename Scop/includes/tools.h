/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/26 23:25:34 by sasiedu           #+#    #+#             */
/*   Updated: 2016/11/26 23:25:36 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TOOLS_H
# define TOOLS_H

# include "structs2.h"
# include "defines.h"

/*
 * matrix functions
 */
t_mat4		mat4dot(t_mat4 a, t_mat4 b);
t_mat4		mat4rotation(t_vec3f angle);
t_mat4		mat4translate(t_vec3f translate);
void		mat4init(t_mat4 *mat);
void		mat4identity(t_mat4 *mat);
void		mat4print(t_mat4 mat);
void		mat4copy(t_mat4 *dst, t_mat4 *old);

/*
 * advance matrix functions
 */


/*
 * vertex functions
 */
t_vec3f		vec3add(t_vec3f a, t_vec3f b);
t_vec3f		vec3sub(t_vec3f a, t_vec3f b);
t_vec3f		vec3cross(t_vec3f a, t_vec3f b);
t_vec3f		vec3normalise(t_vec3f v);
t_vec3f		vec3inverse(t_vec3f v);
float		vec3dot(t_vec3f a, t_vec3f b);
void		vec3print(t_vec3f v);
void		vec3copy(t_vec3f *dst, t_vec3f *old);

#endif
