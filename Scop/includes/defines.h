/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   defines.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/26 23:24:38 by sasiedu           #+#    #+#             */
/*   Updated: 2016/11/26 23:24:39 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DEFINES_H
# define DEFINES_H

# define CROSS_OP(A, B) (a.A * b.B - a.B * b.A)
# define DOT_OP(M) (a.M * b.M)

# define PI 3.14159

# define WIDTH 800
# define HEIGHT 800

# define X_AXIS 0
# define Y_AXIS 1
# define Z_AXIS 2

# define ON 1
# define OFF 0

# define FOURCC_DXT1 0x31545844
# define FOURCC_DXT3 0x33545844
# define FOURCC_DXT5 0x35545844

#endif