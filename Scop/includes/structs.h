/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   struct.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/21 17:25:06 by sasiedu           #+#    #+#             */
/*   Updated: 2016/11/21 17:25:08 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef STRUCT_H
# define STRUCT_H

# include "../Glew/include/GL/glew.h"
# include "../Glfw/include/GLFW/glfw3.h"
# include <OpenGL/OpenGL.h>
# include <GLUT/glut.h>
# include <math.h>
# include "../mylib/includes/mylib.h"

typedef struct              s_vec3f
{
    float                   x;
    float                   y;
    float                   z;
}                           t_vec3f;

typedef struct              s_vec2f
{
    float                   x;
    float                   y;
}                           t_vec2f;

typedef struct              s_mat4
{
	float                   mat[4][4];
}                           t_mat4;

typedef struct              s_face
{
    char                    *line;
    char                    *first;
    char                    *second;
    char                    *third;
    float                   c1;
    float                   c2;
    t_vec3f                 gray;
    t_vec3f                 color;
}                           t_face;

typedef struct              s_camera
{
    t_vec3f                 pos;
    t_vec3f                 dir;
    t_vec3f                 right;
    t_vec3f                 up;
	int                     w;
	int                     h;
    float                   fov;
    float                   ratio;
    float                   near;
    float                   far;
}                           t_camera;

typedef struct              s_opt
{
	t_vec3f                 min;
	t_vec3f                 max;
	t_vec3f                 center;
	t_vec3f                 angle;
	t_vec3f					move;
	t_vec2f					for_uv;
	int                     is_color;
	int 		            face_check;
	int 		            has_texture;
	int 		            has_normal;
}                           t_opt;

typedef struct 				s_mtl
{
	char 					*name;
	float 					ka;
	float 					kd;
	float 					ks;
	float 					d;
	float 					ni;
	float 					ns;
	float 					illum;
	struct s_mtl			*next;
}							t_mtl;

typedef struct              s_tmp
{
	size_t                  indices;
	size_t                  f_count;
	t_face                  *faces;
	size_t                  v_count;
	t_vec3f                 *v;
	size_t                  vc_count;
	t_vec3f                 *vc;
	size_t                  vt_count;
	t_vec2f                 *vt;
	size_t                  vn_count;
	t_vec3f                 *vn;
	t_mtl					*mtls;
}                           t_tmp;

typedef struct		        s_tgaheader
{
	unsigned char           data1[12];
	unsigned short          width;
	unsigned short          height;
	unsigned char           bpp;
	unsigned char           data2;
}				            t_tgaheader;

#endif
