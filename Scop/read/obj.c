/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   obj.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/27 02:02:59 by sasiedu           #+#    #+#             */
/*   Updated: 2016/11/27 17:55:19 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/scop.h"

static t_scop	*obj_to_e(t_scop *e, t_tmp *obj)
{
	e->ob[e->ob_size - 1].meshs.size = 0;
	e->ob[e->ob_size - 1].meshs.t_size = 0;
	e->ob[e->ob_size - 1].meshs.n_size = 0;
	printf("object number : %zu\n", e->ob_size);
	printf("total v : %zu\n", obj->v_count);
	printf("total vn : %zu\n", obj->vn_count);
	printf("total vt : %zu\n", obj->vt_count);
	printf("total f : %zu\n", obj->f_count);
	printf("total vc : %zu\n", obj->vc_count);
	expand_faces(&e->ob[e->ob_size - 1], obj, 0, -1);
	printf("gl v count : %zu\n", e->ob[e->ob_size - 1].meshs.size);
	printf("gl vt count : %zu\n", e->ob[e->ob_size - 1].meshs.t_size);
	printf("gl vn count : %zu\n", e->ob[e->ob_size - 1].meshs.n_size);
	return (e);
}

t_scop			*process_object(t_scop *e, t_tmp **obj)
{
	if (e->ob_size > 0)
		e = obj_to_e(e, *obj);
	e->ob_size += 1;
	e->ob = (t_obj *)realloc(e->ob, sizeof(t_obj) * e->ob_size);
	e->ob[e->ob_size - 1].opts.face_check = 0;
	e->ob[e->ob_size - 1].opts.has_texture = 0;
	e->ob[e->ob_size - 1].opts.has_normal = 0;
	(*obj)->f_count = 0;
	free((*obj)->faces);
	(*obj)->faces = NULL;
	return (e);
}
