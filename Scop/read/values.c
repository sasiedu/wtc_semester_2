/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   values.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/27 02:24:25 by sasiedu           #+#    #+#             */
/*   Updated: 2016/11/27 18:02:15 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/scop.h"

void			add_vertices(t_tmp **o, char *line)
{
	t_split_string		values;
	size_t				i;

	values = ft_nstrsplit(line, ' ');
	if (values.words > 3)
	{
		(*o)->v_count += 1;
		i = (*o)->v_count;
		(*o)->v = (t_vec3f *)realloc((*o)->v, sizeof(t_vec3f) * i);
		(*o)->v[i - 1].x = atof(values.strings[1]);
		(*o)->v[i - 1].y = atof(values.strings[2]);
		(*o)->v[i - 1].z = atof(values.strings[3]);
		if (values.words == 7)
		{
			(*o)->vc_count = i;
			(*o)->vc = (t_vec3f *)realloc((*o)->vc, sizeof(t_vec3f) * i);
			(*o)->vc[i - 1].x = atof(values.strings[4]);
			(*o)->vc[i - 1].y = atof(values.strings[5]);
			(*o)->vc[i - 1].z = atof(values.strings[6]);
		}
	}
	ft_free_split(&values);
}

t_vec3f			*add_vnormals(t_vec3f *vn, size_t *vn_count, char *line)
{
	t_split_string		values;

	values = ft_nstrsplit(line, ' ');
	*vn_count += 1;
	vn = (t_vec3f *)realloc(vn, sizeof(t_vec3f) * *vn_count);
	vn[*vn_count - 1].x = atof(values.strings[1]);
	vn[*vn_count - 1].y = atof(values.strings[2]);
	vn[*vn_count - 1].z = atof(values.strings[3]);
	ft_free_split(&values);
	return (vn);
}

t_vec2f			*add_vtextures(t_vec2f *vt, size_t *vt_count, char *line)
{
	t_split_string		values;

	values = ft_nstrsplit(line, ' ');
	*vt_count += 1;
	vt = (t_vec2f *)realloc(vt, sizeof(t_vec2f) * *vt_count);
	vt[*vt_count - 1].x = atof(values.strings[1]);
	vt[*vt_count - 1].y = atof(values.strings[2]);
	ft_free_split(&values);
	return (vt);
}
