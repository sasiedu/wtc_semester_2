/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   face1.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/27 02:44:39 by sasiedu           #+#    #+#             */
/*   Updated: 2016/11/27 17:54:33 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/scop.h"

static int		count_slash(char *line)
{
	int		i;
	int		j;

	i = -1;
	j = 0;
	while (line[++i] != '\0' && line[i] != ' ')
	{
		if (line[i] == '/')
			j++;
	}
	return (j);
}

static void		check_face(t_obj *ob, char *line)
{
	int		count;

	if (strchr(line, '/') != NULL)
	{
		count = count_slash(strchr(line, ' ') + 1);
		if (count > 2)
		{
			ft_putendl_fd("Error in obj file", 2);
			exit(1);
		}
		if (count == 1)
			ob->opts.has_texture = 1;
		if (count == 2 && *(strchr(line, '/') + 1) != '/')
		{
			ob->opts.has_normal = 1;
			ob->opts.has_texture = 1;
		}
		else if (count == 2)
			ob->opts.has_normal = 1;
	}
	ob->opts.face_check = 1;
}

t_face			*add_face(t_face *f, size_t *f_count, char *line, t_obj *ob)
{
	if (ob->opts.face_check == 0)
		check_face(ob, line);
	*f_count += 1;
	f = (t_face *)realloc(f, sizeof(t_face) * *f_count);
	f[*f_count - 1].line = strdup(line);
	return (f);
}
