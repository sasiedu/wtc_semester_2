/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   normals.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/27 11:02:18 by sasiedu           #+#    #+#             */
/*   Updated: 2016/11/27 17:53:52 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/scop.h"

static	t_obj	*add_values(t_obj *ob, t_vec3f v)
{
	ob->meshs.n_size += 1;
	ob->meshs.vns[ob->meshs.n_size - 1] = v.x;
	ob->meshs.n_size += 1;
	ob->meshs.vns[ob->meshs.n_size - 1] = v.y;
	ob->meshs.n_size += 1;
	ob->meshs.vns[ob->meshs.n_size - 1] = v.z;
	return (ob);
}

t_obj			*add_gl_normals(t_split_string *v, t_obj *ob, t_tmp *t)
{
	ob->meshs.n_size += 1;
	if (ob->opts.has_normal == 1 && ob->opts.has_texture == 0)
		ob = add_values(ob, t->vn[atoi(v->strings[1]) - 1]);
	else if (ob->opts.has_normal == 1 && ob->opts.has_texture == 1)
		ob = add_values(ob, t->vn[atoi(v->strings[2]) - 1]);
	else
		ob = add_values(ob, (t_vec3f){0.0f, 0.0f, 1.0f});
	return (ob);
}
