/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/27 11:28:06 by sasiedu           #+#    #+#             */
/*   Updated: 2016/11/27 18:00:40 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/scop.h"

t_obj		*center_and_uv(t_obj *ob)
{
	ob->opts.for_uv.x = 0 - ob->opts.min.x;
	ob->opts.for_uv.y = 0 - ob->opts.min.y;
	ob->opts.center.x = (float)((ob->opts.min.x + ob->opts.max.x) / 2);
	ob->opts.center.y = (float)((ob->opts.min.y + ob->opts.max.y) / 2);
	ob->opts.center.x = (float)((ob->opts.min.z + ob->opts.max.z) / 2);
	return (ob);
}

t_obj		*get_min_max(t_obj *ob, t_tmp *t, size_t i)
{
	ob->opts.min = (t_vec3f){-1.0f, -1.0f, -1.0f};
	ob->opts.max = (t_vec3f){-1.0f, -1.0f, -1.0f};
	while (++i < t->v_count)
	{
		if (ob->opts.min.x == -1 || t->v[i].x < ob->opts.min.x)
			ob->opts.min.x = t->v[i].x;
		if (ob->opts.max.x == -1 || t->v[i].x > ob->opts.max.x)
			ob->opts.max.x = t->v[i].x;
		if (ob->opts.min.y == -1 || t->v[i].y < ob->opts.min.y)
			ob->opts.min.y = t->v[i].y;
		if (ob->opts.max.y == -1 || t->v[i].y > ob->opts.max.y)
			ob->opts.max.y = t->v[i].y;
		if (ob->opts.min.z == -1 || t->v[i].z < ob->opts.min.z)
			ob->opts.min.z = t->v[i].z;
		if (ob->opts.max.z == -1 || t->v[i].z > ob->opts.max.z)
			ob->opts.max.z = t->v[i].z;
	}
	return (center_and_uv(ob));
}
