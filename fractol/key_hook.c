/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key_hook.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/02 17:47:14 by sasiedu           #+#    #+#             */
/*   Updated: 2016/10/04 14:55:44 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int		key_hook(int key, t_env *e)
{
	if (key == ESC)
		exit(0);
	if (key == LEFT_ARROW || key == RIGHT_ARROW || key == UP_ARROW || \
			key == DOWN_ARROW)
		move_keys(key, e);
	else if (key == 46)
		e->frac.motion = (e->frac.motion == OFF) ? ON : OFF;
	else if (key == 15)
	{
		init_fractol(e);
		setup_tools(e);
	}
	else if (key == HUE_PLUS || key == HUE_MINUS || \
				key == SAT_PLUS || key == SAT_MINUS)
		change_color(key, e);
	printf("key: %d\n", key);
	pthread_action(e);
	return (0);
}

int		change_color(int key, t_env *e)
{
	if (key == HUE_PLUS)
		e->frac.hue += 10;
	else if (key == HUE_MINUS && e->frac.hue - 10 > 0)
		e->frac.hue -= 10;
	else if (key == SAT_PLUS && e->frac.saturation + 5 < 256)
		e->frac.saturation += 5;
	else if (key == SAT_MINUS && e->frac.saturation - 5 > 0)
		e->frac.saturation -= 5;
	return (0);
}

int		move_keys(int key, t_env *e)
{
	if (key == LEFT_ARROW)
		e->frac.move_x += 0.0185 * 4 / e->frac.zoom;
	else if (key == RIGHT_ARROW)
		e->frac.move_x -= 0.0185 * 4 / e->frac.zoom;
	else if (key == UP_ARROW)
		e->frac.move_y += 0.0185 * 4 / e->frac.zoom;
	else if (key == DOWN_ARROW)
		e->frac.move_y -= 0.0185 * 4 / e->frac.zoom;
	return (0);
}
