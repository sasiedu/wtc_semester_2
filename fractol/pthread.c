/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   phtread.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/04 09:41:36 by sasiedu           #+#    #+#             */
/*   Updated: 2016/10/06 09:01:40 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int		pthread_action(t_env *e)
{
	int			i;
	int			div;
	t_env		*dump;
	pthread_t	thread[6];

	i = -1;
	div = WIN_W / 6;
	while (++i < 6)
	{
		dump = (t_env *)malloc(sizeof(t_env));
		*dump = *e;
		dump->frac.w = i * div;
		dump->frac.w_max = dump->frac.w + div;
		pthread_create(&thread[i], NULL, &select_fractol, (void *)dump);
	}
	while (i--)
		pthread_join(thread[i], NULL);
	mlx_put_image_to_window(e->mlx.mlx, e->mlx.win, e->mlx.img, 0, 0);
	mlx_put_string(e);
	return (0);
}
