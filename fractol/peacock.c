/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   peacock.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/04 09:00:54 by sasiedu           #+#    #+#             */
/*   Updated: 2016/10/06 09:02:23 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int		peacock_iteration(t_env *e)
{
	t_complex	up;
	t_complex	down;
	t_complex	div;
	double		tmp;

	while (++e->frac.iteration < e->frac.iteration_max)
	{
		e->frac.old = e->frac.new;
		e->frac.old_sq = e->frac.new_sq;
		up.re = e->frac.old_sq.re - e->frac.old_sq.im - 1;
		up.im = 2 * e->frac.old.re * e->frac.old.im;
		down.re = (e->frac.c.re * e->frac.old_sq.re - e->frac.c.re * \
				e->frac.old_sq.im + e->frac.c.re);
		down.im = (2 * e->frac.c.re * e->frac.old.re * e->frac.old.im);
		tmp = down.re * down.re + down.im * down.im;
		div.re = (up.re * down.re + up.im * down.im) / tmp;
		div.im = (up.im * down.re - up.re * down.im) / tmp;
		e->frac.new.re = e->frac.old.re - div.re;
		e->frac.new.im = e->frac.old.im - div.im;
		e->frac.new_sq.re = e->frac.new.re * e->frac.new.re;
		e->frac.new_sq.im = e->frac.new.im * e->frac.new.im;
		if (e->frac.new_sq.re + e->frac.new_sq.im > 4)
			break ;
	}
	return (0);
}
