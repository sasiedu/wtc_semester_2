/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/02 12:13:46 by sasiedu           #+#    #+#             */
/*   Updated: 2016/10/06 08:34:37 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int		expose_hook(t_env *e)
{
	pthread_action(e);
	mlx_put_image_to_window(e->mlx.mlx, e->mlx.win, e->mlx.img, 0, 0);
	mlx_put_string(e);
	return (0);
}

int		setup_tools(t_env *e)
{
	e->frac.saturation = 255;
	e->frac.hue = 0;
	return (0);
}

int		start_window(char *name)
{
	t_env	*env;
	int		type;

	if ((type = verify_fractol(name)) == -1)
		exit(-1);
	if ((env = (t_env *)malloc(sizeof(t_env))) == NULL)
		exit(write(2, MALLOC_ERR, ft_strlen(MALLOC_ERR)));
	env->fractol_type = type;
	if (init_mlx(env) != 0)
		exit(my_free(env));
	setup_tools(env);
	init_fractol(env);
	pthread_action(env);
	mlx_expose_hook(env->mlx.win, expose_hook, env);
	mlx_mouse_hook(env->mlx.win, mouse_hook, env);
	mlx_key_hook(env->mlx.win, key_hook, env);
	mlx_hook(env->mlx.win, 6, (1L << 6), mouse_motion_hook, env);
	mlx_loop(env->mlx.mlx);
	return (0);
}

int		main(int argc, char **argv)
{
	int		i;
	int		wpid;
	int		status;
	pid_t	process;

	if (argc < 2)
		exit(print_usage());
	i = 0;
	while (++i < argc)
	{
		process = fork();
		if (process == -1)
			exit(1);
		else if (process == 0)
			start_window(argv[i]);
	}
	status = 0;
	while ((wpid = wait(&status)) > 0)
		;
	return (0);
}
