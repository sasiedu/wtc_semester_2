/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mouse_motion.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/02 20:37:38 by sasiedu           #+#    #+#             */
/*   Updated: 2016/10/04 14:56:28 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int		update_julia(int x, int y, t_env *e)
{
	double	crange;
	double	dist;

	crange = (e->frac.c_max.re - e->frac.c_min.re);
	dist = crange / WIN_W;
	e->frac.c.re = e->frac.c_min.re + (dist * (double)x);
	crange = (e->frac.c_max.im - e->frac.c_min.im);
	dist = crange / WIN_H;
	e->frac.c.im = e->frac.c_min.im + (dist * (double)y);
	return (0);
}

int		mouse_motion_hook(int x, int y, t_env *e)
{
	if (e->fractol_type != 1 && e->fractol_type != 4)
		return (0);
	if (x > 0 && y > 0 && x < WIN_W && y < WIN_H)
	{
		if (e->frac.motion == ON)
			update_julia(x, y, e);
	}
	pthread_action(e);
	return (0);
}
