/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   color.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/02 14:07:40 by sasiedu           #+#    #+#             */
/*   Updated: 2016/10/03 09:24:33 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int		hsv_color_to_int(int h, int s, int v)
{
	t_color		hsv;
	int			ret;

	hsv.color = 0;
	if (s == 0)
		return ((hsv.color = (v << 16) | (v << 8) | v));
	hsv.area = h / 43;
	ret = (h - (hsv.area * 43)) * 6;
	hsv.tmp1 = (v * (255 - s)) >> 8;
	hsv.tmp2 = (v * (255 - ((s * ret) >> 8))) >> 8;
	hsv.tmp3 = (v * (255 - ((s * (255 - ret)) >> 8))) >> 8;
	if (hsv.area == 0)
		hsv.color = (v << 16) | (hsv.tmp3 << 8) | hsv.tmp1;
	else if (hsv.area == 1)
		hsv.color = (hsv.tmp2 << 16) | (v << 8) | hsv.tmp1;
	else if (hsv.area == 2)
		hsv.color = (hsv.tmp1 << 16) | (v << 8) | hsv.tmp3;
	else if (hsv.area == 3)
		hsv.color = (hsv.tmp1 << 16) | (hsv.tmp2 << 8) | v;
	else if (hsv.area == 4)
		hsv.color = (hsv.tmp3 << 16) | (hsv.tmp1 << 8) | v;
	else
		hsv.color = (hsv.tmp3 << 16) | (hsv.tmp1 << 8) | hsv.tmp2;
	return (hsv.color);
}
