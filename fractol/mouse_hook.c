/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mouse_hook.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/02 18:32:01 by sasiedu           #+#    #+#             */
/*   Updated: 2016/10/06 08:56:28 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int		mouse_zoom(int button, int x, int y, t_env *e)
{
	int		dist_x;
	int		dist_y;

	dist_x = x - HALF_WIN_W;
	dist_y = y - HALF_WIN_H;
	if (button == WHEEL_UP)
		e->frac.zoom *= 1.32;
	else if (button == WHEEL_DOWN)
		e->frac.zoom /= 1.32;
	if (x > HALF_WIN_W)
		e->frac.px += (0.0185 * 4 / e->frac.zoom);
	else
		e->frac.px -= (0.0185 * 4 / e->frac.zoom);
	if (y > HALF_WIN_H)
		e->frac.py += (0.0185 * 4 / e->frac.zoom);
	else
		e->frac.py -= (0.0185 * 4 / e->frac.zoom);
	return (0);
}

int		mouse_hook(int button, int x, int y, t_env *e)
{
	if (button == WHEEL_UP || button == WHEEL_DOWN)
		mouse_zoom(button, x, y, e);
	pthread_action(e);
	return (0);
}
