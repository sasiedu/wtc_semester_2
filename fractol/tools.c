/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/02 12:56:24 by sasiedu           #+#    #+#             */
/*   Updated: 2016/10/04 17:02:43 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int		my_free(t_env *env)
{
	if (env != NULL)
		free(env);
	env = NULL;
	return (0);
}

char	*fractol_name(int id)
{
	if (id == 1)
		return ("julia");
	if (id == 2)
		return ("mandlebrot");
	if (id == 3)
		return ("mandlebrot Cube");
	if (id == 4)
		return ("julia Cube");
	if (id == 5)
		return ("burning Ship");
	if (id == 6)
		return ("fractol One");
	if (id == 7)
		return ("fractol Two");
	if (id == 8)
		return ("peacock");
	if (id == 9)
		return ("fractol Three");
	return (NULL);
}
