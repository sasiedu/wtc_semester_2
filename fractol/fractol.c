/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/02 13:31:07 by sasiedu           #+#    #+#             */
/*   Updated: 2016/10/06 08:57:41 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int		init_fractol(t_env *e)
{
	if (e->fractol_type == 1 || e->fractol_type == 4)
		return (init_julia(&e->frac));
	if (e->fractol_type == 2 || e->fractol_type == 3 || e->fractol_type == 5)
		return (init_mandlebrot(&e->frac));
	if (e->fractol_type == 6 || e->fractol_type == 7 || e->fractol_type == 8)
		return (init_mandlebrot(&e->frac));
	if (e->fractol_type == 9)
		return (init_mandlebrot(&e->frac));
	return (0);
}

void	*select_fractol(void *env)
{
	t_env	*e;

	e = env;
	if (e->fractol_type == 1 || e->fractol_type == 4)
		create_julia(e, 0, 0);
	else if (e->fractol_type == 2 || e->fractol_type == 3)
		create_mandlebrot(e, 0, 0);
	else if (e->fractol_type == 6 || e->fractol_type == 5)
		create_mandlebrot(e, 0, 0);
	else if (e->fractol_type == 7 || e->fractol_type == 8)
		create_mandlebrot(e, 0, 0);
	else if (e->fractol_type == 9)
		create_mandlebrot(e, 0, 0);
	free(e);
	pthread_exit(0);
}
