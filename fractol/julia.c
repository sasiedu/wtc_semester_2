/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   julia.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/02 13:34:31 by sasiedu           #+#    #+#             */
/*   Updated: 2016/10/06 09:00:59 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int		init_julia(t_fractol *frac)
{
	frac->zoom = 0.2;
	frac->move_x = 0;
	frac->move_y = 0;
	frac->c.re = 0;
	frac->c.im = 0;
	frac->c_min.re = -2.5;
	frac->c_max.re = 1.5;
	frac->c_min.im = -2.0;
	frac->c_max.im = 2.0;
	frac->color = 0x000000;
	frac->iteration_max = 300;
	frac->motion = OFF;
	return (0);
}

int		create_julia(t_env *e, double x, double y)
{
	int		w;
	int		h;
	double	re;
	double	im;

	h = -1;
	re = (e->frac.zoom * HALF_WIN_W);
	x = e->frac.move_x + e->frac.px;
	im = (e->frac.zoom * HALF_WIN_H);
	y = e->frac.move_y + e->frac.py;
	while (++h < WIN_H)
	{
		w = e->frac.w - 1;
		while (++w < e->frac.w_max)
		{
			e->frac.new.re = fmod(ASPECT_RATIO * (w - HALF_WIN_W) / re + x, 1E6);
			e->frac.new.im = fmod((h - HALF_WIN_H) / im + y, 1E6);
			julia_iteration(e);
			e->frac.color = hsv_color_to_int((e->frac.iteration + \
						e->frac.hue) % 256, e->frac.saturation,\
					255 * (e->frac.iteration < e->frac.iteration_max));
			pixel_put_to_image(w, h, 0, e);
		}
	}
	return (0);
}

int		julia_iteration(t_env *e)
{
	e->frac.new_sq.re = e->frac.new.re * e->frac.new.re;
	e->frac.new_sq.im = e->frac.new.im * e->frac.new.im;
	e->frac.iteration = -1;
	if (e->fractol_type == 4)
		return (julia_three_iteration(e));
	while (++e->frac.iteration < e->frac.iteration_max)
	{
		e->frac.old = e->frac.new;
		e->frac.old_sq = e->frac.new_sq;
		e->frac.new.re = e->frac.old_sq.re - e->frac.old_sq.im + e->frac.c.re;
		e->frac.new.im = 2.0 * e->frac.old.re * e->frac.old.im + \
							e->frac.c.im;
		e->frac.new_sq.re = e->frac.new.re * e->frac.new.re;
		e->frac.new_sq.im = e->frac.new.im * e->frac.new.im;
		if (e->frac.new_sq.re + e->frac.new_sq.im > 4)
			break ;
	}
	return (0);
}

int		julia_three_iteration(t_env *e)
{
	while (++e->frac.iteration < e->frac.iteration_max)
	{
		e->frac.old = e->frac.new;
		e->frac.old_sq = e->frac.new_sq;
		e->frac.new.re = e->frac.old_sq.re * e->frac.old.re - 3 * \
							e->frac.old.re * e->frac.old_sq.im + e->frac.c.re;
		e->frac.new.im = 3 * e->frac.old_sq.re * e->frac.old.im - \
							e->frac.old_sq.im * e->frac.old.im + e->frac.c.im;
		e->frac.new_sq.re = e->frac.new.re * e->frac.new.re;
		e->frac.new_sq.im = e->frac.new.im * e->frac.new.im;
		if (e->frac.new_sq.re + e->frac.new_sq.im > 4)
			break ;
	}
	return (0);
}
