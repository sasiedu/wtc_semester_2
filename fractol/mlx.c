/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mlx.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/02 12:58:56 by sasiedu           #+#    #+#             */
/*   Updated: 2016/10/04 14:53:55 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int		init_mlx(t_env *e)
{
	if ((e->mlx.mlx = mlx_init()) == NULL)
		return (write(2, "	Error: mlx init failed\n", 24));
	if ((e->mlx.win = mlx_new_window(e->mlx.mlx, WIN_W, WIN_H, \
					fractol_name(e->fractol_type))) == NULL)
		return (write(2, "	Error: mlx window failed\n", 26));
	e->mlx.img = mlx_new_image(e->mlx.mlx, WIN_W, WIN_H);
	e->mlx.img_new = mlx_new_image(e->mlx.mlx, WIN_W, WIN_H);
	e->mlx.data = mlx_get_data_addr(e->mlx.img, &e->mlx.bps, \
			&e->mlx.size_line, &e->mlx.endian);
	return (0);
}

void	pixel_put_to_image(int x, int y, int i, t_env *e)
{
	int		r;
	int		g;
	int		b;

	r = (e->frac.color & 0xFF0000) >> 16;
	g = (e->frac.color & 0x00FF00) >> 8;
	b = (e->frac.color & 0x0000FF);
	if (x >= e->frac.w && y >= 0 && x < e->frac.w_max && y < WIN_H)
	{
		i = (y * e->mlx.size_line) + ((e->mlx.bps / 8) * x);
		if (e->mlx.endian == 1)
		{
			e->mlx.data[i + 1] = r;
			e->mlx.data[i + 2] = g;
			e->mlx.data[i + 3] = b;
		}
		else if (e->mlx.endian == 0)
		{
			e->mlx.data[i] = b;
			e->mlx.data[i + 1] = g;
			e->mlx.data[i + 2] = r;
		}
	}
}

void	mlx_put_string(t_env *e)
{
	char	str[256];

	ft_strcpy(str, "Name of Fractol: ");
	ft_strcat(str, fractol_name(e->fractol_type));
	mlx_string_put(e->mlx.mlx, e->mlx.win, 0, 0, WHITE, str);
	mlx_string_put(e->mlx.mlx, e->mlx.win, 0, 20, WHITE, ARROW);
	mlx_string_put(e->mlx.mlx, e->mlx.win, 0, 40, WHITE, ZOOM_IN);
	mlx_string_put(e->mlx.mlx, e->mlx.win, 0, 60, WHITE, ZOOM_OUT);
	mlx_string_put(e->mlx.mlx, e->mlx.win, 0, 80, WHITE, HUE);
	mlx_string_put(e->mlx.mlx, e->mlx.win, 0, 100, WHITE, SAT);
	mlx_string_put(e->mlx.mlx, e->mlx.win, 0, 120, WHITE, RESET);
	mlx_string_put(e->mlx.mlx, e->mlx.win, 0, 140, WHITE, MOUSE);
	mlx_string_put(e->mlx.mlx, e->mlx.win, 0, 160, WHITE, EXIT);
}
