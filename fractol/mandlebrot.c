/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mandlebrot.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/02 16:11:16 by sasiedu           #+#    #+#             */
/*   Updated: 2016/10/06 08:53:09 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int		init_mandlebrot(t_fractol *frac)
{
	init_julia(frac);
	frac->px = 0.0;
	frac->py = 0.0;
	return (0);
}

int		create_mandlebrot(t_env *e, double x, double y)
{
	int		w;
	int		h;
	double	re;
	double	im;

	h = -1;
	re = (e->frac.zoom * WIN_W);
	x = e->frac.move_x + e->frac.px;
	im = (e->frac.zoom * WIN_H);
	y = e->frac.move_y + e->frac.py;
	while (++h < WIN_H)
	{
		w = e->frac.w - 1;
		while (++w < e->frac.w_max)
		{
			e->frac.c.re = fmod(ASPECT_RATIO * (w - HALF_WIN_W) / re + x, 1E6);
			e->frac.c.im = fmod((h - HALF_WIN_H) / im + y, 1E6);
			mandlebrot_iteration(e);
			e->frac.color = hsv_color_to_int((e->frac.iteration + \
						e->frac.hue) % 256, e->frac.saturation, \
					255 * (e->frac.iteration < e->frac.iteration_max));
			pixel_put_to_image(w, h, 0, e);
		}
	}
	return (0);
}

int		mandlebrot_iteration(t_env *e)
{
	e->frac.new.re = 0.0;
	e->frac.new.im = 0.0;
	e->frac.new_sq.re = 0.0;
	e->frac.new_sq.im = 0.0;
	e->frac.iteration = -1;
	if (e->fractol_type == 3)
		return (mandlebrot_three_iteration(e));
	if (e->fractol_type == 2)
		return (mandlebrot_one_iteration(e));
	if (e->fractol_type == 5)
		return (burning_ship_iteration(e));
	if (e->fractol_type == 6)
		return (fractol_one_iteration(e));
	if (e->fractol_type == 7)
		return (fractol_two_iteration(e));
	if (e->fractol_type == 8)
		return (peacock_iteration(e));
	if (e->fractol_type == 9)
		return (fractol_three_iteration(e));
	return (0);
}

int		mandlebrot_one_iteration(t_env *e)
{
	while (++e->frac.iteration < e->frac.iteration_max)
	{
		e->frac.old = e->frac.new;
		e->frac.old_sq = e->frac.new_sq;
		e->frac.new.re = e->frac.old_sq.re - e->frac.old_sq.im + e->frac.c.re;
		e->frac.new.im = 2.0 * e->frac.old.re * e->frac.old.im + \
							e->frac.c.im;
		e->frac.new_sq.re = e->frac.new.re * e->frac.new.re;
		e->frac.new_sq.im = e->frac.new.im * e->frac.new.im;
		if (e->frac.new_sq.re + e->frac.new_sq.im > 4)
			break ;
	}
	return (0);
}

int		mandlebrot_three_iteration(t_env *e)
{
	while (++e->frac.iteration < e->frac.iteration_max)
	{
		e->frac.old = e->frac.new;
		e->frac.old_sq = e->frac.new_sq;
		e->frac.new.re = e->frac.old_sq.re * e->frac.old.re - 3 * \
						e->frac.old.re * e->frac.old_sq.im + e->frac.c.re;
		e->frac.new.im = 3 * e->frac.old_sq.re * e->frac.old.im - \
						e->frac.old_sq.im * e->frac.old.im + e->frac.c.im;
		e->frac.new_sq.re = e->frac.new.re * e->frac.new.re;
		e->frac.new_sq.im = e->frac.new.im * e->frac.new.im;
		if (e->frac.new_sq.re + e->frac.new_sq.im > 4)
			break ;
	}
	return (0);
}
