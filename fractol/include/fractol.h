/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/02 12:13:04 by sasiedu           #+#    #+#             */
/*   Updated: 2016/10/06 08:32:52 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRACTOL_H
# define FRACTOL_H

# include "struct.h"
# include "defines.h"
# include <mlx.h>
# include <pthread.h>
# include <math.h>
# include <float.h>

/*
*** usage and tools functions
*/
int		print_usage(void);
int		verify_fractol(char *str);
int		my_free(t_env *env);
int		setup_tools(t_env *e);
char	*fractol_name(int id);

/*
*** pthread functions
*/
int		pthread_action(t_env *e);

/*
*** mlx functions
*/
int		init_mlx(t_env *e);
int		expose_hook(t_env *e);
void	pixel_put_to_image(int x, int y, int i, t_env *e);
void	mlx_put_string(t_env *e);

/*
*** fractol functions
*/
int		init_fractol(t_env *e);
void	*select_fractol(void *env);
int		init_julia(t_fractol *frac);
int		create_julia(t_env *e, double x, double y);
int		julia_iteration(t_env *e);
int		init_mandlebrot(t_fractol *frac);
int		create_mandlebrot(t_env *e, double x, double y);
int		mandlebrot_iteration(t_env *e);
int		mandlebrot_three_iteration(t_env *e);
int		julia_three_iteration(t_env *e);
int		mandlebrot_one_iteration(t_env *e);
int		burning_ship_iteration(t_env *e);
int		peacock_iteration(t_env *e);
int		fractol_one_iteration(t_env *e);
int		fractol_two_iteration(t_env *e);
int		fractol_three_iteration(t_env *e);

/*
*** color functions
*/
int		hsv_color_to_int(int h, int s, int v);
int		change_color(int key, t_env *e);

/*
*** keys and mouse functions
*/
int		key_hook(int key, t_env *e);
int		move_keys(int key, t_env *e);
int		mouse_hook(int button, int x, int y, t_env *e);
int		mouse_zoom(int button, int x, int y, t_env *e);
int		mouse_motion_hook(int x, int y, t_env *e);
int		update_julia(int x, int y, t_env *e);

#endif
