/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   defines.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/02 12:53:14 by sasiedu           #+#    #+#             */
/*   Updated: 2016/10/03 12:08:54 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DEFINES_H
# define DEFINES_H

# include "libft.h"

# define MALLOC_ERR "	Error: falied to malloc\n"

# define WIN_W 800
# define WIN_H 600
# define HALF_WIN_W 400
# define HALF_WIN_H 300
# define ASPECT_RATIO (800 / 600)

# define LEFT_ARROW 123
# define RIGHT_ARROW 124
# define UP_ARROW 126
# define DOWN_ARROW 125
# define WHEEL_UP 5
# define WHEEL_DOWN 4
# define ESC 53

# define ON 1
# define OFF 0

# define HUE_PLUS 30
# define HUE_MINUS 33
# define SAT_PLUS 24
# define SAT_MINUS 27

# define ARROW "Move Fractol: Arrow Keys"
# define ZOOM_IN "Zoom In : Scroll Up"
# define ZOOM_OUT "Zoom Out: Scroll Down"
# define HUE "Change Hue: [ , ]"
# define SAT "Change Saturation: -, +"
# define RESET "Reset Fractol: r"
# define MOUSE "Mouse Move: m"
# define EXIT "Exit: Esc key"

# define WHITE (0x0FFFFFF)
# define BLACK (0x0000000)

#endif
