/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   struct.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/02 12:02:13 by sasiedu           #+#    #+#             */
/*   Updated: 2016/10/04 15:50:56 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef STRUCT_H
# define STRUCT_H

# include "libft.h"

typedef	struct	s_mlx
{
	void		*mlx;
	void		*win;
	void		*img;
	void		*img_new;
	char		*data;
	int			bps;
	int			size_line;
	int			endian;
}				t_mlx;

typedef	struct	s_complex
{
	double		re;
	double		im;
}				t_complex;

typedef	struct	s_color
{
	int			tmp1;
	int			tmp2;
	int			tmp3;
	int			area;
	int			color;
}				t_color;

typedef	struct	s_fractol
{
	t_complex	c;
	t_complex	c_min;
	t_complex	c_max;
	t_complex	old;
	t_complex	new;
	t_complex	old_sq;
	t_complex	new_sq;
	double		zoom;
	double		move_x;
	double		move_y;
	double		px;
	double		py;
	int			saturation;
	int			hue;
	int			iteration;
	int			iteration_max;
	int			color;
	int			motion;
	int			h;
	int			w;
	int			h_max;
	int			w_max;
}				t_fractol;

typedef	struct	s_env
{
	t_mlx		mlx;
	t_fractol	frac;
	int			fractol_type;
}				t_env;

#endif
