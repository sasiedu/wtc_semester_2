/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bShip.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/02 23:26:13 by sasiedu           #+#    #+#             */
/*   Updated: 2016/10/03 12:17:48 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int		burning_ship_iteration(t_env *e)
{
	while (++e->frac.iteration < e->frac.iteration_max)
	{
		e->frac.old = e->frac.new;
		e->frac.old_sq = e->frac.new_sq;
		e->frac.new.re = e->frac.old_sq.re - e->frac.old_sq.im - e->frac.c.re;
		e->frac.new.im = 2 * fabs(e->frac.old.im) * fabs(e->frac.old.re) + \
							e->frac.c.im;
		e->frac.new_sq.re = e->frac.new.re * e->frac.new.re;
		e->frac.new_sq.im = e->frac.new.im * e->frac.new.im;
		if (e->frac.new_sq.re + e->frac.new_sq.im > 4.0)
			break ;
	}
	return (0);
}
