/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   usage.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/02 12:15:59 by sasiedu           #+#    #+#             */
/*   Updated: 2016/10/06 09:05:43 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int		print_usage(void)
{
	ft_putendl("run ./fractol with the arguments below : ");
	ft_putendl("	julia -> for Julia fractol");
	ft_putendl("	mandlebrot OR mand -> for Mandlebrot fractol");
	ft_putendl("	mandlebrot2 OR mand2 -> for Mandlebrot cube fractol");
	ft_putendl("	julia2 -> for Julia cube fractol");
	ft_putendl("	burningship OR bship -> for BurningShip fractol");
	ft_putendl("	fractolOne OR fOne -> for fractol One");
	ft_putendl("	fractolTwo OR fTwo -> for fractol Two");
	ft_putendl("*******************************************");
	return (-1);
}

int		verify_fractol(char *str)
{
	ft_tolower_str(&str);
	if (ft_strcmp(str, "julia") == 0)
		return (1);
	if (ft_strcmp(str, "mandlebrot2") == 0 || ft_strcmp(str, "mand2") == 0)
		return (3);
	if (ft_strcmp(str, "mandlebrot") == 0 || ft_strcmp(str, "mand") == 0)
		return (2);
	if (ft_strcmp(str, "julia2") == 0)
		return (4);
	if (ft_strcmp(str, "burningship") == 0 || ft_strcmp(str, "bship") == 0)
		return (5);
	if (ft_strcmp(str, "fractolone") == 0 || ft_strcmp(str, "fone") == 0)
		return (6);
	if (ft_strcmp(str, "fractoltwo") == 0 || ft_strcmp(str, "ftwo") == 0)
		return (7);
	if (ft_strcmp(str, "peacock") == 0 || ft_strcmp(str, "pcock") == 0)
		return (8);
	if (ft_strcmp(str, "fractolthree") == 0 || ft_strcmp(str, "fthree") == 0)
		return (9);
	ft_putendl_fd("	Error: Unknown fractol", 2);
	return (print_usage());
}
