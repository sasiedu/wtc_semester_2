/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   new1.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/04 15:18:41 by sasiedu           #+#    #+#             */
/*   Updated: 2016/10/06 09:06:02 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int		fractol_one_iteration(t_env *e)
{
	while (++e->frac.iteration < e->frac.iteration_max)
	{
		e->frac.old = e->frac.new;
		e->frac.old_sq = e->frac.new_sq;
		e->frac.new.re = e->frac.old_sq.re + e->frac.old_sq.im - \
						e->frac.old.re + e->frac.c.re;
		e->frac.new.im = (2 * e->frac.old.re * e->frac.old.im) - \
						e->frac.old.im + e->frac.c.im;
		e->frac.new_sq.re = e->frac.new.re * e->frac.new.re;
		e->frac.new_sq.im = e->frac.new.im * e->frac.new.im;
		if (e->frac.new_sq.re + e->frac.new_sq.im > 4)
			break ;
	}
	return (0);
}

int		fractol_two_iteration(t_env *e)
{
	double	re;
	double	im;

	while (++e->frac.iteration < e->frac.iteration_max)
	{
		e->frac.old = e->frac.new;
		e->frac.old_sq = e->frac.new_sq;
		re = e->frac.old_sq.re - e->frac.old_sq.im + e->frac.c.re;
		im = 2.0 * e->frac.old.re * e->frac.old.im + e->frac.c.im;
		e->frac.new.re = re + e->frac.old.re + e->frac.c.re;
		e->frac.new.im = im + e->frac.old.im + e->frac.c.im;
		e->frac.new_sq.re = e->frac.new.re * e->frac.new.re;
		e->frac.new_sq.im = e->frac.new.im * e->frac.new.im;
		if (e->frac.new_sq.re + e->frac.new_sq.im > 4)
			break ;
	}
	return (0);
}

int		fractol_three_iteration(t_env *e)
{
	t_complex	a;
	t_complex	b;

	while (++e->frac.iteration < e->frac.iteration_max)
	{
		e->frac.old = e->frac.new;
		e->frac.old_sq = e->frac.new_sq;
		a.re = (pow(2, e->frac.old.re) * (cos(e->frac.old.im)));
		a.im = sin(e->frac.old.im);
		b.re = e->frac.c.re * e->frac.old_sq.re - e->frac.c.re * \
			e->frac.old_sq.im + 2 * e->frac.c.im * e->frac.old.re * \
			e->frac.old.im;
		b.im = 2 * e->frac.c.re * e->frac.old.re * e->frac.old.im + \
			e->frac.c.im * e->frac.old_sq.re - e->frac.old_sq.im;
		e->frac.new.re = a.re * b.re + b.im * a.im;
		e->frac.new.im = a.re * b.im + a.im * b.re;
		e->frac.new_sq.re = e->frac.new.re * e->frac.new.re;
		e->frac.new_sq.im = e->frac.new.im * e->frac.new.im;
		if (e->frac.new_sq.re + e->frac.new_sq.im > 4)
			break ;
	}
	return (0);
}
